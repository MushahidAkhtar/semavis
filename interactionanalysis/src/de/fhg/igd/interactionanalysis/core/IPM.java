package de.fhg.igd.interactionanalysis.core;

import de.fhg.igd.interactionanalysis.core.data.QueryResult;
import de.fhg.igd.interactionanalysis.core.data.SteadyStateVector;

/**
 * The main Interface for probabilistic models
 * 
 * @author Christian Stab
 */
public interface IPM {

	/**
	 * Updates the probabilities if a new observation occures
	 * @param obs observation
	 */
	public abstract void newObservation(int obs);
	
	/**
	 * Updates the probabilities if new observations occures
	 * @param obs observations
	 */
	public abstract void newObservations(int[] obs);
	
	/**
	 * returns the next Action 
	 * @return next state
	 */
	public int getNextAction();
	
	/**
	 * returns the next most possible actions
	 * @param count number of actions
	 * @return next most possible actions 
	 */
	public int[] getNextActions(int count);
	
	/**
	 * returns the rank of a prediction
	 * @param from source state
	 * @param to target state
	 * @return rank
	 */
	public int getPredictionRank(int from, int to);
	
	/**
	 * returns the probability of a prediction
	 * @param from source state
	 * @param to target state
	 * @return rank
	 */
	public double getPredictionProbability(int from, int to);
	
	/**
	 * returns the steady state vector of this model
	 * @return SteadyStateVector
	 */
	public SteadyStateVector getSteadyStateVector();
	
	/**
	 * returns the results of a query
	 * @param ranks ranks of the query
	 * @return QueryResult
	 */
	public QueryResult query(int[] ranks);
	
	/**
	 * sets the trainingsdata for the probabilistic model 
	 * @param data trainingsdata
	 */
	public void setTrainingsData(int[] data);
}
