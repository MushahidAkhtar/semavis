package de.fhg.igd.interactionanalysis.core;

import de.fhg.igd.interactionanalysis.core.data.Abstractions;
import de.fhg.igd.interactionanalysis.core.data.Interactions;


/**
 * The implementation of relational markov model 
 * 
 * @author Christian Stab
 */
public class RMMUniform extends RMM {
	
	/** its serial number for serialization */
	private static final long serialVersionUID = -4455462699940037568L;

	/** the counts of transitions from state to state */
	private int[][] transitionCounts;
	
	/** counts of a state */
	private int[] counts;
	
	/** Counts of the initial state */
	private int[] initialCounts;
	
	/** count the number of seen initial observations */ 
	private int initialCount;
	
	/**
	 * Basic Constructor
	 */
	public RMMUniform() {
		super();
		int numberOfStates = Interactions.getInstance().getSize();
		transitionCounts = new int[numberOfStates][numberOfStates];
		counts = new int[numberOfStates];
		initialCounts = new int[numberOfStates];
		initialCount=0;
	}
	
	@Override
	protected void updateTransitionMatrix(int obs) {
		//int observation = States.getInstance().getIndex(obs);
		if (previousObservation==-1) {
			previousObservation = obs;
			updateInitialProbabilies(obs);
		} else {
			transitionCounts[previousObservation][obs]++;
			counts[previousObservation]++;
			for (int i=0; i<stm.getSize(); i++) {
				stm.setProbability(previousObservation, i, (double)transitionCounts[previousObservation][i]/(double) counts[previousObservation]);
			}
			
			previousObservation = obs;
		}
	}
	
	@Override
	protected double getTransProb(int from, int to) {
		//System.out.println("  getTransProb()");
		
		// rmm-uniform
		int[] fromAbstractions = Abstractions.getInstance().getStateAbstractions(from);
		int[] toAbstractions = Abstractions.getInstance().getStateAbstractions(to);
		
		// lambda uniform
		double res = 0.0f;
		double lambda = 1.0f / (double)(fromAbstractions.length * toAbstractions.length);
		for (int i=0; i<fromAbstractions.length; i++) {
			int fromAbstraction = fromAbstractions[i];
			for (int j=0; j<toAbstractions.length; j++) {
				int toAbstraction = toAbstractions[j];
				
				// P(to | toAbstr)
				double toProb = 1.0f / (double)Abstractions.getInstance().getAbstractionStates(toAbstraction).length;
				res = res + (lambda * abstMatrix.getProbability(fromAbstraction, toAbstraction) * toProb);
			}
		}
		
		return res;
	}
	
	@Override
	protected void updateInitialProbabilies(int firstObservation) {
		initialCounts[firstObservation]++;
		initialCount++;
		for (int i=0; i<initProb.length; i++) {
			initProb[i] = ((double)initialCounts[i]) / ((double)initialCount);
		}
	}
	
	@Override
	public void setTrainingsData(int[] data) {
		for (int i=0; i<data.length; i++) {
			newObservation(data[i]);
		}
	}
	
}
