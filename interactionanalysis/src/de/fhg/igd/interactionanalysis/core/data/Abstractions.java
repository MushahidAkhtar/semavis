package de.fhg.igd.interactionanalysis.core.data;

import java.util.ArrayList;
import java.util.HashMap;

import de.fhg.igd.interactionanalysis.core.utility.Permutator;
import de.fhg.igd.interactionanalysis.core.utility.Printer;

/**
 * This Singleton contains all abstractions and provides some functions
 * for abstraction handling including the mapping from the string representation
 * to a unique number for each abstraction
 * 
 * @author Christian Stab
 */
public class Abstractions {

	  /** Instance of this Singleton */ 
	  private static Abstractions instance;
	  
	  /** string representation of all abtractions */
	  private String[][] abstractionStrings;
	  
	  /** HashMap for mapping names of states to numbers */
	  private HashMap<String, Integer> map;
		
	  /** reversed Mapping: from number to String */
	  private HashMap<Integer, String[]> reversedMap; 
	  
	  /** the abstractions of a state */ 
	  private int[][] stateAbstractions; 
	  
	  /** the states a abstraction contains */
	  private int[][] abstractionStates;
	  
	  /** the ranks of abstractions*/
	  private int[] ranks;
	  
	  /** size of abstractions */
	  private int size;
	  
	  /** 
	   * Basic Constructor 
	   */
	  private Abstractions () {
		  map = new HashMap<String, Integer>();
		  reversedMap = new HashMap<Integer, String[]>();
		  
		  init();
	  }
	  
	  /**
	   * Returns the instance of this sinlgeton
	   * @return instance of this singleton
	   */
	  public static synchronized Abstractions getInstance() {
	    if (Abstractions.instance == null) {
	    	Abstractions.instance = new Abstractions();
	    }
	    return Abstractions.instance;
	  }
	  
	  /**
	   * calculates all abstractions
	   */
	  private void init() {

		  ArrayList<ArrayList<Integer>> stateAbstractions = new ArrayList<ArrayList<Integer>>();
		  HashMap<Integer, ArrayList<Integer>> abstractionStates = new HashMap<Integer,ArrayList<Integer>>();
		  ArrayList<String[]> abstractionStrings = new ArrayList<String[]>();
		  
		  int stateCount = Interactions.getInstance().getSize();
		  for (int i=0; i<stateCount; i++) {
			  
			  // with leafs !!!
			  String[][] abstractions = Permutator.getPermutations(Domains.getInstance().getAllParents(Interactions.getInstance().getInteractionString(i)));
			  
			  ArrayList<Integer> stateAbst = new ArrayList<Integer>();
			  for (int j=0;j<abstractions.length; j++) {
				  int key = -1;
				  
				  // add abstraction
				  if (!map.containsKey(getKey(abstractions[j]))) {
					  map.put(getKey(abstractions[j]), size);
					  reversedMap.put(size, abstractions[j]);
					  abstractionStrings.add(abstractions[j]);
					  abstractionStates.put(size,new ArrayList<Integer>());
					  size++;
					  key = size-1;
				  } else {
					  key = map.get(getKey(abstractions[j]));
				  }
				  
				  // add state to the abstractionstates
				  abstractionStates.get(key).add(i);
				  
				  // add abstraction to states Abstractions
				  stateAbst.add(key);
				 
			  }
			  stateAbstractions.add(stateAbst);
			  
		  }
		  
		  // convert to Arrays
		  this.stateAbstractions = new int[stateAbstractions.size()][];
		  for (int j=0;j<stateAbstractions.size(); j++) {
			  ArrayList<Integer> abstractions = stateAbstractions.get(j);
			  int[] tmp = new int[abstractions.size()];
			  for (int i=0; i<tmp.length; i++) {
				  tmp[i] = abstractions.get(i).intValue();
			  }
			 this.stateAbstractions[j] = tmp;
		  }
		  
		  this.abstractionStates = new int[abstractionStates.size()][];
		  for (int j=0; j<abstractionStates.size(); j++) {
			  ArrayList<Integer> states = abstractionStates.get(j);
			  int[] tmp = new int[states.size()];
			  for (int i=0; i<tmp.length; i++) {
				  tmp[i] = states.get(i).intValue();
			  }
		 
			  this.abstractionStates[j]=tmp;
		  }
		  
		  this.abstractionStrings = new String[abstractionStrings.size()][];
		  for (int j=0;j<abstractionStrings.size(); j++) {
			  this.abstractionStrings[j] = abstractionStrings.get(j);
		  }
		  
		  ranks = new int[size];
		  
	  }
	  
	  /**
	   * Returns the states which belong to an abstraction 
	   * @param abstraction abstraction
	   * @return the states belonging to the abstraction
	   */
	  public int[] getAbstractionStates(int abstraction) {
		  return abstractionStates[abstraction];
	  }
	  
	  /**
	   * Returns the states which belong to an abstraction 
	   * @param abstraction abstraction
	   * @return the states belonging to the abstraction
	   */
	  public int[] getAbstractionStates(String[] abstraction) {
		  int index = getIndex(abstraction);
		  if (index!=-1) {
			  return getAbstractionStates(index);
		  } else {
			  return null;
		  }
	  }
	  
	  /**
	   * returns all abstractions a state belongs to
	   * @param state the state
	   * @return all abstractions
	   */
	  public int[] getStateAbstractions(int state) {
		  return stateAbstractions[state];
	  }
	  
	  /**
	   * returns the number of abstractions
	   * @return number of abstraction
	   */
	  public int getSize() {
		  return size;
	  }
	  
	  /**
	   * returns the index of a state
	   * @param abstraction String representation of a state
	   * @return index
	   */
	  private int getIndex(String[] abstraction) {
		  if (!map.containsKey(getKey(abstraction))) {
			  System.out.println(getKey(abstraction));
			  return -1;
		  } else {
			  return map.get(getKey(abstraction)).intValue();
		  }
	  }
	
	  /**
	   * returns the internal string representation of a abstraction
	   * @param abstraction state
	   * @return String
	   */
	  private String getKey(String[] abstraction) {
		  String key = "";
		  for (int j=0; j<abstraction.length;j++) {
			  key = key + "." + abstraction[j];
		  }
		  return key;
	  }
	 
	  /**
	   * returns the String representation of a state
	   * @param index index of the state
	   * @return String representation of a state
	   */
	  private String[] getAbstString(int index) {
		  return reversedMap.get(new Integer(index));
	  }
	  
	  public String[][] getRankAbstractions(int ranks[]) {
		  ArrayList<String[]> list = new ArrayList<String[]>();
		  
		  for (int i=0;i<abstractionStrings.length; i++) {
			  boolean match = true;
			  for (int j=0; j<ranks.length; j++) {
				  
				  if (abstractionStrings[i][j].split("\\.").length != ranks[j]+1) { 
					  match = false;
				  } 
				  if (Domains.getInstance().getNode(abstractionStrings[i][j])!= null && 
					   Domains.getInstance().getNode(abstractionStrings[i][j]).getChildren()==null &&
					   abstractionStrings[i][j].split("\\.").length < ranks[j]+1) {
					  match = true;
				  } 
				  
				  
			  }
			  if (match) {
				  list.add(abstractionStrings[i]);
			  }
		  }
		  
		  return list.toArray(new String[list.size()][]);
	  }
	  
	  /**
	   * returns the rank of an abstraction
	   * @param abstraction the abstraction
	   * @return the rank of the abstraction
	   */
	  public int getRank(int abstraction) {
		  String[] abst = getAbstString(abstraction);
		  if (ranks[abstraction]==0) {
			  int rank = 1;
			  for (int i=0; i<abst.length; i++) {
				  rank = rank + Domains.getInstance().getNode(abst[i]).getRank();
			  }
			  ranks[abstraction] = rank;
		  }
		  return ranks[abstraction];
	  }
	  
	  
	  public static void main(String args[]) {
		  String[][] abst = Abstractions.getInstance().getRankAbstractions(new int[] {1,0});
		  for (int i=0; i<abst.length; i++) {
			  Printer.printState2(abst[i]);
			  System.out.println();
		  }
		 
		 
	  }
	 
}
