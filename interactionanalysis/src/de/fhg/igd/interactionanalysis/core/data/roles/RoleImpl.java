package de.fhg.igd.interactionanalysis.core.data.roles;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.fhg.igd.interactionanalysis.core.data.Abstractions;
import de.fhg.igd.interactionanalysis.core.data.QueryResult;
import de.fhg.igd.interactionanalysis.core.data.Interactions;
import de.fhg.igd.interactionanalysis.core.data.SteadyStateVector;
import de.fhg.igd.interactionanalysis.core.utility.InteractionTranslator;

/**
 * this class implements a Role. It provides functions for query this
 * role, adding sequences and calculating matchings with sequences.
 * 
 * @author Christian Stab
 */
public class RoleImpl implements IRole, Serializable {


	/** its serial number */
	private static final long serialVersionUID = -1359414284427908343L;

	/** counts for every state */
	private int[] interactionCounts;
	
	/** count of all seen interactions */
	private int interactionCount; 
	
	/** all sequences belonging to this role */
	private List<List<Integer>> sequences;
	
	
	/**
	 * basic constructor
	 */
	public RoleImpl() {
		sequences = new ArrayList<List<Integer>>();
		int numberOfStates = Interactions.getInstance().getSize();
		interactionCounts = new int[numberOfStates];
	}
	
	@Override
	public void newRoleSequence(List<Integer> sequence) {
		// update stateCounts
		for (int i=0; i<sequence.size(); i++) {
			interactionCounts[sequence.get(i)]++;
			interactionCount++;
		}
		
		sequences.add(sequence);
	}
	
	@Override
	public double match(List<Integer> seq) {
		double result =0;
		for (int i=0; i<sequences.size(); i++) {
			List<Integer> sequence = sequences.get(i);
			result = result + matchSequences(seq, sequence);
		}
		
		return result;
	}
	
	/**
	 * compares two sequences an return a ranking. if the ranking is high 
	 * the similarity of the two sequences is high
	 * @param a list 1
	 * @param b list 2
	 * @return ranking 
	 */
	public double matchSequences(List<Integer> a, List<Integer> b) {
		double result = 0;
		for (int i=0; i<=a.size(); i++) {
			for (int j=i+1;j<=a.size(); j++) {
				List<Integer> subList = a.subList(i, j);
				result = result + countSeq(subList, b)*Math.pow(subList.size(),19);
			}
		}
		return result;
	}
	
	/**
	 * counts the occurences of the list a in list in
	 * @param a the occurences of this sequences is counted
	 * @param in the search space
	 * @return the number of occurences of a in in
	 */
	public int countSeq(List<Integer> a, List<Integer> in) {
		int result = 0;
		
		for(int i=0; i<in.size()-a.size()+1; i++) {
			boolean match = true;
			for (int j=0; j<a.size(); j++) {
				//int t1=in.get(i+j);
				//int t2=a.get(j);
				if (in.get(i+j).intValue()!=a.get(j).intValue()) {
					match = false;
				} 
			}
			if (match) {
				result++;
			}
		}
		return result;
	}
	
	@Override
	public QueryResult query(int[] ranks) {
		String[][] rankAbstractions = Abstractions.getInstance().getRankAbstractions(ranks);
		SteadyStateVector ssv = getSteadyStateVector();
		double[] result = new double[rankAbstractions.length];
		
		for (int i=0; i<result.length; i++) {
			int[] rankAbstractionStates = Abstractions.getInstance().getAbstractionStates(rankAbstractions[i]);
			double prob = 0.0f;
			for (int j=0; j<rankAbstractionStates.length; j++) {
				prob = prob + ssv.getValues()[rankAbstractionStates[j]];
			}
			result[i] = prob;
		}
		
		return new QueryResult(result, InteractionTranslator.translateInteractions(rankAbstractions));
	}
	
	@Override
	public SteadyStateVector getSteadyStateVector() {
		SteadyStateVector ssv = new SteadyStateVector(Interactions.getInstance().getSize());
		ssv.setIndices(InteractionTranslator.translateInteractions(Interactions.getInstance().getInteractions()));
		
		for (int i=0; i<Interactions.getInstance().getSize(); i++) {
			ssv.setProbability(i, ((double)interactionCounts[i])/((double)interactionCount));
		}
		
		return ssv;
	}
	
	@Override
	public List<List<Integer>> getSequences() {
		return sequences;
	}


	/**
	 * prints all sequences of this role (for debugging)
	 */ 
	public void printSequences() {
		for (int i=0; i<sequences.size(); i++) {
			List<Integer> list = sequences.get(i);
			System.out.print("    ");
			for (int j=0; j<list.size(); j++) {
				System.out.print(list.get(j) + "\t");
			}
			System.out.println();
		}
	}
	
}
