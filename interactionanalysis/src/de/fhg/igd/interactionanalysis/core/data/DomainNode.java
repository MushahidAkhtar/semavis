package de.fhg.igd.interactionanalysis.core.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This Class represents a node of a domain.
 * 
 * @author Christian Stab
 */
public class DomainNode {
	
	/** the name of this node */
	private String name;
	
	/** The complete name of this node */
	private String completeName;
	
	/** the parent node of this node. For root nodes parent is null */
	private DomainNode parent;
	
	/** the child nodes of this node. For leaf nodes children is null */
	private List<DomainNode> children;
	
	/** the rank of the node */
	private int rank;
	
	/** Basic Constructor */
	public DomainNode(String name) {
		this.name = name;
		rank = -1;
	}
	
	/**
	 * Returns the name of this node
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the complete Name of this Node
	 * @return complete Name
	 */
	private String getCompleteName() {
		if (completeName==null){
			if (parent==null) {
				return name;
			} else {
				completeName = parent.getCompleteName() + "." + name;
				
			}
		} 
		return completeName;
	}
	
	/**
	 * Returns the leaves of the subtree 
	 * @return the leaves of the subtree
	 */
	public ArrayList<String> getLeaves() {
		ArrayList<String> leaves = new ArrayList<String>();
		if (children!=null) {
			for (int i=0; i<children.size(); i++) {
				leaves.addAll(children.get(i).getLeaves());
			}
		} else {
			leaves.add(getCompleteName());
		}
		return leaves;
	}
	
	/**
	 * returns all nodes up to the given rank
	 * @param rank the rank
	 * @return  all nodes up to the given rank
	 */
	public ArrayList<String> getNodesUpToRank(int rank) {
		ArrayList<String> nodes = new ArrayList<String>();
		if (getRank()==rank || children==null) {
			nodes.add(this.getCompleteName());
		} else {
			for (int i=0; i<children.size(); i++) {
				nodes.addAll(children.get(i).getNodesUpToRank(rank));
			}
		}
		
		return nodes;
	}
	
	/** 
	 * Returns the parent of this node
	 * @return the parent node
	 */
	private DomainNode getParent() {
		return parent;
	}
		
	/**
	 * Sets the parent node for this node
	 * @param parent the new parent
	 */
	private void setParent(DomainNode parent) {
		this.parent=parent;
	}
	
	/** 
	 * Adds a Child node to this node
	 * @param child the new Child node
	 */
	public void addChildren(DomainNode child) {
		if (children==null) {
			children=new ArrayList<DomainNode>();
		}
		child.setParent(this);
		children.add(child);
	}
	
	/**
	 * Returns the children of this node 
	 * @return children of this node
	 */
	public List<DomainNode> getChildren() {
		return children;
	}
	
	/**
	 * returns the complete names of all descendants of this node
	 * @return descendants of this node
	 */
	public String[] getAllNodes() {
		ArrayList<String> nodes = new ArrayList<String>();
		nodes.add(getCompleteName());
		if (children!=null) {
			for (int i=0; i<children.size(); i++) {
				nodes.addAll(Arrays.asList(children.get(i).getAllNodes()));
			}
		}
		String[] nodesArray = new String[nodes.size()];
		return nodes.toArray(nodesArray);
	}
	
	/** 
	 * Returns the complete Names of all Parents of this Node. (List contains 
	 * also this node )
	 * @return Complete names of all parents
	 */
	public String[] getAllParents() {
		ArrayList<String> parents = new ArrayList<String>();
		parents.add(getCompleteName());
		if (parent!=null) {
			parents.addAll(Arrays.asList(getParent().getAllParents()));
		}
		String[] parentsArray = new String[parents.size()];
		return parents.toArray(parentsArray);
	}
	
	/**
	 * returns the child with the given name or null if node doesn't contain
	 * child with the given name
	 * @param name name of the Child
	 * @return DomainNode
	 */
	public DomainNode getChild(String name) {
		for (int i=0; i<children.size(); i++) {
			if (children.get(i).getName().equals(name)) {
				return children.get(i);
			}
		}
		return null;
	}
	
	/** 
	 * returns the rank of a node (the rank of root node is 0)
	 * @return the rank of this node
	 */
	public int getRank() {
		if (rank == -1) {
			if (parent==null) {
				rank = 0;
			} else {
				rank = getParent().getRank() + 1;
			}
		}
		return rank;
	}
}
