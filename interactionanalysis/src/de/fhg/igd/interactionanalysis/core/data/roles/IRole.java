package de.fhg.igd.interactionanalysis.core.data.roles;

import java.util.List;

import de.fhg.igd.interactionanalysis.core.data.QueryResult;
import de.fhg.igd.interactionanalysis.core.data.SteadyStateVector;

/**
 * Interface defining a user role
 * 
 * @author Christian Stab
 */
public interface IRole {

	/**
	 * indicates a new sequence for this role
	 * @param sequence sequence of interactions
	 */
	public void newRoleSequence(List<Integer> sequence);
	
	/**
	 * returns the affinity value that the given sequence of interactions
	 * matches to this role
	 * @param sequence sequence of interactions
	 * @return affinity value
	 */
	public double match(List<Integer> sequence);
	
	/**
	 * returns all sequences belonging to this role
	 * @return all sequences belonging to this role
	 */
	public List<List<Integer>> getSequences();
	
	/**
	 * queries the role and returns a QueryResult object
	 * @param ranks ranks for the query
	 * @return QueryResult
	 */
	public QueryResult query(int[] ranks);
	
	/**
	 * returns the steady state vector of this role
	 * @return SteadyStateVector
	 */
	public SteadyStateVector getSteadyStateVector();
	
}
