package de.fhg.igd.interactionanalysis.core.data;

import java.io.Serializable;

/**
 * This class represents a Transition Matrix
 * 
 * @author Christian Stab
 */
public class TransitionMatrix implements Serializable {

	/** its serial number for serialization */
	private static final long serialVersionUID = -8570464534703533090L;
	
	/** the matrix */
	private double[][] matrix;
	
	/** 
	 * Basic Constructor
	 * @param size the size of the matrix
	 */
	public TransitionMatrix(int size) {
		matrix = new double[size][size];
	}
	
	/**
	 * Sets the probability
	 * @param from from-index
	 * @param to to-index
	 * @param value probability
	 */
	public void setProbability(int from, int to, double value) {
		matrix[from][to] = value;
	}
	
	/**
	 * returns the probability
	 * @param from from state
	 * @param to to state
	 * @return probability
	 */
	public double getProbability(int from, int to) {
		return matrix[from][to];
	}
	
	/**
	 * returns the transition probability vector of one state
	 * @param index the state
	 * @return transition probability vector
	 */
	public double[] getVector(int index) {
		return matrix[index];
	}
	
	/**
	 * sets the probability vector of a state
	 * @param state the state
	 * @param vector the new probability vector
	 */
	public void setVector(int state, double[] vector) {
		matrix[state] = vector;
	}
	
	/**
	 * Initializes the probability of a state with equal probabilities
	 * @param state the state
	 */
	private void initializeState(int state) {
		double init = 1.0f / (double)matrix.length;
		for (int i=0; i<matrix.length; i++) {
			matrix[state][i]=init;
		}
	}
	
	/**
	 * Initializes all states with equal probabilities
	 */
	public void initialize() {
		for (int i=0; i<matrix.length; i++) {
			initializeState(i);
		}
	}
	
	/**
	 * Returns true if the row of the state is initialized
	 * @param state
	 * @return true if the row af the state is initialized; else false
	 */
	public boolean isInitialized(int state) {
		double[] test = getVector(state);
		double result = 0;
		for (int i=0; i<test.length; i++) {
			result = result + test[i];
		}
		return result!=0;
	}
	
	/**
	 * returns true if the state was seen before
	 * @param state state
	 * @return true if the state was seen before
	 */
	public boolean stateSeen(int state) {
		double init = 1.0f / (double)matrix.length;
		for (int i=0; i<matrix[state].length; i++) {
			if (matrix[state][i]!=init) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * !!Use only for debugging purposes 
	 * @return the internal double matrix
	 */
	public double[][] getMatrix() {
		return matrix;
	}
	
	/**
	 * returns the size of the matrix
	 * @return size of the matrix
	 */
	public int getSize() {
		return matrix.length;
	}
}
