package de.fhg.igd.interactionanalysis.core.data;

import java.util.HashMap;

import de.fhg.igd.interactionanalysis.core.utility.Config;

/**
 * This Singleton contains all interactions of the System and some functions 
 * for using them. In particular this class is used for mapping the extern 
 * string representation to a unique number which identifies an interaction.
 * 
 * @author Christian Stab
 */
public class Interactions {

	  /** the instance of this singletong */
	  private static Interactions instance;
	  
	  /** the extern string representation of the interactions */  
	  private String[][] interactions;
	  
	  /** HashMap for mapping names of interactions to numbers */
	  private HashMap<String, Integer> map;
		
	  /** reversed Mapping: from number to String */
	  private HashMap<Integer, String[]> reversedMap; 
	  
	  /**
	   * Basic Constructor
	   */
	  private Interactions() {
		  map = new HashMap<String, Integer>();
		  reversedMap = new HashMap<Integer, String[]>();
		  String[][] states = getInteractions();
		  for (int i=0; i<states.length; i++) {
			  map.put(getKey(states[i]), new Integer(i));
			  reversedMap.put(new Integer(i), states[i]);
		  }
	  }
	  
	  /**
	   * Returns the instance of this sinlgeton
	   * @return instance of this singleton
	   */
	  public static synchronized Interactions getInstance () {
	    if (Interactions.instance == null) {
	    	Interactions.instance = new Interactions ();
	    }
	    return Interactions.instance;
	  }
	  
	  /**
	   * Returns an array with the extern string representation of all interactions
	   * @return string representation of all interactions
	   */
	  public String[][] getInteractions() {
		  if (interactions==null) {
			  interactions = Config.getInstance(null).getInteractions();
			  //states = Permutator.getPermutations(Domains.getInstance().getAllLeaves()); 
		  }
		  return interactions;
	  }
	  
 
	  /**
	   * Returns the number of interactions
	   * @return number of interactions
	   */
	  public int getSize() {
		  return getInteractions().length;
	  }
	  
	  /**
	   * returns if the given String tuple is an interaction
	   * @param interaction the interaction
	   * @return true if input is an valid interaction; else it returns false
	   */
	  public boolean isInteraction(String[] interaction) {
		  if (interaction!=null) {
			  for (int i=0; i<interaction.length; i++) {
				  if (Domains.getInstance().getNode(interaction[i]).getChildren()!=null) {
					  return false;
				  } 
			  }
			  return true;
		  } else {
			  return false;
		  }
	  }
	  
	  /** 
	   * returns the index of an interaction
	   * @param interaction extern string representation of an interaction
	   * @return unique index
	   */
	  public int getIndex(String[] interaction) {
		  return map.get(getKey(interaction)).intValue();
	  }
		
	  /**
	   * returns the internal string representation of an interaction
	   * @param state state
	   * @return String
	   */
	  private String getKey(String[] state) {
		  String key = "";
		  for (int j=0; j<state.length;j++) {
			  key = key + "." + state[j];
		  }
		  return key;
	  }
	 
	  /**
	   * returns the extern representation of an interaction
	   * @param index intern representation of the interaction
	   * @return extern representation
	   */
	  public String[] getInteractionString(int index) {
		  return reversedMap.get(new Integer(index));
	  }
	  
}
