package de.fhg.igd.interactionanalysis.core.data;

/**
 * This class contains the steady state vector of a probabilistic model
 * 
 * @author Christian Stab
 */
public class SteadyStateVector {
	
	/** the steady state vector */
	private double[] vector;
	
	/** the indeces of the vector */
	private String[] indices;
	
	/**
	 * Basic Constructor
	 * @param size size of the vector
	 */
	public SteadyStateVector(int size) {
		vector = new double[size];
	}
	
	/**
	 * Basic Constructor 2
	 * @param vector size of the vector
	 */
	public SteadyStateVector(double[] vector) {
		this.vector = vector;
	}
	
	/**
	 * returns the probability of the given state
	 * @param state the state
	 * @return probability of the given state
	 */
	public double getProbability(int state) {
		return vector[state];
	}
	
	/**
	 * returns the probability od the given state
	 * @param state the state
	 * @return probability od the given state
	 */
	public double getProbability(String state) {
		for (int i=0; i<indices.length; i++) {
			if (indices[i].equals(state)) {
				return vector[i];
			}
		}
		return -1;
	}
	
	/**
	 * sets the probability of a state
	 * @param state the state
	 * @param value probability of the state
	 */
	public void setProbability(int state, double value) {
		vector[state]=value;
	}
	
	/**
	 * sets the indeces descriptions for every state
	 * @param indices names of states
	 */
	public void setIndices(String[] indices) {
		this.indices = indices; 
	}
	
	/**
	 * returns the string indeces of this vector
	 * @return the string indeces of this vector
	 */
	public String[] getIndices() {
		return indices;
	}
	
	/**
	 * returns the values of this vector
	 * @return the values of this vector
	 */
	public double[] getValues() {
		return vector;
	}
	
	/**
	 * returns the length of this vector
	 * @return the length of this vector
	 */
	public int getSize() {
		return vector.length;
	}

}
