package de.fhg.igd.interactionanalysis.core.data;

import java.util.ArrayList;

import de.fhg.igd.interactionanalysis.core.utility.Config;

/**
 * This Class represents the Domains of the System. It is used to construct
 * the interactions and to gather the abstractions of an interaction
 * 
 * @author Christian Stab
 */
public class Domains {
	 
	  /** the instance of the singleton */
	  private static Domains instance;
	  
	  /** the domains */
	  private DomainNode[] domains;
	  
	  /** Basic Constructor */
	  private Domains () {
		 domains = Config.getInstance(null).getDomains();
	  }
	  
	  /**
	   * If no Instance created so far, this method creates an instance
	   * and returns it. 
	   * @return the instance of Domains
	   */
	  public static synchronized Domains getInstance () {
		  if (Domains.instance == null) {
			  Domains.instance = new Domains ();
		  }
		  return Domains.instance;
	  }
	  
	  /**
	   * Returns the number of Domains
	   * @return number of domains
	   */
	  public int getSize() {
		  return domains.length;
	  }
	  
	  /**
	   * Returns all Nodes
	   *  @return returns all nodes
	   */
	  public String[][] getAllNodes() {
		  String nodes[][] = new String[getSize()][];
		  for (int i=0; i<domains.length; i++) {
			  nodes[i] = domains[i].getAllNodes();
		  }
		  return nodes;
	  }
	  
	  /**
	   * Returns a list with all leave nodes 
	   * @return Array of leave nodes
	   */
	  public String[][] getAllLeaves() {
		  String[][] leaves = new String[domains.length][];
		  for (int i=0; i<domains.length;i++) {
			  ArrayList<String> tmp = domains[i].getLeaves();
			  String[] domainLeaves = new String[tmp.size()];
			  leaves[i] = tmp.toArray(domainLeaves);
		  }
		  return leaves;
	  }
	  
	  /**
	   * Returns all nodes up to the specified rank
	   * @param ranks ranks of each domain
	   * @return all nodes up to the specified rank
	   */
	  public String[][] getNodesUpToRank(int[] ranks) {
		  String[][] nodes = new String[domains.length][];
		  for (int i=0; i<domains.length;i++) {
			  ArrayList<String> tmp = domains[i].getNodesUpToRank(ranks[i]);
			  String[] domainLeaves = new String[tmp.size()];
			  nodes[i] = tmp.toArray(domainLeaves);
		  }
		  return nodes;
	  }
	  
	  /**
	   * returns the node with the given name
	   * @param str name of the wanted node
	   * @return the node
	   */
	  public DomainNode getNode(String str) {
		  String[] names = str.split("\\.");
		  DomainNode d = null;
		  for (int j=0; j<domains.length; j++) {
			  if (domains[j].getName().equals(names[0])) {
				  d = domains[j];
			  }
		  } 
		  if(d!=null) {
			  for (int i=1; i<names.length; i++) {
				 d=d.getChild(names[i]);
			  }
			  return d;
		  } else {
			  return null;
		  }
	  }
	  
	  /**
	   * returns all parents of each argument of a state relation
	   * @param state the state
	   * @return all parents of each argument
	   */
	  public String[][] getAllParents(String[] state) {
		  String[][] parents = new String[domains.length][];
		  for (int i=0; i<domains.length; i++) {
			  parents[i] = getNode(state[i]).getAllParents();
		  }
		  return parents;
	  }

}
