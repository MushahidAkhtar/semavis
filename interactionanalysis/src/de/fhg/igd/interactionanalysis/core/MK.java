package de.fhg.igd.interactionanalysis.core;

import java.util.List;

import de.fhg.igd.interactionanalysis.core.data.Interactions;
import de.fhg.igd.interactionanalysis.core.data.TransitionMatrix;

/**
 * Implementation of markov chain
 * @author Christian Stab
 */
public class MK extends PM {

	/** its serial number*/
	private static final long serialVersionUID = -7149146981873041924L;

	/** the counts of transitions from state to state */
	private int[][] transitionCounts;
	
	/** counts of a state */
	private int[] counts;
	
	/** Counts of the initial state */
	private int[] initialCounts;
	
	/** count the number of seen initial observations */ 
	private int initialCount;
	
	/**
	 * Basic Constructor
	 */
	public MK() {
		super();
		int numberOfStates = Interactions.getInstance().getSize();
		stm = new TransitionMatrix(numberOfStates);
		transitionCounts = new int[numberOfStates][numberOfStates];
		counts = new int[numberOfStates];
		initialCounts = new int[numberOfStates];
		initialCount=0;
	}
	
	/**
	 * Updates the probabilities if a new observation occures
	 * @param obs observation
	 */
	public void newObservation(int obs) {
		//int observation = States.getInstance().getIndex(obs);
		if (previousObservation==-1) {
			previousObservation = obs;
			updateInitialProbabilies(obs);
		} else {
			transitionCounts[previousObservation][obs]++;
			counts[previousObservation]++;
			for (int i=0; i<stm.getSize(); i++) {
				stm.setProbability(previousObservation, i, (double)transitionCounts[previousObservation][i]/(double) counts[previousObservation]);
			}
			previousObservation = obs;
		}
	}
	
	/**
	 * Updates the probabilities if new observations occures
	 * @param obs observations
	 */
	public void newObservations(int[] obs) {
		for (int i=0; i<obs.length; i++) {
			newObservation(obs[i]);
		}
	}
	
	/**
	 * updates the initial probabilities
	 */
	protected void updateInitialProbabilies(int firstObservation) {
		initialCounts[firstObservation]++;
		initialCount++;
		for (int i=0; i<initProb.length; i++) {
			initProb[i] = ((double)initialCounts[i]) / ((double)initialCount);
		}
		
	}
	
	/**
	 * resets the model
	 */
	public void reset() {
		int numberOfStates = Interactions.getInstance().getSize();
		transitionCounts = new int[numberOfStates][numberOfStates];
		counts = new int[numberOfStates];
	}

	@Override
	public TransitionMatrix getTransitionMatrix() {
		return stm;
	}

	/**
	 * Calculates a ranking for the sequence beloning to this chain 
	 * @param sequence the sequence
	 * @return ranking
	 */
	public double getSeqRanking(List<Integer> sequence) {
		double prob = 0.0d;
		for (int i=0; i<sequence.size(); i++) {
			if (i==0) {
//				if (initProb[sequence.get(i)]==0 && counts[sequence.get(i)]!=0) {
//					prob++;
//				} else {
					prob = initProb[sequence.get(i)];
//				}
			} else {
				if (prob==0) {
					prob = stm.getProbability(sequence.get(i-1), sequence.get(i));
				} else {
					prob = prob * stm.getProbability(sequence.get(i-1), sequence.get(i));
				}
				
			}
		}
		
		return prob;
	}
	
	@Override
	public void setTrainingsData(int[] data) {
		for (int i=0; i<data.length; i++) {
			newObservation(data[i]);
		}
	}
	
}
