package de.fhg.igd.interactionanalysis.core;

import de.fhg.igd.interactionanalysis.core.data.TransitionMatrix;

/**
 * Implementation of Incremental probabilistic Action Modeling (IPAM)
 * 
 * @author Christian Stab
 */
public class IPAM extends PM {
	
	/** its serial number*/
	private static final long serialVersionUID = -885324053262638645L;
	
	/** Alpha as forgetting-factor */
	private double alpha = 0.8d;
	
	/**
	 * Basic Constructor
	 */
	public IPAM() {
		super();
		//alpha = Config.getInstance(null).getIPAMParameter();
	}
	
	/**
	 * updates the initial probabilities
	 */
	protected void updateInitialProbabilies(int firstObservation) {
		for (int i=0; i<initProb.length; i++) {
			initProb[i] = initProb[i] * alpha;
		}
		initProb[firstObservation] = initProb[firstObservation] + (double)(1.0f-alpha);
	}
	
	/**
	 * Updates the probabilities if a new observation occures
	 * @param obs observation
	 */
	public void newObservation(int obs) {
		//int observation = States.getInstance().getIndex(obs);
		if (previousObservation==-1) {
			previousObservation = obs;
			updateInitialProbabilies(obs);
		} else {
			// update this with ipam
			double[] vector = stm.getVector(previousObservation);
			for (int i=0; i<vector.length; i++) {
				vector[i] = vector[i] * alpha;
			}
			stm.setVector(previousObservation, vector);
			stm.setProbability(previousObservation, obs, stm.getProbability(previousObservation, obs) + (double)(1.0f-alpha));
			previousObservation = obs;
		}
	}
	
	/**
	 * Updates the probabilities if new observations occures
	 * @param obs observations
	 */
	public void newObservations(int[] obs) {
		for (int i=0; i<obs.length; i++) {
			newObservation(obs[i]);
		}
	}

	@Override
	public TransitionMatrix getTransitionMatrix() {
		return stm;
	}

	@Override
	public void setTrainingsData(int[] data) {
		for (int i=0; i<data.length; i++) {
			newObservation(data[i]);
		}
	}
}
