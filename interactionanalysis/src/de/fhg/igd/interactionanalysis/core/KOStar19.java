package de.fhg.igd.interactionanalysis.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.fhg.igd.interactionanalysis.core.data.Abstractions;
import de.fhg.igd.interactionanalysis.core.data.QueryResult;
import de.fhg.igd.interactionanalysis.core.data.Interactions;
import de.fhg.igd.interactionanalysis.core.data.SteadyStateVector;
import de.fhg.igd.interactionanalysis.core.data.roles.IRole;
import de.fhg.igd.interactionanalysis.core.data.roles.RoleImpl;
import de.fhg.igd.interactionanalysis.core.utility.InteractionTranslator;

/**
 * This class implements the KO* /19 algorithm with simple roles.
 * 
 * @author Christian Stab
 */
public class KOStar19 implements IRolePM, IPM, Serializable{

	/** its serial number */
	private static final long serialVersionUID = -8528996924274139374L;

	/** trainings data containing all interactions */
	private List<Integer> trainingData;
	
	/** the indices of every occurrence of an interaction in the trainingsdata */
	private List<Integer>[] occurrence;
	
	/** actual probabilities */
	private double[] result;
	
	/** indicates if a new interaction occurres. If true new results must be calculated */
	private boolean newObservation;

	/** the count for every state*/
	private int[] interactionCounts;
	
	/** count of all observations */
	private int interactions;
	
	/** length of the longest sequence */
	private int maxLength;
	
	/** startindex of the actual sequence */
	private int startIndex;
	
	/** startindex of the actual role */
	private int roleStartIndex;
	
	/** the minimum size of role interactions */
	private int minRoleSize = 2;
	
	/** Threshold for role creation */
	private double roleThreshold = 0.0d;
	
	/** role interactions */
	private ArrayList<Integer> roleList;
	
	/** the roles of this model */
	private List<IRole> roles;
	
	/** the active role */
	private IRole activeRole;
	
	
	/** is role learning enabled */ 
	private boolean roleLearning;
	
	/**
	 * Basic Constructor
	 */
	@SuppressWarnings("unchecked")
	public KOStar19() {
		this.trainingData = new ArrayList<Integer>();
		
		occurrence = new ArrayList[Interactions.getInstance().getSize()];
		interactionCounts = new int[Interactions.getInstance().getSize()];
		roles = new ArrayList<IRole>();
		newObservation = true;
		roleList = new ArrayList<Integer>();
		roleLearning=false;
		interactions = 0;
		for (int i=0; i<occurrence.length; i++) {
			occurrence[i] = new ArrayList<Integer>();
		}
	}
	
	/**
	 * calculates the probability distribution
	 * @return the probability distribution
	 */
	private double[] calcPrediction() {
		if (newObservation) {
			maxLength = 0;
			result = new double[Interactions.getInstance().getSize()];
			double count = 0.0d;
		
			// f�r jeden Zustand
			for (int state=0; state<Interactions.getInstance().getSize(); state++) {
			
				List<Integer> occurrence = getOccurence(state);
				
				trainingData.add(state);
				for (int i=0; i<occurrence.size(); i++) {
					double length = calcSeqLength(state, occurrence.get(i));
				
					result[state] = result[state] + length * w(length);
				}
				count = count + result[state];
				trainingData.remove(trainingData.size()-1);
			}
			if (roleLearning) {
				learnRoles();
			}
			
			// normalize
			for (int i=0; i< result.length; i++) {
				result[i] = result[i]/count;
			}
			
			newObservation=false;
			return result;
		} else {
			return result;
		}
	}
	
	/**
	 * compares two sequences in the trainingsdata. The first sequence begins at the 
	 * last element of the trainingsdata and the second sequence begins at the given index.
	 * It returns the length of the matched sequence.
	 * @param index occurence of the tested interaction in the trainingsdata (Must be the same
	 * Interaction as the last element of the trainingsdata)
	 * @return the length of the matching sequence
	 */
	private double calcSeqLength(int state, int index) {
		int i = 0;
		
		while (Math.min(index-i, trainingData.size()-i-1)>=0 && trainingData.size()-i-1>index && trainingData.get(index-i).intValue()==trainingData.get(trainingData.size()-1-i).intValue()) {
			i++;
		}
		if (i>maxLength) {
			maxLength = i-1;
			startIndex = index - i+1;
		}
		
		return i;
	}
	
	/**
	 * the weightening function 
	 * @param i length of a sequence
	 * @return the weight for a given length
	 */
	private double w(double i) {
		return Math.pow(i, 19);
	}
	
	/**
	 * learn the roles. checks in which role the user is and
	 * sets the actual role
	 */
	private void learnRoles() {
		// get actual role sequence
		roleList.clear();
		for (int i=startIndex; i<startIndex+maxLength; i++) {
			roleList.add(trainingData.get(i));
		}
		
		//if (maxLength>=minRoleSize) {
			if (roleStartIndex!=startIndex) {
				// ### change Role
				//System.out.println("### Role Changed");
				roleStartIndex = startIndex;
			}
		//} else {
			//activeRole = null;
		//}
		
		
		ArrayList<Integer> tmp = new ArrayList<Integer>(); 
		for (int i=0; i<roleList.size(); i++) {
			tmp.add(roleList.get(i));
		}
		
		if (!tmp.isEmpty()) {
			// find matching role
			double bestMatch = roleThreshold;
			int bestRoleIndex = -1;
			for (int i=0; i<roles.size(); i++) {
				double roleRank = roles.get(i).match(tmp);
				if (roleRank>bestMatch) {
					bestMatch = roleRank;
					bestRoleIndex = i;
				}
			}
			
			if (bestRoleIndex!=-1) {
				roles.get(bestRoleIndex).newRoleSequence(tmp);
				activeRole = roles.get(bestRoleIndex); 
			} else {
				if (tmp.size()>=minRoleSize) {
					IRole newRole = new RoleImpl();
					newRole.newRoleSequence(tmp);
					roles.add(newRole);
					activeRole = roles.get(roles.size()-1);
				}
			}
		} else {
			activeRole = null;
		}
	}
	
	/**
	 * returns the occurences of a given state
	 * @param state the state
	 * @return the occurences of a given state
	 */
	private List<Integer> getOccurence(int state) {
		return occurrence[state];
	}
	
	
	@Override
	public void newObservation(int observation) {
		newObservation=true;
		trainingData.add(new Integer(observation));
		
		occurrence[observation].add(trainingData.size()-1); 
		calcPrediction();
		
		interactionCounts[observation]++;
		interactions++;
	}
	
	@Override
	public void newObservations(int[] obs) {
		for (int i=0; i<obs.length; i++) {
			newObservation(obs[i]);
		}
	}
	
	@Override
	public int getNextAction() {
		double[] vector = calcPrediction();
		int nextAction = -1;
		double prob = 0.0f;
		
		for (int i=0; i<vector.length; i++) {
			if (vector[i]>prob) {
				nextAction = i;
				prob = vector[i];
			} 
		}
		return nextAction;
	}
	
	@Override
	public double getPredictionProbability(int from, int to) {
		Double result =  new Double(calcPrediction()[to]);
		if (result.isNaN()) {
			return 0.0d;
		} else {
			return result;
		}
		
	}
	
	@Override
	public int[] getNextActions(int count){
		int[] result = new int[count];
		double lastMax = Double.POSITIVE_INFINITY;
		int lastAction = -1;
		
		// initialize result
		for (int i=0; i<count; i++) {
			result[i] = -1;
		}
		
		double[] stateTrans = calcPrediction();
		for (int i=0; i<count; i++) {
			double tmp = 0.0f;
			for (int j=0; j<stateTrans.length; j++) {
				if (stateTrans[j]>tmp && stateTrans[j]<lastMax) {
					tmp = stateTrans[j];
					lastAction = j;
				}
			}
			lastMax = tmp;
			result[i] = lastAction;
		}
		return result;
	}
	
	@Override
	public int getPredictionRank(int from, int to) {
		double[] v = calcPrediction();
		double toProb = v[to];
		int rank = 1;
		for (int i=0; i<v.length; i++) {
			Double test = new Double(v[i]);
			if ((v[i]>=toProb && to!=i) || test.isNaN()) {
				rank++;
			}
		}
		return rank;
	}
	
	@Override
	public QueryResult query(int[] ranks) {
		String[][] rankAbstractions = Abstractions.getInstance().getRankAbstractions(ranks);
		SteadyStateVector ssv = getSteadyStateVector();
		double[] result = new double[rankAbstractions.length];
		
		for (int i=0; i<result.length; i++) {
			int[] rankAbstractionStates = Abstractions.getInstance().getAbstractionStates(rankAbstractions[i]);
			double prob = 0.0f;
			for (int j=0; j<rankAbstractionStates.length; j++) {
				prob = prob + ssv.getValues()[rankAbstractionStates[j]];
			}
			result[i] = prob;
		}
		
		return new QueryResult(result, InteractionTranslator.translateInteractions(rankAbstractions));
	}
	
	@Override
	public SteadyStateVector getSteadyStateVector() {
		SteadyStateVector ssv = new SteadyStateVector(Interactions.getInstance().getSize());
		ssv.setIndices(InteractionTranslator.translateInteractions(Interactions.getInstance().getInteractions()));
		
		for (int i=0; i<Interactions.getInstance().getSize(); i++) {
			ssv.setProbability(i, ((double)interactionCounts[i])/((double)interactions));
		}
		return ssv;
	}
	
	@Override
	public void setTrainingsData(int[] data) {
		for (int i=0; i<data.length; i++) {
			trainingData.add(new Integer(data[i]));
			occurrence[data[i]].add(trainingData.size()-1); 
		}
	}
	
	@Override
	public void enableRoleLearning(boolean b) {
		roleLearning = true;
	}
	
	@Override
	public int getRoleCounts() {
		return roles.size();
	}
	
	@Override
	public int getActiveRole() {
		for (int i=0; i<roles.size(); i++) {
			if (roles.get(i)==activeRole) {
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public int[][] getRoleSequences(int role) {
		IRole rol = roles.get(role);
		List<List<Integer>> sequences = rol.getSequences();
		int[][] result = new int[sequences.size()][];
		for (int i=0; i<result.length; i++) {
			result[i] = new int[sequences.get(i).size()];
			for (int j=0; j<result[i].length; j++) {
				result[i][j] = sequences.get(i).get(j);
			}
		}
		
		return result;
	}
	
	@Override
	public SteadyStateVector getRoleSSV(int roleIndex) {
		return roles.get(roleIndex).getSteadyStateVector();
	}
	
	@Override
	public QueryResult queryRole(int[] ranks, int roleIndex) {
		return roles.get(roleIndex).query(ranks);
	}
}
