package de.fhg.igd.interactionanalysis.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.fhg.igd.interactionanalysis.core.data.Abstractions;
import de.fhg.igd.interactionanalysis.core.data.QueryResult;
import de.fhg.igd.interactionanalysis.core.data.Interactions;
import de.fhg.igd.interactionanalysis.core.data.SteadyStateVector;
import de.fhg.igd.interactionanalysis.core.utility.InteractionTranslator;

/**
 * This class implements a probabilistic model using the LEV-Algorithm
 * 
 * @author Chistian Stab 
 */
public class LEV implements IPM,Serializable {

	/** its serial Number*/
	private static final long serialVersionUID = 7801164604474526422L;
	
	/** the counts of every interaction */
	private int[] interactionCounts;
	
	/** count of al observations */
	private int interactions;
	
	/** trainings Data */
	private List<Integer> trainingData;
	
	/** markov order for LEV */
	private static int markovOrder = 3;
	
	/**
	 * Basic Constructor
	 */
	public LEV() {
		super();
		this.trainingData = new ArrayList<Integer>();
		interactionCounts = new int[Interactions.getInstance().getSize()];
		interactions = 0;
	}
	
	
	/**
	 * calculates the probability distribution with Lev algorithm
	 * @return probability distribution
	 */
	private double[] calcPrediction() {
		//System.out.println("CalcPrediction with " + trainingData.size() + " coutns of trainingsdata");
		int predLength = Math.min(markovOrder, trainingData.size());
		boolean hit = false;
		double[] result = new double[Interactions.getInstance().getSize()];
		
		double count = 0.0d;
		
		for(int i=0; i<predLength+1; i++) {
			// mit dieser Sequenz wird verglichen
			List<Integer> sequence = trainingData.subList(trainingData.size()-predLength, trainingData.size());
			sequence.add(new Integer(0));
			
			for (int j=0; j<Interactions.getInstance().getSize(); j++) {
				sequence.set(sequence.size()-1, new Integer(j));
				
				result[j] = seqCount(sequence,i);
				
				// falls eine Übereinstimmung gefunden wurde
				// breche nach diesem Durchgang ab
				count = count + result[j];
				if (result[j]!=0) hit = true;
			}
			
			// entferne hinzugefügtes element wieder
			trainingData.remove(trainingData.size()-1);
			
			if (hit) {
				break;
			}
		}
		
		// normalize
		for (int i=0; i< result.length; i++) {
			result[i] = result[i]/count;
		}
		return result;
	}
	
	/**
	 * counts the occurences of the given sequence and levenshtein distance
	 * in the trainingsdata
	 * @param o sequence
	 * @param levDist levenshtein distacen
	 * @return counts
	 */
	private int seqCount(List<Integer> o, int levDist) {
		int result = 0;
		
		
		for (int i=0;i<trainingData.size()-(2*(o.size()))+1; i++) {
			List<Integer> subseq = trainingData.subList(i, i+o.size());
			if (calcLevenshtein(subseq, o)==levDist) {
				result++;
			}
		}
		
//		for (int i=0;i<trainingData.size()-o.size(); i++) {
//			List<Integer> subseq = trainingData.subList(i, i+o.size());
//			if (calcLevenshtein(subseq, o)==levDist) {
//				result++;
//			}
//		}
		return result;
	}
	
	/**
	 * calculates the levenshtein distance of the given sequences 
	 * @param s sequence 1
	 * @param t sequence 2
	 * @return levenshtein distance
	 */
	private int calcLevenshtein(List<Integer> s, List<Integer> t) {
		int[][] d = new int[s.size()+1][t.size()+1];
		int cost = 0;
		
		for (int i=0; i<s.size()+1; i++) {
			d[i][0] = i;
		}
		for (int j=0; j<t.size()+1; j++) {
			d[0][j] = j;
		}
		for (int i=1; i<s.size()+1; i++) {
			for (int j=1; j<t.size()+1; j++) {
				if (s.get(i-1).intValue()==t.get(j-1).intValue()) {
					cost = 0;
				} else {
					cost = 1;
				}
				d[i][j] = Math.min(Math.min(d[i-1][j]+1, d[i][j-1]+1), d[i-1][j-1] + cost);
			}
		}
		return d[s.size()][t.size()];
	}
	
	@Override
	public void newObservation(int obs) {
		trainingData.add(new Integer(obs));
		interactionCounts[obs]++;
		interactions++;
	}

	@Override
	public void newObservations(int[] obs) {
		for (int i=0; i<obs.length; i++) {
			newObservation(obs[i]);
		}
	}

	@Override
	public int getNextAction() {
		double[] vector = calcPrediction();
		int nextAction = -1;
		double prob = 0.0f;
		
		for (int i=0; i<vector.length; i++) {
			if (vector[i]>prob) {
				nextAction = i;
				prob = vector[i];
			} 
		}
		return nextAction;
	}
	
	@Override
	public int[] getNextActions(int count){
		int[] result = new int[count];
		double lastMax = Double.POSITIVE_INFINITY;
		int lastAction = -1;
		
		// initialize result
		for (int i=0; i<count; i++) {
			result[i] = -1;
		}
		
		double[] stateTrans = calcPrediction();
		for (int i=0; i<count; i++) {
			double tmp = 0.0f;
			for (int j=0; j<stateTrans.length; j++) {
				if (stateTrans[j]>tmp && stateTrans[j]<lastMax) {
					tmp = stateTrans[j];
					lastAction = j;
				}
			}
			lastMax = tmp;
			result[i] = lastAction;
		}
		return result;
	}
	
	@Override
	public int getPredictionRank(int from, int to) {
		double[] v = calcPrediction();
		double toProb = v[to];
		int rank = 1;
		for (int i=0; i<v.length; i++) {
			Double test = new Double(v[i]);
			if ((v[i]>=toProb && to!=i) || test.isNaN()) {
				rank++;
			}
		}
		return rank;
	}
	
	@Override
	public double getPredictionProbability(int from, int to) {
		Double result =  new Double(calcPrediction()[to]);
		if (result.isNaN()) {
			return 0.0d;
		} else {
			return result;
		}
	}

	@Override
	public QueryResult query(int[] ranks) {
		//String[][] rankNodes = Domains.getInstance().getNodesUpToRank(ranks);
		String[][] rankAbstractions = Abstractions.getInstance().getRankAbstractions(ranks);
		SteadyStateVector ssv = getSteadyStateVector();
		double[] result = new double[rankAbstractions.length];
		
		for (int i=0; i<result.length; i++) {
			int[] rankAbstractionStates = Abstractions.getInstance().getAbstractionStates(rankAbstractions[i]);
			double prob = 0.0f;
			for (int j=0; j<rankAbstractionStates.length; j++) {
				prob = prob + ssv.getValues()[rankAbstractionStates[j]];
			}
			result[i] = prob;
		}
		
		return new QueryResult(result, InteractionTranslator.translateInteractions(rankAbstractions));
	}
	
	@Override
	public SteadyStateVector getSteadyStateVector() {
		SteadyStateVector ssv = new SteadyStateVector(Interactions.getInstance().getSize());
		ssv.setIndices(InteractionTranslator.translateInteractions(Interactions.getInstance().getInteractions()));
		
		for (int i=0; i<Interactions.getInstance().getSize(); i++) {
			ssv.setProbability(i, ((double)interactionCounts[i])/((double)interactions));
		}
		
		return ssv;
	}
	
	@Override
	public void setTrainingsData(int[] data) {
		for (int i=0; i<data.length; i++) {
			trainingData.add(new Integer(data[i]));
		}
	}
	
}
