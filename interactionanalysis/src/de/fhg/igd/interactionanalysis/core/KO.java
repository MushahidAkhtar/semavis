package de.fhg.igd.interactionanalysis.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import de.fhg.igd.interactionanalysis.core.data.Abstractions;
import de.fhg.igd.interactionanalysis.core.data.QueryResult;
import de.fhg.igd.interactionanalysis.core.data.Interactions;
import de.fhg.igd.interactionanalysis.core.data.SteadyStateVector;
import de.fhg.igd.interactionanalysis.core.utility.InteractionTranslator;

/**
 * This class implement a probabilistic model using the KO-Algorithm
 * @author Christian Stab
 */
public class KO implements IPM, Serializable {

	/** its serial Number*/
	private static final long serialVersionUID = 7801164604474526422L;
	
	/** the counts of every interaction */
	private int[] interactionCounts;
	
	/** count of al observations */
	private int interactions;
	
	/** trainings Data */
	private List<Integer> trainingData;
	
	/** maximum Sequence length of KO-algorithm */
	private int searchDepth=3;
	
	private int minFreq =0;
	
	/**
	 * Basic Constructor
	 */
	public KO() {
		super();
		this.trainingData = new ArrayList<Integer>();
		interactionCounts = new int[Interactions.getInstance().getSize()];
		interactions = 0;
	}
	
	
	/**
	 * calculates the probability distribution for the next action
	 * @return the probability distribution for the next action
	 */
	public double[] calcPrediction() {
		double[] result = new double[Interactions.getInstance().getSize()];
		double count = 0.0d;
		
		for (int a=0; a<Interactions.getInstance().getSize(); a++) {
			ArrayList<Integer> tmp = new ArrayList<Integer>();
			tmp.add(new Integer(a));
			result[a] = subseqCount(tmp) * w(0);
			
			//for (int i=1; i<Math.min(trainingData.size(), searchDepth); i++)
			for (int i=1; i<Math.min(trainingData.size(), searchDepth+1); i++) {
				tmp.clear();
				tmp.addAll(0, trainingData.subList(trainingData.size()-i, trainingData.size()));
				tmp.add(new Integer(a));
				result[a] = result[a] + subseqCount(tmp) * w(i);
			}
			
			// inc count
			count = count + result[a];
		}
		
		// normalize
		for (int i=0; i< result.length; i++) {
			result[i] = result[i]/count;
		}
		return result;
	}
	
	/**
	 * count the occurences of o in the trainingsdata
	 * @param o sequence which is counted
	 * @return occurences
	 */
	public int subseqCount(ArrayList<Integer> o) {
		int result = 0;
		//for (int i=0;i<trainingData.size()-o.size(); i++)
		for (int i=0;i<trainingData.size()-(2*o.size())+2; i++) {
			List<Integer> subseq = trainingData.subList(i, i+o.size());
			if (same(o,subseq)) {
				result++;
			}
		}
		if (result<minFreq) result =0;
		return result;
	}
	
	/**
	 * the weightening function
	 * @param i length of a sequence
	 * @return weighted result
	 */
	private double w(int i) {
		return Math.pow(i, 19);
	}
	
	/**
	 * compares two sequences
	 * @param a sequence 1
	 * @param b sequence 2
	 * @return true if sequences are the same
	 */
	private boolean same(List<Integer> a, List<Integer> b) {
		if (a.size() != b.size()) return false;
		for (int i=0; i<a.size(); i++) {
			if (a.get(i).intValue() != b.get(i).intValue()) return false;
		}
		return true;
	}
	
	@Override
	public void newObservation(int obs) {
		trainingData.add(new Integer(obs));
		interactionCounts[obs]++;
		interactions ++;
	}

	@Override
	public void newObservations(int[] obs) {
		for (int i=0; i<obs.length; i++) {
			newObservation(obs[i]);
		}
	}

	@Override
	public int getNextAction() {
		double[] vector = calcPrediction();
		int nextAction = -1;
		double prob = 0.0f;
		
		for (int i=0; i<vector.length; i++) {
			if (vector[i]>prob) {
				nextAction = i;
				prob = vector[i];
			} 
		}
		return nextAction;
	}
	
	@Override
	public int[] getNextActions(int count){
		int[] result = new int[count];
		double lastMax = Double.POSITIVE_INFINITY;
		int lastAction = -1;
		
		// initialize result
		for (int i=0; i<count; i++) {
			result[i] = -1;
		}
		
		double[] stateTrans = calcPrediction();
		for (int i=0; i<count; i++) {
			double tmp = 0.0f;
			for (int j=0; j<stateTrans.length; j++) {
				if (stateTrans[j]>tmp && stateTrans[j]<lastMax) {
					tmp = stateTrans[j];
					lastAction = j;
				}
			}
			lastMax = tmp;
			result[i] = lastAction;
		}
		return result;
	}
	
	@Override
	public int getPredictionRank(int from, int to) {
		double[] v = calcPrediction();
		double toProb = v[to];
		int rank = 1;
		for (int i=0; i<v.length; i++) {
			Double test = new Double(v[i]);
			if ((v[i]>=toProb && to!=i) || test.isNaN()) {
				rank++;
			}
		}
		return rank;
	}
	
	@Override
	public double getPredictionProbability(int from, int to) {
		Double result =  new Double(calcPrediction()[to]);
		if (result.isNaN()) {
			return 0.0d;
		} else {
			return result;
		}
	}
	
	@Override
	public QueryResult query(int[] ranks) {
		//String[][] rankNodes = Domains.getInstance().getNodesUpToRank(ranks);
		String[][] rankAbstractions = Abstractions.getInstance().getRankAbstractions(ranks);
		SteadyStateVector ssv = getSteadyStateVector();
		double[] result = new double[rankAbstractions.length];
		
		for (int i=0; i<result.length; i++) {
			int[] rankAbstractionStates = Abstractions.getInstance().getAbstractionStates(rankAbstractions[i]);
			double prob = 0.0f;
			for (int j=0; j<rankAbstractionStates.length; j++) {
				prob = prob + ssv.getValues()[rankAbstractionStates[j]];
			}
			result[i] = prob;
		}
		
		return new QueryResult(result, InteractionTranslator.translateInteractions(rankAbstractions));
	}
	
	@Override
	public SteadyStateVector getSteadyStateVector() {
		SteadyStateVector ssv = new SteadyStateVector(Interactions.getInstance().getSize());
		ssv.setIndices(InteractionTranslator.translateInteractions(Interactions.getInstance().getInteractions()));
		
		for (int i=0; i<Interactions.getInstance().getSize(); i++) {
			ssv.setProbability(i, ((double)interactionCounts[i])/((double)interactions));
		}
		
		return ssv;
	}
	
	@Override
	public void setTrainingsData(int[] data) {
		for (int i=0; i<data.length; i++) {
			trainingData.add(new Integer(data[i]));
		}
	}

}
