package de.fhg.igd.interactionanalysis.core;

import java.io.Serializable;

import de.fhg.igd.interactionanalysis.core.data.Abstractions;
import de.fhg.igd.interactionanalysis.core.data.QueryResult;
import de.fhg.igd.interactionanalysis.core.data.Interactions;
import de.fhg.igd.interactionanalysis.core.data.SteadyStateVector;
import de.fhg.igd.interactionanalysis.core.data.TransitionMatrix;
import de.fhg.igd.interactionanalysis.core.utility.InteractionTranslator;
import de.fhg.igd.interactionanalysis.core.utility.SSVCalculator;

/**
 * This is the Interface for every probabilistic Model
 * 
 * @author Christian Stab
 *
 */
public abstract class PM implements IPM,Serializable {
	
	/** its serial number for serialisation */
	private static final long serialVersionUID = 613110022563789770L;

	/** previous Observation */
	protected int previousObservation;
	
	/** Its StateTransitionMatrix */
	protected TransitionMatrix stm;
	
	/** Initial probabilities */
	protected double[] initProb;
	
	/**
	 * Basic Constructor
	 */
	public PM() {
		stm = new TransitionMatrix(Interactions.getInstance().getSize());
		initProb = new double[Interactions.getInstance().getSize()];
		initializeInitProb();
		stm.initialize();
		previousObservation = -1;
	}
	
	/**
	 * initializes the inital probabilities with equal probabilities
	 */
	private void initializeInitProb() {
		double init = 1.0f / (double) Interactions.getInstance().getSize();
		for (int i=0; i<initProb.length; i++) {
			initProb[i]=init;
		}
	}
	
	/**
	 * returns true if intial probabilities were changed before
	 * @return true if intial probabilities were changed before
	 */
	private boolean isInitProbInitialized() {
		double init = 1.0f / (double) Interactions.getInstance().getSize();
		for (int i=0; i<initProb.length; i++) {
			if (initProb[i]!=init) return true;
		}
		return false;
	}
	
	/**
	 * returns the most possible initial action
	 * @return the most possible initial action
	 */
	private int getInitialAction() {
		int state = -1;
		double actualMax = 0.0f;
		for (int i=0; i<initProb.length; i++) {
			if (initProb[i]>actualMax) {
				state = i;
				actualMax = initProb[i];
			}
		}
		return state;
	}
	
	/**
	 * updates the initial probabilities
	 */
	protected abstract void updateInitialProbabilies(int firstObservation);
	
	/**
	 * Updates the probabilities if a new observation occures
	 * @param obs observation
	 */
	public abstract void newObservation(int obs);
	
	/**
	 * Updates the probabilities if new observations occures
	 * @param obs observations
	 */
	public abstract void newObservations(int[] obs);
	
	/**
	 * Returns the StateTransitionMatrix
	 * @return StateTransitionMatrix
	 */
	public abstract TransitionMatrix getTransitionMatrix();
		
	
	/**
	 * returns the next Action depending on the acual state of the model
	 * @return next state
	 */
	public int getNextAction() {
		double[][] matrix = getTransitionMatrix().getMatrix();
		int nextAction = -1;
		double prob = 0.0f;
		
		if (previousObservation==-1 && isInitProbInitialized()) {
			return getInitialAction();
		}
		
		if (previousObservation!=-1 && getTransitionMatrix().stateSeen(previousObservation)) {
			for (int i=0; i<matrix.length; i++) {
				if (matrix[previousObservation][i]>prob) {
					nextAction = i;
					prob = matrix[previousObservation][i];
				} 
			}
		}
		return nextAction;
	}
	
	/**
	 * returns the next most possible actions
	 * @param count number of actions
	 * @return next most possible actions 
	 */
	public int[] getNextActions(int count) {
		int[] result = new int[count];
		double lastMax = Double.POSITIVE_INFINITY;
		int lastAction = -1;
		
		// initialize result
		for (int i=0; i<count; i++) {
			result[i] = -1;
		}
		
		int actualState = -1;
		
		if (previousObservation==-1 && isInitProbInitialized()) {
			actualState = getInitialAction();
		} else {
			actualState = previousObservation;
		}
		
		if (actualState!=-1 && getTransitionMatrix().stateSeen(actualState)) {
			double[] stateTrans = getTransitionMatrix().getVector(actualState);
			for (int i=0; i<count; i++) {
				double tmp = 0.0f;
				for (int j=0; j<stateTrans.length; j++) {
					if (stateTrans[j]>tmp && stateTrans[j]<lastMax) {
						tmp = stateTrans[j];
						lastAction = j;
					}
				}
				lastMax = tmp;
				result[i] = lastAction;
			}
		} 
		return result;
	}
	
	/**
	 * returns the rank of a prediction
	 * @param from source state
	 * @param to target state
	 * @return rank
	 */
	public int getPredictionRank(int from, int to) {
		double[] v = getTransitionMatrix().getMatrix()[from];
		double toProb = v[to];
		int rank = 1;
		for (int i=0; i<v.length; i++) {
			if (v[i]>=toProb && to!=i) {
				rank++;
			}
		}
		return rank;
	}
	
	/**
	 * returns the probability of a prediction
	 * @param from source state
	 * @param to target state
	 * @return rank
	 */
	public double getPredictionProbability(int from, int to) {
		if (from==-1) {
			return getTransitionMatrix().getMatrix()[previousObservation][to];
		}
		return getTransitionMatrix().getMatrix()[from][to];
	}
	
	/**
	 * resets the model
	 */
	public void reset() {
		stm.initialize();
	}
	
	/**
	 * returns the steady state vector of this model
	 */
	public SteadyStateVector getSteadyStateVector() {
		return SSVCalculator.getSSV(getTransitionMatrix());
	}
	
	/**
	 * resets the previous observation
	 */
	public void resetPreviousObservation() {
		previousObservation = -1;
	}
	
	/**
	 * returns the results of a query
	 * @param ranks ranks of the query
	 * @return QueryResult
	 */
	public QueryResult query(int[] ranks) {
		//String[][] rankNodes = Domains.getInstance().getNodesUpToRank(ranks);
		String[][] rankAbstractions = Abstractions.getInstance().getRankAbstractions(ranks);
		SteadyStateVector ssv = getSteadyStateVector();
		double[] result = new double[rankAbstractions.length];
		
		for (int i=0; i<result.length; i++) {
			int[] rankAbstractionStates = Abstractions.getInstance().getAbstractionStates(rankAbstractions[i]);
			double prob = 0.0f;
			for (int j=0; j<rankAbstractionStates.length; j++) {
				prob = prob + ssv.getValues()[rankAbstractionStates[j]];
			}
			result[i] = prob;
		}
		
		return new QueryResult(result, InteractionTranslator.translateInteractions(rankAbstractions));
	}
	
}
