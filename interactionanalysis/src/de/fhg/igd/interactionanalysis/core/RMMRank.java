package de.fhg.igd.interactionanalysis.core;

import de.fhg.igd.interactionanalysis.core.data.Abstractions;
import de.fhg.igd.interactionanalysis.core.data.Interactions;


/**
 * The implementation of relational markov model 
 * 
 * @author Christian Stab
 */
public class RMMRank extends RMM {

	/** its serial number for serialization */
	private static final long serialVersionUID = 6220795211683926159L;

	/** design parameter for rmm-rank */
	protected int k =10;;
	
	/** test */
	private int[] abstractionCountsTest;
	
	/** the counts of transitions from state to state */
	private int[][] transitionCounts;
	
	/** counts of a state */
	private int[] counts;
	
	/** Counts of the initial state */
	private int[] initialCounts;
	
	/** count the number of seen initial observations */ 
	private int initialCount;
	
	/**
	 * Basic Constructor
	 */
	public RMMRank() {
		super();
		int abstractionCount = Abstractions.getInstance().getSize();
		abstractionCountsTest = new int[abstractionCount];
		for (int i=0;i<abstractionCount; i++) {
			abstractionCountsTest[i]=1;
		}
		int numberOfStates = Interactions.getInstance().getSize();
		transitionCounts = new int[numberOfStates][numberOfStates];
		counts = new int[numberOfStates];
		//k = Config.getInstance(null).getDesignParameter();
		initialCounts = new int[numberOfStates];
		initialCount=0;
	}
	
	@Override
	protected void updateTransitionMatrix(int obs) {
		// update ipam (stm) matrix
		//int observation = States.getInstance().getIndex(obs);		
		
		if (previousObservation==-1) {
			previousObservation = obs;
			updateTransitionCounts(obs);
			updateInitialProbabilies(obs);
		} else {
			// update this with ipam
			transitionCounts[previousObservation][obs]++;
			counts[previousObservation]++;
			for (int i=0; i<stm.getSize(); i++) {
				stm.setProbability(previousObservation, i, (double)transitionCounts[previousObservation][i]/(double) counts[previousObservation]);
			}
			updateTransitionCounts(obs);
			abstractionCountsTest[obs]++;
			previousObservation = obs;
		}
	}
	
	/**
	 * updates the transition count when a new observation occures
	 * @param state source state
	 */
	private void updateTransitionCounts(int state) {
		int[] abstractions = Abstractions.getInstance().getStateAbstractions(state);
		
		for (int i=0; i<abstractions.length; i++) {
			abstractionCountsTest[abstractions[i]]++;
		}
	}
	
	@Override
	protected void updateInitialProbabilies(int firstObservation) {
		initialCounts[firstObservation]++;
		initialCount++;
		for (int i=0; i<initProb.length; i++) {
			initProb[i] = ((double)initialCounts[i]) / ((double)initialCount);
		}
	}
	
	@Override
	protected double getTransProb(int from, int to) {
		// rmm-rank
		int[] fromAbstractions = Abstractions.getInstance().getStateAbstractions(from);
		int[] toAbstractions = Abstractions.getInstance().getStateAbstractions(to);
		
		// lambda rank
		double res = 0.0f;		
		double lambdaSum = 0.0f;
		for (int i=0; i<fromAbstractions.length; i++) {
			int fromAbstraction = fromAbstractions[i];
			for (int j=0; j<toAbstractions.length; j++) {
				int toAbstraction = toAbstractions[j];
				
				int exp = Abstractions.getInstance().getRank(fromAbstraction) + Abstractions.getInstance().getRank(toAbstraction);
				//double lambda = (double)Math.pow((double)(((double)transitionCounts[fromAbstraction][toAbstraction])/(double)k), (double)exp);
				double lambda = (double)Math.pow((double)(((double)abstractionCountsTest[fromAbstraction])/(double)k), (double)exp);
				//double lambda = (double)exp;
				lambdaSum = lambdaSum + lambda;
				
				// P(to | toAbstr)
				double toProb = 1.0f / (double)Abstractions.getInstance().getAbstractionStates(toAbstraction).length;
				res = res + (lambda * abstMatrix.getProbability(fromAbstraction, toAbstraction) * toProb);
			}
		}

		if (lambdaSum==0) {
			return 0.0f;
		} else {
			return res/lambdaSum;
		}
	}
	
	@Override
	public void setTrainingsData(int[] data) {
		for (int i=0; i<data.length; i++) {
			newObservation(data[i]);
		}
	}
}
