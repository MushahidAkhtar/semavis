package de.fhg.igd.interactionanalysis.core;

import de.fhg.igd.interactionanalysis.core.data.Abstractions;
import de.fhg.igd.interactionanalysis.core.data.Interactions;
import de.fhg.igd.interactionanalysis.core.data.TransitionMatrix;

/**
 * This abstract class is the skeleton of an rmm implementation.
 * 
 * @author Christian Stab
 */
public abstract class RMM extends PM {

	/** its serial number for serialization */
	private static final long serialVersionUID = -7198594618316582010L;

	/** the transistion matrix of abstractions */
	protected TransitionMatrix abstMatrix;
	
	/** the infered transition probabilities */
	protected TransitionMatrix inferedMatrix;
	
	/** Alpha as forgetting-factor */
	protected double alpha = 0.8d;
	
	/**
	 * Basic Constructor
	 */
	public RMM() {
		super();
		inferedMatrix = new TransitionMatrix(Interactions.getInstance().getSize());
		abstMatrix = new TransitionMatrix(Abstractions.getInstance().getSize());
		//alpha=Config.getInstance(null).getIPAMParameter();
	}
	
	@Override
	public void newObservation(int obs) {
		updateTransitionMatrix(obs);
		updateAbstMatrix();
		updateInferedMatrix();
	}
	
	@Override
	public void newObservations(int[] obs) {
		for (int i=0; i<obs.length; i++) {
			updateTransitionMatrix(obs[i]);
		}
		updateAbstMatrix();
		updateInferedMatrix();
	}
	
	/**
	 * Updates the IPAM transition matrix if a new Observation occures
	 * without updating the other matricex
	 * @param obs observation
	 */
	protected abstract void updateTransitionMatrix(int obs);
	
	/**
	 * Updates the infered probability matrix
	 */
	protected void updateInferedMatrix() {
		int stateCount = Interactions.getInstance().getSize();
		for (int i=0; i<stateCount; i++) {
			for (int j=0; j<stateCount; j++) {
				inferedMatrix.setProbability(i, j, getTransProb(i, j));
			}
		}
	}
	
	/**
	 * Returns the transition probability using rmm-uniform
	 * @param from source state 
	 * @param to target states
	 * @return probability
	 */
	protected abstract double getTransProb(int from, int to);
	
	/**
	 * Updates the transition matrix of abstractions
	 */
	protected void updateAbstMatrix() {
		for (int i=0; i<abstMatrix.getSize(); i++) {
			for (int j=0; j<abstMatrix.getSize(); j++) {
				abstMatrix.setProbability(i, j, getAbstProb(i, j));
			}
		}
	}
	
	/**
	 * returns the probability of a transition between two abstractions
	 * @param from source abstraction
	 * @param to target abstraction
	 * @return probability
	 */
	protected double getAbstProb(int from, int to) {
		//System.out.println("  getAbstProb()");
		int[] fromStates = Abstractions.getInstance().getAbstractionStates(from);
		int[] toStates = Abstractions.getInstance().getAbstractionStates(to);
		int fromNumber = fromStates.length;
		double result = 0.0f;
		for (int i=0; i<fromStates.length; i++) {
			int fromState = fromStates[i];
			double tmp=0.0f;
			for (int j=0; j<toStates.length; j++) {
				tmp = tmp + stm.getProbability(fromState, toStates[j]);
			}
			result = result + (1.0f / (double)fromNumber) * tmp;
		}
		return result;
	}
	
	@Override
	public TransitionMatrix getTransitionMatrix() {
		return inferedMatrix;
	}

}
