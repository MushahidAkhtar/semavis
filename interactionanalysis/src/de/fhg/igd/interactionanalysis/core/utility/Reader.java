package de.fhg.igd.interactionanalysis.core.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import de.fhg.igd.interactionanalysis.core.data.Interactions;

/**
 * This class contains Methods for reading interactions from files
 * 
 * @author Christian Stab
 */
public class Reader {

	/**
	 * reads the interactions from the given file and returns the interactions
	 * in the intern representation
	 * @param fileName filename
	 * @return interactions
	 */
	public static int[] readData(String fileName) {
		if (fileName!= null && (new File(fileName)).exists()) {
			ArrayList<String> inter = new ArrayList<String>();
			try {
				FileReader reader = new FileReader(fileName);
				BufferedReader in = new BufferedReader(reader);
				while (in.ready()) {
					String line = in.readLine();
					//System.out.println(line);
					inter.add(line);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			int[] interactions = new int[inter.size()];
			for (int i=0; i<inter.size(); i++) {
				interactions[i] = Interactions.getInstance().getIndex(inter.get(i).split(" "));
			}
			return interactions;
		} else {
			return null;
		}
	}
}
