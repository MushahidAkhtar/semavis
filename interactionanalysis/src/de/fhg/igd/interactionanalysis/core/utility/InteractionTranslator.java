package de.fhg.igd.interactionanalysis.core.utility;

import de.fhg.igd.interactionanalysis.core.data.Interactions;

/**
 * This class contains methods to translate interactions from the space 
 * speparated representation in the string array representation
 * 
 * @author Christian Stab
 */
public class InteractionTranslator {

	/**
	 * translate an interaction from String array representation in a space 
	 * separated string representation
	 * @param interaction interaction
	 * @return translated form of the interaction
	 */
	public static String translateInteraction(String[] interaction) {
		String result = "";
		if (interaction!=null) {
			for (int i=0;i<interaction.length; i++) {
				if (i!=0) {
					result = result + " " + interaction[i];
				} else {
					result = interaction[i];
				}
			}
		}
		return result;
	}
	
	/**
	 * translates interaction from int representation in a space 
	 * separated string representation
	 * @param actions interactions
	 * @return translated form of the interactions
	 */
	public static String[] translateInteractions(int[] actions) {
		String[] result = new String[actions.length];
		for (int i=0; i<actions.length; i++) {
			String[] interaction = Interactions.getInstance().getInteractionString(actions[i]);
			result[i] = translateInteraction(interaction);
		}
		return result;
	}
	
	/**
	 * translates interactions from the intern Stringarray representation in the
	 * extern one String representation 
	 * @param interactions interactions in the inter representation
	 * @return extern representation
	 */
	public static String[] translateInteractions(String[][] interactions) {
		String[] result = new String[interactions.length];
		for (int i=0; i<result.length; i++) {
			result[i] = translateInteraction(interactions[i]);
		}
		return result;
	}
	
	/**
	 * translates interactions from the Stringarray representation in the
	 * int representaion
	 * @param interactions interactions in the inter representation
	 * @return intern representation
	 */
	public static int[] translateInteractions2(String[][] interactions) {
		int[] result = new int[interactions.length];
		for (int i=0; i<result.length; i++) {
			result[i] = Interactions.getInstance().getIndex(interactions[i]);
		}
		return result;
	}
	
	/**
	 * translates an interaction from its integer representation to a single
	 * string representation
	 * @param i interaction as integer
	 * @return interaction as string
	 */
	public static String translateInteraction(int i) {
		if (i==-1) {
			return "";
		} else {
			return translateInteraction(Interactions.getInstance().getInteractionString(i));
		}
	}
	
	/**
	 * translate interaction from a single string representation to a 
	 * representation with separated strings
	 * @param interaction sing string representation
	 * @return separated String representation
	 */
	public static String[] translateInteraction(String interaction) {
		return interaction.split(" ");
	}
	
}
