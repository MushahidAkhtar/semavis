package de.fhg.igd.interactionanalysis.core.utility;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import de.fhg.igd.interactionanalysis.core.data.DomainNode;
import de.fhg.igd.interactionanalysis.core.data.Domains;

/**
 * This Class provides some functions to obtain the configuration of the system
 * from an XML file
 * 
 * @author Christian Stab
 */
public class Config {

	/** Logger */
	static Logger logger = Logger.getLogger(Config.class);
	
	/** location of the configuration file */
	public static String defaultConfigFile="config.xml";
	
	/** the location of the config file */
	private String configFile;
	
	/** the instance of this singleton */
	private static Config instance;
	
	/**
	 * basic constructor
	 */
	private Config(String configFile) {
		if (configFile==null) {
			this.configFile = defaultConfigFile;
		} else {
			this.configFile = configFile;
		}
	}
	
	/**
	 * returns the Instance of this singleton
	 * @param config the location of the config file; if null the default name 
	 * of the config file is used
	 * @return the instance of this singleton
	 */
	public static synchronized Config getInstance(String config) {
		if (instance==null) {
			instance = new Config(config);
		}
		return instance;
	
	}
	
	/**
	 * This Method reads the domains defined in the configuration file
	 * @return DomainNode[] the domains
	 */
	public DomainNode[] getDomains() {
		logger.info("### [START] parsing domains from file '" + configFile + "' ...");
		ArrayList<DomainNode> domains = new ArrayList<DomainNode>();
		try {			
		    DocumentBuilderFactory factory  = DocumentBuilderFactory.newInstance();
		    DocumentBuilder builder  = factory.newDocumentBuilder();
		    Document document = builder.parse(new File(configFile));
		    
		    NodeList ndList = document.getElementsByTagName("domain");
		    for(int i=0; i<ndList.getLength(); i++) {
		    	Node domain=ndList.item(i);
		    	if (domain.getParentNode().getNodeName() != null && !domain.getParentNode().getNodeName().equals("relation")) {
		    		NamedNodeMap attributes= domain.getAttributes();
					String domainName = attributes.getNamedItem("name").getNodeValue();
					logger.info("Capturing domain '" + domainName + "'");
					NodeList children = domain.getChildNodes();
					domains.add(getNodes(children,domainName));
		    	}
		    }

		} catch(SAXParseException pe) {
			logger.error("Parsing error in line " + pe.getLineNumber());
			logger.error(pe.getMessage());
			logger.error(pe.getStackTrace());
	    } catch(SAXException se) {
	    	logger.error(se.getStackTrace());
	    } catch( ParserConfigurationException pce ) {
	    	logger.error(pce.getStackTrace());
	    } catch( IOException ioe ) {
	        logger.error(ioe.getStackTrace());
	        ioe.printStackTrace();
	    }
	    logger.info("### [END] " +  domains.size() + " domains captured successfully!");
	    DomainNode[] tmp = new DomainNode[domains.size()];
	    return domains.toArray(tmp);
	}
	
	/**
	 * Returns the sub nodes of a domain
	 * @param nodes
	 * @param name
	 * @return the sub nodes of a domain
	 */
	private DomainNode getNodes(NodeList nodes, String name) {
		if (nodes!=null) {
			DomainNode node = new DomainNode(name);
			for (int i=0; i<nodes.getLength(); i++) {
				if (nodes.item(i).getNodeName().equals("node")) {
					String childName = nodes.item(i).getAttributes().getNamedItem("name").getNodeValue();
					
					NodeList childsChildren = nodes.item(i).getChildNodes(); 
					node.addChildren(getNodes(childsChildren, childName));
				}
			}
			return node;
		} else {
			return null;
		}
	}

	/**
	 * returns the location of the trainingsfile for the probabilistic method
	 * @return the location of the trainingsfile for the probabilistic method
	 */
	public String getTrainingsFile() {
		logger.info("### [START] parsing Trainings File Location from file '" + configFile + "' ...");
		String res="";
		try {			
		    DocumentBuilderFactory factory  = DocumentBuilderFactory.newInstance();
		    DocumentBuilder builder  = factory.newDocumentBuilder();
		    Document document = builder.parse(new File(configFile));
		    
		    NodeList ndList = document.getElementsByTagName("config");
		    ndList=ndList.item(0).getChildNodes();
		    for(int i=0; i<ndList.getLength(); i++) {
		    	
		    	if (ndList.item(i).getNodeName().equals("trainingsFile")) {
		    		res= ndList.item(i).getTextContent();
		    	}
		    }
	
		} catch(SAXParseException pe) {
			logger.error("Parsing error in line " + pe.getLineNumber());
			logger.error(pe.getMessage());
			logger.error(pe.getStackTrace());
	    } catch(SAXException se) {
	    	logger.error(se.getStackTrace());
	    } catch( ParserConfigurationException pce ) {
	    	logger.error(pce.getStackTrace());
	    } catch( IOException ioe ) {
	        logger.error(ioe.getStackTrace());
	    }
	    logger.info("### [END] p�arsing Trainings File Location (value=" +  res + ")  captured successfully!");
		return res;
	}
	
	
	/**
	 * returns the name of the used Model
	 * @return name of the model 
	 */
	public String getModelName() {
		logger.info("### [START] parsing model name from file '" + configFile + "' ...");
		String res="";
		try {
			//logger.info(System.getProperty(System.getProperty("user.dir")));
		    DocumentBuilderFactory factory  = DocumentBuilderFactory.newInstance();
		    DocumentBuilder builder  = factory.newDocumentBuilder();
		    Document document = builder.parse(new File(configFile));
		    
		    NodeList ndList = document.getElementsByTagName("config");
		    ndList=ndList.item(0).getChildNodes();
		    for(int i=0; i<ndList.getLength(); i++) {
		    	
		    	if (ndList.item(i).getNodeName().equals("model")) {
		    		res= ndList.item(i).getTextContent();
		    	}
		    }

		} catch(SAXParseException pe) {
			logger.error("Parsing error in line " + pe.getLineNumber());
			logger.error(pe.getMessage());
			logger.error(pe.getStackTrace());
	    } catch(SAXException se) {
	    	logger.error(se.getStackTrace());
	    	logger.error(se.getMessage());
	    } catch( ParserConfigurationException pce ) {
	    	logger.error(pce.getStackTrace());
	    	logger.error(pce.getMessage());
	    } catch( IOException ioe ) {
	        logger.error(ioe.getStackTrace());
	        logger.error(ioe.getMessage());
	    }
	    logger.info("### [END] model name (value=" +  res + ")  captured successfully!");
		return res;
	}
	
	/**
	 * returns the parameter of the storage method
	 * @return name of the model 
	 */
	public boolean getStoreParam() {
		logger.info("### [START] parsing storing parameter from file '" + configFile + "' ...");
		String res="";
		try {
			//logger.info(System.getProperty(System.getProperty("user.dir")));
		    DocumentBuilderFactory factory  = DocumentBuilderFactory.newInstance();
		    DocumentBuilder builder  = factory.newDocumentBuilder();
		    Document document = builder.parse(new File(configFile));
		    
		    NodeList ndList = document.getElementsByTagName("config");
		    ndList=ndList.item(0).getChildNodes();
		    for(int i=0; i<ndList.getLength(); i++) {
		    	
		    	if (ndList.item(i).getNodeName().equals("storeall")) {
		    		res= ndList.item(i).getTextContent();
		    	}
		    }

		} catch(SAXParseException pe) {
			logger.error("Parsing error in line " + pe.getLineNumber());
			logger.error(pe.getMessage());
			logger.error(pe.getStackTrace());
	    } catch(SAXException se) {
	    	logger.error(se.getStackTrace());
	    	logger.error(se.getMessage());
	    } catch( ParserConfigurationException pce ) {
	    	logger.error(pce.getStackTrace());
	    	logger.error(pce.getMessage());
	    } catch( IOException ioe ) {
	        logger.error(ioe.getStackTrace());
	        logger.error(ioe.getMessage());
	    }
	    logger.info("### [END] storing parameter (value=" +  res + ")  captured successfully!");
		return Boolean.parseBoolean(res);
	}
	
	/**
	 * returns the interactions defined in the config file
	 * @return the interactions defined in the config file
	 */
	public String[][] getInteractions() {
		logger.info("### [START] parsing interactions from file '" + configFile + "' ...");
		
		ArrayList<String[]> interactions = new ArrayList<String[]>();
		
		try {
			//logger.info(System.getProperty(System.getProperty("user.dir")));
		    DocumentBuilderFactory factory  = DocumentBuilderFactory.newInstance();
		    DocumentBuilder builder  = factory.newDocumentBuilder();
		    Document document = builder.parse(new File(configFile));
		    
		    NodeList ndList = document.getElementsByTagName("config");
		    ndList=ndList.item(0).getChildNodes();
		    for(int i=0; i<ndList.getLength(); i++) {
		    	
		    	if (ndList.item(i).getNodeName().equals("interaction")) {
		    		String action = ndList.item(i).getTextContent();
		    		//System.out.println(action);
		    		String[] res = action.split(" ");
		    		if (res.length==Domains.getInstance().getSize()) {
		    			interactions.add(res);
		    		} else {
		    			logger.error("  '" + action + " is not a valid interaction");
		    		}
		    	}
		    }

		} catch(SAXParseException pe) {
			logger.error("Parsing error in line " + pe.getLineNumber());
			logger.error(pe.getMessage());
			logger.error(pe.getStackTrace());
	    } catch(SAXException se) {
	    	logger.error(se.getStackTrace());
	    	logger.error(se.getMessage());
	    } catch( ParserConfigurationException pce ) {
	    	logger.error(pce.getStackTrace());
	    	logger.error(pce.getMessage());
	    } catch( IOException ioe ) {
	        logger.error(ioe.getStackTrace());
	        logger.error(ioe.getMessage());
	    }
	    logger.info("### [END] parsing interactions. " + interactions.size() + "  captured successfully!");		
		
		String[][] tmp = new String[interactions.size()][Domains.getInstance().getSize()];
		return interactions.toArray(tmp);
	}
	
}
