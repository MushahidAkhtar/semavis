package de.fhg.igd.interactionanalysis.core.utility;

import jama.Matrix;

import org.apache.log4j.Logger;

import de.fhg.igd.interactionanalysis.core.data.Interactions;
import de.fhg.igd.interactionanalysis.core.data.SteadyStateVector;
import de.fhg.igd.interactionanalysis.core.data.TransitionMatrix;

/**
 * This class calculates a steady state vector of a learned probabilistic model
 * with the given accuracy
 * 
 * @author Christian Stab
 */
public class SSVCalculator {
	
	static Logger logger = Logger.getLogger(SSVCalculator.class);
	
	
	/**
	 * Calculates the steady state vector
	 * @param stm TransitionMatrix
	 * @return SteadyStateVector
	 */
	public static SteadyStateVector getSSV(TransitionMatrix stm) {
		double[][] src = stm.getMatrix();
		double[][] source = new double[src.length][src.length];
		for (int i=0; i<src.length; i++) {
			for (int j=0; j<src[i].length; j++) {
				source[i][j] = src[i][j];
			}
		}
		
		//transform matrix
		double[][] matrix = new double[source.length+1][source.length];
		double[][] bmatrix = new double[source.length+1][1];
		
		for (int i=0; i<source.length; i++) {
			source[i][i] = source[i][i]-1;
		}
		
		for (int i=0; i<matrix.length; i++) {
			for (int j=0;j<matrix[0].length; j++) {
				if (i==source.length) {
					matrix[i][j]=1;
				} else {
					matrix[i][j]=source[j][i];
				}
			}
			if (i==source.length) {
				bmatrix[i]= new double[] {1.0d};
			} else {
				bmatrix[i]= new double[] {0.0d};
			}
		}
		
		Matrix A = new Matrix(matrix);
	    Matrix b = new Matrix(bmatrix);
	    Matrix x = A.solve(b);
	      
	    double[][] result = x.getArray();
	    double[] res = new double[matrix.length];
	    for (int i=0; i<result.length; i++) {
	    	for (int j=0; j<result[i].length; j++) {
	    		res[i] = result[i][j];
	    	}
	    }
	    SteadyStateVector ssv = new SteadyStateVector(res);
	    ssv.setIndices(InteractionTranslator.translateInteractions(Interactions.getInstance().getInteractions()));
	    logger.info("Steady State Vector calculated.");
	    return ssv;
	}
	
}
