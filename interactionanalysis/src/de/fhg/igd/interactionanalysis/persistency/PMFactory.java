package de.fhg.igd.interactionanalysis.persistency;

import de.fhg.igd.interactionanalysis.core.IPAM;
import de.fhg.igd.interactionanalysis.core.IPM;
import de.fhg.igd.interactionanalysis.core.IRolePM;
import de.fhg.igd.interactionanalysis.core.KO;
import de.fhg.igd.interactionanalysis.core.LEV;
import de.fhg.igd.interactionanalysis.core.MK;
import de.fhg.igd.interactionanalysis.core.KOStar19;
import de.fhg.igd.interactionanalysis.core.RMMRank;
import de.fhg.igd.interactionanalysis.core.RMMUniform;
import de.fhg.igd.interactionanalysis.core.utility.Config;
import de.fhg.igd.interactionanalysis.core.utility.Reader;

/**
 * Creates probabilistic models dependened on the model defined in the 
 * configuration file
 * 
 * @author Christian Stab
 */
public class PMFactory {

	/** name of the model */
	private String modelName;
	
	/** the instance of this singleton */
	private static PMFactory instance;
	
	/**
	 * Basic Constructor
	 */
	private PMFactory () {
		modelName = Config.getInstance(null).getModelName();
	}
	
	/**
	 * returns the instance of this singleton
	 * @return the instance of this singleton
	 */
	public static synchronized PMFactory getInstance () {
		if (PMFactory.instance == null) {
			PMFactory.instance = new PMFactory ();
	    }
	    return PMFactory.instance;
	}
	
	/**
	 * creates and returns an instance of a probabilistic model
	 * @return an instance of a probabilistic model
	 */
	public IPM createPM() {
		IPM pm = null;
		if (modelName.equals("RMMRank")) {
			pm = new RMMRank();
		}
		if (modelName.equals("RMMUniform")) {
			pm = new RMMUniform();
		}
		if (modelName.equals("IPAM")) {
			pm = new IPAM();
		}
		if (modelName.equals("MK")) {
			pm = new MK();
		}
		if (modelName.equals("LEV")) {
			pm = new LEV();
		}
		if (modelName.equals("KO")) {
			pm = new KO();
		}
		if (modelName.equals("KOStar19")) {
			pm = new KOStar19();
		}
		
		// train model
		if (pm!=null) {
			int [] interactions = Reader.readData(Config.getInstance(null).getTrainingsFile());
			
			for (int i=0; i<interactions.length; i++) {
				pm.newObservation(interactions[i]);
			}
		}
		if (pm instanceof IRolePM) {
			((IRolePM) pm).enableRoleLearning(true);
		}
		return pm;
	}
}
