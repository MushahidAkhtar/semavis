package de.fhg.igd.interactionanalysis.persistency;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;

import de.fhg.igd.interactionanalysis.core.IPM;
import de.fhg.igd.interactionanalysis.core.utility.Config;

/**
 * This class is responsible for storing and loading a probabilistic model for
 * an user in an mysql database. 
 * 
 * @author Christian Stab
 */
public class Persistency {

	/** Logger */
	static Logger logger = Logger.getLogger(Persistency.class);
	
	/** Springs simpleJdbcTemplate */
	private SimpleJdbcTemplate jdbcTemplate;
	
	/** instance of this singleton */
	private static Persistency instance;
	
	/** the datasource*/
	private DataSource dataSource;
	
	/** Spring handler for Lob Data*/
	private LobHandler lobHandler;
	
	/** determines if interactions are stores for evavluation purposes */
	private boolean storeall;
	
	/**
	 * Returns an instance of this singleton
	 * @return instance of this singleton
	 */
	public static synchronized Persistency getInstance () {
		if (Persistency.instance == null) {
			Persistency.instance = new Persistency();
	    }
	    return Persistency.instance;
	}

	/**
	 * Basic Constructor
	 */
	public Persistency() {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
		dataSource = (DriverManagerDataSource)ctx.getBean("dataSource");
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
		lobHandler = new DefaultLobHandler();
		storeall = Config.getInstance(null).getStoreParam();
	}
	
	/**
	 * Loads an probabilistic Model from a mysql database. If there isn't a
	 * model stored for the given id this method returns null.
	 * @param userID id of the user
	 * @return probabilistic model if for the user one exists in the database; if not this method returns null.
	 */
	public IPM loadPM(String userID) {
		logger.info("Load model for user '" + userID + "'");
		String sql = "SELECT pm FROM pm WHERE userid=?";
		List<byte[]> l = jdbcTemplate.query(sql, new ParameterizedRowMapper<byte[]>() {

			@Override
			public byte[] mapRow(ResultSet rs, int i) throws SQLException {
				byte[] blobBytes = lobHandler.getBlobAsBytes(rs, "pm");
				return blobBytes;
			}
			
		}, userID);
		
		IPM pm = null;
		if (l.size()>0) {
			try {
				pm = (IPM)new java.io.ObjectInputStream(new ByteArrayInputStream(l.get(0))).readObject();
			} catch (IOException e) {
				logger.error(e.getMessage());
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				logger.error(e.getMessage());
				e.printStackTrace();
			}
		}
		return pm;
	}

	/**
	 * Saves the probabilistic model in the database. (It overrides existing data)
	 * @param userID id of the user
	 * @param pm probabilistic model
	 */
	public void savePM(final String userID, IPM pm) {
		logger.info("Save model for user '" + userID + "'");
		System.out.println("Persistency: Save Model for user " + userID);
		String sql = "SELECT userid FROM pm WHERE userid=?";
		List<String> test = jdbcTemplate.query(sql, new ParameterizedRowMapper<String>() {

			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString (1);
			}
			
		}, userID);
				
		if (test.size()>0) {
			sql = "UPDATE pm SET pm=? WHERE userid=?";
			jdbcTemplate.update(sql, pm, userID);
		} else {
			sql = "INSERT INTO pm (userid, pm) VALUES (?,?)";
			jdbcTemplate.update(sql, userID, pm);
		}
	}
	
	
	/**
	 * Saves an interaction for evaluation purpose in the database 
	 * @param userID id of the user
	 * @param interaction interaction
	 */
	public void saveInteraction(String userID, String interaction) {
		if (storeall) {
			logger.info("Save interactions for user '" + userID + "'");
			String sql = "INSERT INTO interactions (userid, interaction) VALUES (?,?)";
			jdbcTemplate.update(sql, userID, interaction);
		}
	}

}
