package de.fhg.igd.interactionanalysis2.core.test;

import de.fhg.igd.interactionanalysis2.core.data.Interactions;
import de.fhg.igd.interactionanalysis2.core.utility.Reader;

public class InteractionsTest {

	public static void main (String[] args) {
		String fileName = "interactions/PKITool1.txt";
		String[] data = Reader.readData2(fileName);
		
		Interactions interactions = new Interactions(); 
		
		for (int i=0;i<data.length; i++) {
			System.out.println(data[i]);
			System.out.println();
			interactions.newInteraction(data[i]);
			//System.out.println(interactions.getIndex(data[i]));
		}
		
		System.out.println();
		System.out.println("Size: " + interactions.getSize());
		System.out.println();
		System.out.println("##########################");
		
		String[] test = interactions.getInteractions();
		
		for (int i=0;i<test.length; i++) {
			System.out.println(test[i]);
			System.out.print("\t\t" + interactions.contains(test[i]));
			System.out.println();
			
		}
	}
}
