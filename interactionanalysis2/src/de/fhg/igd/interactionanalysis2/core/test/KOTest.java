package de.fhg.igd.interactionanalysis2.core.test;

import de.fhg.igd.interactionanalysis2.core.KOStar19;
import de.fhg.igd.interactionanalysis2.core.utility.Printer;
import de.fhg.igd.interactionanalysis2.core.utility.Reader;

public class KOTest {
	
	public static void main(String[] args) {
		String fileName = "interactions/PKITool1.txt";
		String[] data = Reader.readData2(fileName);
		
		KOStar19 pm = new KOStar19();
		
		// learn model
		for (int i=0;i<data.length; i++) {
			pm.newObservation(data[i]);
			System.out.println(i + " " + pm.getNextAction());
		}
		
		
		Printer.printSSV(pm.getSteadyStateVector());
		
		System.out.println();
		
		System.out.println("nextAction: " + pm.getNextAction());
		
		System.out.println();
		
		Printer.printQueryResult(pm.query(new int[] {1,1}));
		
//		for (int i=0;i<data.length; i++) {
//			Printer.printState2(data[i]);
//			System.out.println();
//			interactions.newInteraction(data[i]);
//			//System.out.println(interactions.getIndex(data[i]));
//		}
//		
//		System.out.println();
//		System.out.println("Size: " + interactions.getSize());
//		System.out.println();
//		System.out.println("##########################");
//		
//		String[][] test = interactions.getInteractions();
//		
//		for (int i=0;i<test.length; i++) {
//			Printer.printState(interactions, test[i]);
//			System.out.print("\t\t" + interactions.contains(test[i]));
//			System.out.println();
//			
//		}
	}
}
