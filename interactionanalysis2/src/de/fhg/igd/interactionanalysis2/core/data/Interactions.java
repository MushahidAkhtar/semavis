package de.fhg.igd.interactionanalysis2.core.data;

import java.io.Serializable;
import java.util.HashMap;

/**
 * This Class contains all interactions of the System and some functions 
 * for using them. In particular this class is used for mapping the external 
 * string representation to a unique number which identifies an interaction.
 * 
 * @author Christian Stab
 */
public class Interactions implements Serializable {
	  
	 /** its serial number */
	private static final long serialVersionUID = 7902513987933748024L;

	/** HashMap for mapping names of interactions to numbers */
	private HashMap<String, Integer> map;
		
	/** reversed Mapping: from number to String */
	private HashMap<Integer, String> reversedMap; 
	  
	/**
	 * Basic Constructor
	 */
	public Interactions() {
		map = new HashMap<String, Integer>();
		reversedMap = new HashMap<Integer, String>();
	}
	  
	/**
	 * adds a new interaction
	 * @param inter new interaction
	 */
	public void newInteraction(String inter) {
		if (!map.containsKey(inter)) {
			map.put(inter, map.size());
			reversedMap.put(map.size()-1, inter);
		}
	}
	  
	/**
	 * Returns the number of interactions
	 * @return number of interactions
	 */
	public int getSize() {
		return map.size();
	}
	  
	/**
	 * returns if the given String tuple is an interaction
	 * @param interaction the interaction
	 * @return true if input is an valid interaction; else it returns false
    */
	public boolean contains(String interaction) {
		if (interaction!=null) {
			return map.containsKey(interaction);
		} else {
			return false;
		}
	}
	  
	/** 
	 * returns the index of an interaction
	 * @param interaction external string representation of an interaction
	 * @return unique index
	 */
	public int getIndex(String interaction) {
		return map.get(interaction).intValue();
	}
		
	/**
	 * returns the external representation of an interaction
	 * @param index intern representation of the interaction
	 * @return external representation
	 */
	public String getInteractionString(int index) {
		return reversedMap.get(new Integer(index));
	}
	  
	/**
	 * returns all interaction in the external representation
	 * @return all interaction in the external representation
	 */
	public String[] getInteractions() {
		String[] result = new String[map.size()];
		for (int i=0; i<map.size() ; i++) {
			result[i] = reversedMap.get(i);
		}
		return result;
	}  
}
