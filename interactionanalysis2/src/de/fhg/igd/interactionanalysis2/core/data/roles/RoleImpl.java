package de.fhg.igd.interactionanalysis2.core.data.roles;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import de.fhg.igd.interactionanalysis2.core.data.Interactions;
import de.fhg.igd.interactionanalysis2.core.data.QueryResult;
import de.fhg.igd.interactionanalysis2.core.data.SteadyStateVector;
import de.fhg.igd.interactionanalysis2.core.utility.Abstractions;

/**
 * this class implements a Role. It provides functions for query this
 * role, adding sequences and calculating matchings with sequences.
 * 
 * @author Christian Stab
 */
public class RoleImpl implements IRole, Serializable {


	/** its serial number */
	private static final long serialVersionUID = -1359414284427908343L;

	/** counts for every state */
	private List<Integer> interactionCounts;
	
	/** count of all seen interactions */
	private int interactionCount; 
	
	/** all sequences belonging to this role */
	private List<List<Integer>> sequences;
	
	/** the interactions */
	private Interactions interactions;
	
	
	/**
	 * basic constructor
	 * @param interactions - Interactions
	 */
	public RoleImpl(Interactions interactions) {
		sequences = new ArrayList<List<Integer>>();
		this.interactions = interactions;
		
		interactionCounts = new ArrayList<Integer>();
	}
	
	@Override
	public void newRoleSequence(List<Integer> sequence) {
		// update stateCounts
		for (int i=0; i<sequence.size(); i++) {
			
			if (interactionCounts.size()<=sequence.get(i)+1) {
				while (interactionCounts.size()<=sequence.get(i)+1) {
					interactionCounts.add(new Integer(0));
				}
			}
			
			interactionCounts.set(sequence.get(i), new Integer(interactionCounts.get(sequence.get(i)).intValue() +1));
			interactionCount++;
		}
		
		sequences.add(sequence);
	}
	
	@Override
	public double match(List<Integer> seq) {
		double result =0;
		for (int i=0; i<sequences.size(); i++) {
			List<Integer> sequence = sequences.get(i);
			result = result + matchSequences(seq, sequence);
		}
		
		return result;
	}
	
	/**
	 * compares two sequences an return a ranking. if the ranking is high 
	 * the similarity of the two sequences is high
	 * @param a list 1
	 * @param b list 2
	 * @return ranking 
	 */
	public double matchSequences(List<Integer> a, List<Integer> b) {
		double result = 0;
		for (int i=0; i<=a.size(); i++) {
			for (int j=i+1;j<=a.size(); j++) {
				List<Integer> subList = a.subList(i, j);
				result = result + countSeq(subList, b)*Math.pow(subList.size(),19);
			}
		}
		return result;
	}
	
	/**
	 * counts the occurrences of the list a in list in
	 * @param a the occurrences of this sequences is counted
	 * @param in the search space
	 * @return the number of occurrences of a in in
	 */
	public int countSeq(List<Integer> a, List<Integer> in) {
		int result = 0;
		
		for(int i=0; i<in.size()-a.size()+1; i++) {
			boolean match = true;
			for (int j=0; j<a.size(); j++) {
				//int t1=in.get(i+j);
				//int t2=a.get(j);
				if (in.get(i+j).intValue()!=a.get(j).intValue()) {
					match = false;
				} 
			}
			if (match) {
				result++;
			}
		}
		return result;
	}
	
	@Override
	public QueryResult query(int[] ranks) {
		HashMap<String,Integer> map = new HashMap<String, Integer>();
		ArrayList<Double> prob = new ArrayList<Double>(); 
		
		SteadyStateVector ssv = getSteadyStateVector();
		
		for (int i=0; i<ssv.getSize();i++) {
			String stateAbst = Abstractions.getAbstraction(ssv.getIndices()[i], ranks);
			if (stateAbst!= null) {
				if (!map.containsKey(stateAbst)) {
					map.put(stateAbst,map.size());
					prob.add(new Double(ssv.getValues()[i]));
				} else {
					prob.set(map.get(stateAbst), new Double(prob.get(map.get(stateAbst)).doubleValue() + ssv.getValues()[i]));
				}
			}
		}
		
		double[] result = new double[map.size()];
		String[] indices = new String[map.size()];
		int i=0;
		for (Iterator<String> it = map.keySet().iterator(); it.hasNext();) {
			indices[i] = it.next();
	        result[i] = prob.get(map.get(indices[i]));
	        i++;
		}
		
		return new QueryResult(result, indices); 
	}
	
	@Override
	public SteadyStateVector getSteadyStateVector() {
		SteadyStateVector ssv = new SteadyStateVector(interactions.getSize());
		ssv.setIndices(interactions.getInteractions());
		
		for (int i=0; i<interactions.getSize(); i++) {
			if (i<interactionCounts.size()) {
				ssv.setProbability(i, ((double)interactionCounts.get(i))/((double)interactionCount));
			} else {
				ssv.setProbability(i, 0.0d);
			}
		}
		
		return ssv;
	}
	
	@Override
	public List<List<Integer>> getSequences() {
		return sequences;
	}


	/**
	 * prints all sequences of this role (for debugging)
	 */ 
	public void printSequences() {
		for (int i=0; i<sequences.size(); i++) {
			List<Integer> list = sequences.get(i);
			System.out.print("    ");
			for (int j=0; j<list.size(); j++) {
				System.out.print(list.get(j) + "\t");
			}
			System.out.println();
		}
	}
	
}
