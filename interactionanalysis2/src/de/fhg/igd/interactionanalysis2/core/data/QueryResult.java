package de.fhg.igd.interactionanalysis2.core.data;

/**
 * This class contains the results of a query.
 * @author Christian Stab
 */
public class QueryResult {

	/** the values of the of the query result */
	private double[] values;
	
	/** the indices of a query result*/
	private String[] indices;
	
	/**
	 * Basic Constructor
	 * @param values the values of the result
	 * @param indices indices of the result
	 */
	public QueryResult(double[] values, String[] indices) {
		this.values = values;
		this.indices = indices;
	}
	
	/**
	 * returns the values of this query result
	 * @return the values of this query result
	 */
	public double[] getValues() {
		return values;
	}
	
	/**
	 * returns the indices of the query result
	 * @return the indices of the query result
	 */
	public String[] getIndices() {
		return indices;
	}
}
