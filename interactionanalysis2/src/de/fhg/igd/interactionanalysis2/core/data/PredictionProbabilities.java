package de.fhg.igd.interactionanalysis2.core.data;

/**
 * This class contains the probability distribution of a prediction
 * @author Christian Stab
 */
public class PredictionProbabilities {
	
	/** the values of the of the probability distribution */
	private double[] values;
	
	/** the indices of a probability distribution*/
	private String[] indices;
	
	/**
	 * Basic Constructor
	 * @param values the values of the probability distribution
	 * @param indices indices of the probability distribution
	 */
	public PredictionProbabilities(double[] values, String[] indices) {
		this.values = values;
		this.indices = indices;
	}
	
	/**
	 * returns the values of this probability distribution
	 * @return the values of this probability distribution
	 */
	public double[] getValues() {
		return values;
	}
	
	/**
	 * returns the indices of the probability distribution
	 * @return the indices of the probability distribution
	 */
	public String[] getIndices() {
		return indices;
	}
}
