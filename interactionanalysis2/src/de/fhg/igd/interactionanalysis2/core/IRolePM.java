package de.fhg.igd.interactionanalysis2.core;

import de.fhg.igd.interactionanalysis2.core.data.QueryResult;
import de.fhg.igd.interactionanalysis2.core.data.SteadyStateVector;

/**
 * This interface provides some methodes for a probabilistic model
 * able to handle roles 
 * 
 * @author Christian Stab
 */
public interface IRolePM {

	/**
	 * returns the number of roles
	 * @return the number of roles
	 */
	public int getRoleCounts();
	
	/**
	 * returns the index of the active role. 
	 * If no role is known this method returns -1
	 * @return the active role
	 */
	public int getActiveRole();
	
	/**
	 * queries the active role
	 * @param ranks ranks for the query
	 * @param roleIndex
	 * @return QueryResult
	 */
	public QueryResult queryRole(int[] ranks, int roleIndex);
	
	/**
	 * returns the steady state vector of the given role
	 * @param roleIndex the index of the role
	 * @return the steady state vector of the given role
	 */
	public SteadyStateVector getRoleSSV(int roleIndex);
	
	/**
	 * returns the sequences of the given role
	 * @param role the role
	 * @return the sequences of the given role
	 */
	public String[] getRoleSequences(int role);
	
	/**
	 * enables or disables the learning of the roles 
	 * @param b boolean
	 */
	public void enableRoleLearning(boolean b);
}
