package de.fhg.igd.interactionanalysis2.core;

import de.fhg.igd.interactionanalysis2.core.data.Interactions;
import de.fhg.igd.interactionanalysis2.core.data.PredictionProbabilities;
import de.fhg.igd.interactionanalysis2.core.data.QueryResult;
import de.fhg.igd.interactionanalysis2.core.data.SteadyStateVector;

/**
 * The main Interface for probabilistic models
 * 
 * @author Christian Stab
 */
public interface IPM {

	/**
	 * Updates the probabilities if a new observation occurs
	 * @param obs observation
	 */
	public abstract void newObservation(String obs);
	
	/**
	 * Updates the probabilities if new observations occurs
	 * @param obs observations
	 */
	public abstract void newObservations(String[] obs);
	
	/**
	 * returns the next Action 
	 * @return next state
	 */
	public String getNextAction();
	
	/**
	 * returns the next most possible actions
	 * @param count number of actions
	 * @return next most possible actions 
	 */
	public String[] getNextActions(int count);
	
	/**
	 * returns the probability distribution of the actual prediction
	 * @return the probability distribution of the actual prediction
	 */
	public PredictionProbabilities getPredictionProbabilities();
	
	/**
	 * returns the steady state vector of this model
	 * @return SteadyStateVector
	 */
	public SteadyStateVector getSteadyStateVector();
	
	/**
	 * returns the results of a query
	 * @param ranks ranks of the query
	 * @return QueryResult
	 */
	public QueryResult query(int[] ranks);
	
	/**
	 * sets the training data for the probabilistic model 
	 * @param data training data
	 */
	public void setTrainingsData(String[] data);
	
	/**
	 * returns the interactions which belong to this model
	 * @return
	 */
	public Interactions getInteractions();
	
	/**
	 * returns an array with all prediction probabilities
	 * @return an array with all prediction probabilities
	 */
	public double[] getPredictionProbabilityList();
	
	/**
	 * returns an array with all prediction ranks
	 * @return an array with all prediction ranks
	 */
	public int[] getPredictionRankList(); 
	
	/**
	 * returns an array with all perfect matches
	 * @return an array with all perfect matches
	 */
	public int[] getperfectMatchesList();
}
