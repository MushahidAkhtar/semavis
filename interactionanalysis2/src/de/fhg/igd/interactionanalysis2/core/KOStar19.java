package de.fhg.igd.interactionanalysis2.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import de.fhg.igd.interactionanalysis2.core.data.Interactions;
import de.fhg.igd.interactionanalysis2.core.data.PredictionProbabilities;
import de.fhg.igd.interactionanalysis2.core.data.QueryResult;
import de.fhg.igd.interactionanalysis2.core.data.SteadyStateVector;
import de.fhg.igd.interactionanalysis2.core.data.roles.IRole;
import de.fhg.igd.interactionanalysis2.core.data.roles.RoleImpl;
import de.fhg.igd.interactionanalysis2.core.utility.Abstractions;

/**
 * This class implements the KO* /19 algorithm with simple roles.
 * 
 * @author Christian Stab
 */
public class KOStar19 implements IRolePM, IPM, Serializable {

	/** its serial number */
	private static final long serialVersionUID = -8528996924274139374L;

	/** training data containing all interactions */
	private List<Integer> trainingData;
	
	/** Number of correct predicted interactions */
	private int perfectMatches;
	
	/** prediction probability list*/
	private List<Double> predictionProbabilityList;
	
	/** prediction rank list */
	private List<Integer> predictionRankList;
	
	/** perfect matches list*/
	private List<Integer> perfectMatchesList;
	
	/** the indices of every occurrence of an interaction in the training data */
	private List<List<Integer>> occurrence;
	
	/** actual probabilities */
	private double[] result;
	
	/** indicates if a new interaction occurrences. If true new results must be calculated */
	private boolean newObservation;

	/** the count for every state*/
	private List<Integer> interactionCounts;
	
	/** the interactions of this model */
	private Interactions interactions;
	
	/** count of all observations */
	private int countAll;
	
	/** length of the longest sequence */
	private int maxLength;
	
	/** start index of the actual sequence */
	private int startIndex;
	
	/** start index of the actual role */
	private int roleStartIndex;
	
	/** the minimum size of role interactions */
	private int minRoleSize = 2;
	
	/** Threshold for role creation */
	private double roleThreshold = 0.0d;
	
	/** role interactions */
	private ArrayList<Integer> roleList;
	
	/** the roles of this model */
	private List<IRole> roles;
	
	/** the active role */
	private IRole activeRole;
	
	/** is role learning enabled */ 
	private boolean roleLearning;
	
	/**
	 * Basic Constructor
	 */
	public KOStar19() {
		this.trainingData = new ArrayList<Integer>();
		perfectMatches = 0;
		predictionProbabilityList = new ArrayList<Double>();
		predictionRankList = new ArrayList<Integer>();
		perfectMatchesList = new ArrayList<Integer>();
		interactions = new Interactions();
		occurrence = new ArrayList<List<Integer>>();
		interactionCounts = new ArrayList<Integer>();
		roles = new ArrayList<IRole>();
		newObservation = true;
		roleList = new ArrayList<Integer>();
		roleLearning=false;
		countAll = 0;
	}
	
	/**
	 * calculates the probability distribution
	 * @return the probability distribution
	 */
	private double[] calcPrediction() {
		if (newObservation) {
			maxLength = 0;
			result = new double[interactions.getSize()];
			double count = 0.0d;
		
			// for every state
			for (int state=0; state<interactions.getSize(); state++) {
			
				List<Integer> occurrence = getOccurence(state);
				
				trainingData.add(state);
				for (int i=0; i<occurrence.size(); i++) {
					double length = calcSeqLength(state, occurrence.get(i));
				
					result[state] = result[state] + length * w(length);
				}
				count = count + result[state];
				trainingData.remove(trainingData.size()-1);
			}
			if (roleLearning) {
				learnRoles();
			}
			
			// normalize
			for (int i=0; i< result.length; i++) {
				result[i] = result[i]/count;
			}
			
			newObservation=false;
			return result;
		} else {
			return result;
		}
	}
	
	/**
	 * compares two sequences in the training data. The first sequence begins at the 
	 * last element of the training data and the second sequence begins at the given index.
	 * It returns the length of the matched sequence.
	 * @param index occurrence of the tested interaction in the training data (Must be the same
	 * Interaction as the last element of the training data)
	 * @return the length of the matching sequence
	 */
	private double calcSeqLength(int state, int index) {
		int i = 0;
		
		while (Math.min(index-i, trainingData.size()-i-1)>=0 && trainingData.size()-i-1>index && trainingData.get(index-i).intValue()==trainingData.get(trainingData.size()-1-i).intValue()) {
			i++;
		}
		if (i>maxLength) {
			maxLength = i-1;
			startIndex = index - i+1;
		}
		
		return i;
	}
	
	/**
	 * the weighting function 
	 * @param i length of a sequence
	 * @return the weight for a given length
	 */
	private double w(double i) {
		return Math.pow(i, 19);
	}
	
	/**
	 * learn the roles. checks in which role the user is and
	 * sets the actual role
	 */
	private void learnRoles() {
		// get actual role sequence
		roleList.clear();
		for (int i=startIndex; i<startIndex+maxLength; i++) {
			roleList.add(trainingData.get(i));
		}
		
		if (roleStartIndex!=startIndex) {
			roleStartIndex = startIndex;
		}
		
		ArrayList<Integer> tmp = new ArrayList<Integer>(); 
		for (int i=0; i<roleList.size(); i++) {
			tmp.add(roleList.get(i));
		}
		
		if (!tmp.isEmpty()) {
			// find matching role
			double bestMatch = roleThreshold;
			int bestRoleIndex = -1;
			for (int i=0; i<roles.size(); i++) {
				double roleRank = roles.get(i).match(tmp);
				if (roleRank>bestMatch) {
					bestMatch = roleRank;
					bestRoleIndex = i;
				}
			}
			
			if (bestRoleIndex!=-1) {
				roles.get(bestRoleIndex).newRoleSequence(tmp);
				activeRole = roles.get(bestRoleIndex); 
			} else {
				if (tmp.size()>=minRoleSize) {
					IRole newRole = new RoleImpl(interactions);
					newRole.newRoleSequence(tmp);
					roles.add(newRole);
					activeRole = roles.get(roles.size()-1);
				}
			}
		} else {
			activeRole = null;
		}
	}
	
	/**
	 * returns the occurrences of a given state
	 * @param state the state
	 * @return the occurrences of a given state
	 */
	private List<Integer> getOccurence(int state) {
		return occurrence.get(state);
	}
	
	
	@Override
	public void newObservation(String observation) {
		predictionProbabilityList.add(new Double(getPredictionProbability(observation)));
		predictionRankList.add(new Integer(getPredictionRank(observation)));
		
		
		if (getNextAction().equals(observation)) {
			perfectMatches++;
		}
		perfectMatchesList.add(new Integer(perfectMatches));
		
		updateInteractions(observation);
		int	obs = interactions.getIndex(observation);
		newObservation=true;
		trainingData.add(new Integer(obs));
		occurrence.get(obs).add(trainingData.size()-1);
		
		calcPrediction();
		
		interactionCounts.set(obs, new Integer(interactionCounts.get(obs)+1));
		countAll++;
	}
	
	@Override
	public void newObservations(String[] obs) {
		for (int i=0; i<obs.length; i++) {
			newObservation(obs[i]);
		}
	}
	
	/**
	 * updates the interactions and all data structures of this model
	 * @param inter new interaction
	 */
	private void updateInteractions(String inter) {
		if (!interactions.contains(inter)) {
			interactions.newInteraction(inter);
			occurrence.add(new ArrayList<Integer>());
			interactionCounts.add(new Integer(0));	
		} 
	}
	
	@Override
	public String getNextAction() {
		double[] vector = calcPrediction();
		int nextAction = -1;
		double prob = 0.0f;
		
		for (int i=0; i<vector.length; i++) {
			if (vector[i]>prob) {
				nextAction = i;
				prob = vector[i];
			} 
		}
		return interactions.getInteractionString(nextAction);
	}
	
	
	private double getPredictionProbability(String interaction) {
		if (!interactions.contains(interaction)) return 0.0d;
		Double result =  new Double(calcPrediction()[interactions.getIndex(interaction)]);
		if (result.isNaN()) {
			return 0.0d;
		} else {
			return result;
		}
	}
	
	private int getPredictionRank(String interaction) {
		double[] v = calcPrediction();
		if (!interactions.contains(interaction)) return interactions.getSize();
		double toProb = v[interactions.getIndex(interaction)];
		int rank = 1;
		for (int i=0; i<v.length; i++) {
			Double test = new Double(v[i]);
			if ((v[i]>=toProb && interactions.getIndex(interaction)!=i) || test.isNaN()) {
				rank++;
			}
		}
		return rank;
	}
	
	@Override
	public PredictionProbabilities getPredictionProbabilities() {
		if (result==null) {
			return new PredictionProbabilities(new double[0], new String[0]);
		}
		double[] values = result.clone();
		String[] indices = new String[interactions.getSize()];
		for (int i=0; i<indices.length; i++) {
			indices[i] = interactions.getInteractionString(i);
		}
	 
		return new PredictionProbabilities(values, indices);
	}
	
	@Override
	public String[] getNextActions(int count){
		String[] result = new String[count];
		double lastMax = Double.POSITIVE_INFINITY;
		int lastAction = -1;
		
		// initialize result
		for (int i=0; i<count; i++) {
			result[i] = "";
		}
		
		double[] stateTrans = calcPrediction();
		for (int i=0; i<count; i++) {
			double tmp = 0.0f;
			for (int j=0; j<stateTrans.length; j++) {
				if (stateTrans[j]>tmp && stateTrans[j]<lastMax) {
					tmp = stateTrans[j];
					lastAction = j;
				}
			}
			lastMax = tmp;
			result[i] = interactions.getInteractionString(lastAction);
		}
		return result;
	}
	
	@Override
	public QueryResult query(int[] ranks) {
		HashMap<String,Integer> map = new HashMap<String, Integer>();
		ArrayList<Double> prob = new ArrayList<Double>(); 
		
		SteadyStateVector ssv = getSteadyStateVector();
		
		for (int i=0; i<ssv.getSize();i++) {
			String stateAbst = Abstractions.getAbstraction(ssv.getIndices()[i], ranks);
			if (stateAbst!= null) {
				if (!map.containsKey(stateAbst)) {
					map.put(stateAbst,map.size());
					prob.add(new Double(ssv.getValues()[i]));
				} else {
					prob.set(map.get(stateAbst), new Double(prob.get(map.get(stateAbst)).doubleValue() + ssv.getValues()[i]));
				}
			}
		}
		
		double[] result = new double[map.size()];
		String[] indices = new String[map.size()];
		int i=0;
		for (Iterator<String> it = map.keySet().iterator(); it.hasNext();) {
			indices[i] = it.next();
	        result[i] = prob.get(map.get(indices[i]));
	        i++;
		}
		
		return new QueryResult(result, indices);
	}
	
	@Override
	public SteadyStateVector getSteadyStateVector() {
		SteadyStateVector ssv = new SteadyStateVector(interactions.getSize());
		ssv.setIndices(interactions.getInteractions());
		
		for (int i=0; i<interactions.getSize(); i++) {
			ssv.setProbability(i, ((double)interactionCounts.get(i))/((double)countAll));
		}
		return ssv;
	}
	
	@Override
	public void setTrainingsData(String[] data) {
		for (int i=0; i<data.length; i++) {
			
			updateInteractions(data[i]);
				
			int	obs = interactions.getIndex(data[i]);
			trainingData.add(obs);
			occurrence.get(obs).add(trainingData.size()-1); 
		}
	}
	
	@Override
	public void enableRoleLearning(boolean b) {
		roleLearning = true;
	}
	
	@Override
	public int getRoleCounts() {
		return roles.size();
	}
	
	@Override
	public int getActiveRole() {
		for (int i=0; i<roles.size(); i++) {
			if (roles.get(i)==activeRole) {
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public String[] getRoleSequences(int role) {
		//System.out.println("#### GetRoleSequences");
		//System.out.println("     Role = " + role);
		//System.out.println("     RoleCount = "+ roles.size());
		if (role < roles.size() && role >=0) {
			IRole rol = roles.get(role);
			List<List<Integer>> sequences = rol.getSequences();
			String[] result = new String[sequences.size()];
			for (int i=0; i<sequences.size(); i++) {
				//System.out.println("    Sequence " + i);
				for (int j=0; j<sequences.get(i).size(); j++) {
					if (result[i]==null || result[i].equals("")) {
						result[i] = sequences.get(i).get(j).toString();
					} else {
						result[i] = result[i] + " " + sequences.get(i).get(j).toString();
					}
				}
				//System.out.println("       " + result[i]);
			}
			//System.out.println("     Returning result");
			
			return result;
		} else {
			return new String[0];
		}
	}
	
	@Override
	public SteadyStateVector getRoleSSV(int roleIndex) {
		return roles.get(roleIndex).getSteadyStateVector();
	}
	
	@Override
	public QueryResult queryRole(int[] ranks, int roleIndex) {
		return roles.get(roleIndex).query(ranks);
	}
	
	@Override
	public Interactions getInteractions() {
		return interactions;
	}

	@Override
	public double[] getPredictionProbabilityList() {
		double[] result = new double[predictionProbabilityList.size()];
		for (int i=0; i<predictionProbabilityList.size(); i++) {
			result[i] = predictionProbabilityList.get(i).doubleValue();
		}
		return result;
	}

	@Override
	public int[] getPredictionRankList() {
		int[] result = new int[predictionRankList.size()];
		for (int i=0; i<predictionRankList.size(); i++) {
			result[i] = predictionRankList.get(i).intValue();
		}
		return result;
	}

	@Override
	public int[] getperfectMatchesList() {
		int[] result = new int[perfectMatchesList.size()];
		for (int i=0; i<perfectMatchesList.size(); i++) {
			result[i] = perfectMatchesList.get(i).intValue();
		}
		return result;
	}
}
