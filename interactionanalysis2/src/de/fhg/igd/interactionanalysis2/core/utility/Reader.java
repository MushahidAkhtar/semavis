package de.fhg.igd.interactionanalysis2.core.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * This class contains Methods for reading interactions from files
 * 
 * @author Christian Stab
 */
public class Reader {

	/**
	 * reads the interactions from the given file and returns the interactions
	 * in the intern representation
	 * @param fileName filename
	 * @return interactions
	 */
	public static String[][] readData(String fileName) {
		if (fileName!= null && (new File(fileName)).exists()) {
			ArrayList<String> inter = new ArrayList<String>();
			try {
				FileReader reader = new FileReader(fileName);
				BufferedReader in = new BufferedReader(reader);
				while (in.ready()) {
					String line = in.readLine();
					//System.out.println(line);
					inter.add(line);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			String[][] result = new String[inter.size()][];
			for (int i=0; i<inter.size();i++) {
				result[i] = inter.get(i).split(" ");
			}
			
			return result;
		} else {
			return null;
		}
	}
	
	/**
	 * reads the interactions from the given file and returns the interactions
	 * in the intern representation
	 * @param fileName filename
	 * @return interactions
	 */
	public static String[] readData2(String fileName) {
		if (fileName!= null && (new File(fileName)).exists()) {
			ArrayList<String> inter = new ArrayList<String>();
			try {
				FileReader reader = new FileReader(fileName);
				BufferedReader in = new BufferedReader(reader);
				while (in.ready()) {
					String line = in.readLine();
					//System.out.println(line);
					inter.add(line);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			String[] result = new String[inter.size()];
			for (int i=0; i<inter.size();i++) {
				result[i] = inter.get(i);
			}
			
			return result;
		} else {
			return null;
		}
	}
}
