package de.fhg.igd.interactionanalysis2.core.utility;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


/**
 * This Class provides some functions to obtain the configuration of the system
 * from an XML file
 * 
 * @author Christian Stab
 */
public class Config {

	/** Logger */
	static Logger logger = Logger.getLogger(Config.class);
	
	/** location of the configuration file */
	public static String defaultConfigFile="config.xml";
	
	/** the location of the configuration file */
	private String configFile;
	
	/** the instance of this singleton */
	private static Config instance;
	
	/**
	 * basic constructor
	 */
	private Config(String configFile) {
		if (configFile==null) {
			this.configFile = defaultConfigFile;
		} else {
			this.configFile = configFile;
		}
	}
	
	/**
	 * returns the Instance of this singleton
	 * @param config the location of the configuration file; if null the default name 
	 * of the configuration file is used
	 * @return the instance of this singleton
	 */
	public static synchronized Config getInstance(String config) {
		if (instance==null) {
			instance = new Config(config);
		}
		return instance;
	
	}
	
	/**
	 * returns the location of the training file for the probabilistic method
	 * @return the location of the training file for the probabilistic method
	 */
	public String getTrainingsFile() {
		logger.info("### [START] parsing Trainings File Location from file '" + configFile + "' ...");
		String res="";
		try {			
		    DocumentBuilderFactory factory  = DocumentBuilderFactory.newInstance();
		    DocumentBuilder builder  = factory.newDocumentBuilder();
		    Document document = builder.parse(new File(configFile));
		    
		    NodeList ndList = document.getElementsByTagName("config");
		    ndList=ndList.item(0).getChildNodes();
		    for(int i=0; i<ndList.getLength(); i++) {
		    	
		    	if (ndList.item(i).getNodeName().equals("trainingsFile")) {
		    		res= ndList.item(i).getTextContent();
		    	}
		    }
	
		} catch(SAXParseException pe) {
			logger.error("Parsing error in line " + pe.getLineNumber());
			logger.error(pe.getMessage());
			logger.error(pe.getStackTrace());
	    } catch(SAXException se) {
	    	logger.error(se.getStackTrace());
	    } catch( ParserConfigurationException pce ) {
	    	logger.error(pce.getStackTrace());
	    } catch( IOException ioe ) {
	        logger.error(ioe.getStackTrace());
	    }
	    logger.info("### [END] p�arsing Trainings File Location (value=" +  res + ")  captured successfully!");
		return res;
	}
	
	
	/**
	 * returns the name of the used Model
	 * @return name of the model 
	 */
	public String getModelName() {
		logger.info("### [START] parsing model name from file '" + configFile + "' ...");
		String res="";
		try {
			//logger.info(System.getProperty(System.getProperty("user.dir")));
		    DocumentBuilderFactory factory  = DocumentBuilderFactory.newInstance();
		    DocumentBuilder builder  = factory.newDocumentBuilder();
		    Document document = builder.parse(new File(configFile));
		    
		    NodeList ndList = document.getElementsByTagName("config");
		    ndList=ndList.item(0).getChildNodes();
		    for(int i=0; i<ndList.getLength(); i++) {
		    	
		    	if (ndList.item(i).getNodeName().equals("model")) {
		    		res= ndList.item(i).getTextContent();
		    	}
		    }

		} catch(SAXParseException pe) {
			logger.error("Parsing error in line " + pe.getLineNumber());
			logger.error(pe.getMessage());
			logger.error(pe.getStackTrace());
	    } catch(SAXException se) {
	    	logger.error(se.getStackTrace());
	    	logger.error(se.getMessage());
	    } catch( ParserConfigurationException pce ) {
	    	logger.error(pce.getStackTrace());
	    	logger.error(pce.getMessage());
	    } catch( IOException ioe ) {
	        logger.error(ioe.getStackTrace());
	        logger.error(ioe.getMessage());
	    }
	    logger.info("### [END] model name (value=" +  res + ")  captured successfully!");
		return res;
	}
	
	/**
	 * returns the parameter of the storage method
	 * @return name of the model 
	 */
	public boolean getStoreParam() {
		logger.info("### [START] parsing storing parameter from file '" + configFile + "' ...");
		String res="";
		try {
			//logger.info(System.getProperty(System.getProperty("user.dir")));
		    DocumentBuilderFactory factory  = DocumentBuilderFactory.newInstance();
		    DocumentBuilder builder  = factory.newDocumentBuilder();
		    Document document = builder.parse(new File(configFile));
		    
		    NodeList ndList = document.getElementsByTagName("config");
		    ndList=ndList.item(0).getChildNodes();
		    for(int i=0; i<ndList.getLength(); i++) {
		    	
		    	if (ndList.item(i).getNodeName().equals("storeall")) {
		    		res= ndList.item(i).getTextContent();
		    	}
		    }

		} catch(SAXParseException pe) {
			logger.error("Parsing error in line " + pe.getLineNumber());
			logger.error(pe.getMessage());
			logger.error(pe.getStackTrace());
	    } catch(SAXException se) {
	    	logger.error(se.getStackTrace());
	    	logger.error(se.getMessage());
	    } catch( ParserConfigurationException pce ) {
	    	logger.error(pce.getStackTrace());
	    	logger.error(pce.getMessage());
	    } catch( IOException ioe ) {
	        logger.error(ioe.getStackTrace());
	        logger.error(ioe.getMessage());
	    }
	    logger.info("### [END] storing parameter (value=" +  res + ")  captured successfully!");
		return Boolean.parseBoolean(res);
	}	
}
