package de.fhg.igd.interactionanalysis2.core.utility;

public class Abstractions {
	/**
	 * returns the abstraction string for the given interaction and ranks
	 * @param str interaction 
	 * @param ranks ranks
	 * @return abstraction string
	 */
	public static String getAbstraction(String str, int[] ranks) {
		String[] t = str.split(" ");
		String[][] u = new String[t.length][];
		for (int i=0; i<u.length;i++){
			u[i]=t[i].split("\\.");
		}
		
		String abstraction = "";
		for (int i=0; i<ranks.length; i++) {
			
			for (int j=0;j<=ranks[i]; j++) {
				//System.out.println(u[i][j]);
				if (u[i].length>j) {
					abstraction = abstraction + u[i][j];
					if (u[i].length>j+1 && ranks[i]>j) {
						abstraction = abstraction + ".";
					}
				} else {
					return null;
				}
			}
			if (i<ranks.length-1) {
				abstraction = abstraction + " ";
			}
		}
		return abstraction;
	}
}
