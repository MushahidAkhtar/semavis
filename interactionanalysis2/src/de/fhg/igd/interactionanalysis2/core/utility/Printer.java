package de.fhg.igd.interactionanalysis2.core.utility;

import de.fhg.igd.interactionanalysis2.core.data.QueryResult;
import de.fhg.igd.interactionanalysis2.core.data.SteadyStateVector;

/**
 * This Class contains methods for printing matrices 
 * 
 * @author Christian Stab
 */
public class Printer {
	
	/**
	 * prints a matrix
	 * @param matrix StateTransitionMatrix
	 */
	public static void printMatrix(double[][] matrix) {
		for (int i=0; i<matrix.length; i++) {
			System.out.print("\t[" + i + "]");
		}
		System.out.println();
		for (int i=0; i<matrix.length+1; i++) {
			System.out.print("--------");
		}
		System.out.println();
		for (int i=0; i<matrix.length;i++) {
			if (i<10) {
				System.out.print("[0" + i +"]  |");
			} else{
				System.out.print("[" + i +"]  |");
			}
			for (int j=0; j<matrix[i].length; j++) {
				double prob = trunc(matrix[i][j]);
				boolean s = (prob*100)%10!=0;
				System.out.print("\t" + ((prob<10)?" ":"") + prob + ((s)?"%":"0%"));
			}
			System.out.println();
		}
		System.out.println();
	}
	
	/**
	 * prints a vector
	 * @param ssv steady state vector
	 */
	public static void printVector(double[] ssv) {
		for (int i=0; i<ssv.length; i++) {
			if (i>0) {
				System.out.print("\t[" + i + "]");
			} else {
				System.out.print("[" + i + "]");
			}
		}
		System.out.println();
		for (int i=0; i<ssv.length; i++) {
			System.out.print("--------");
		}
		System.out.println();
		for (int j=0; j<ssv.length; j++) {
			double prob = trunc(ssv[j]);
			boolean s = (prob*100)%10!=0;
			if (j>0) {
				System.out.print("\t" + ((prob<10)?" ":"") + prob + ((s)?"%":"0%"));
			} else {
				System.out.print(((prob<10)?" ":"") + prob + ((s)?"%":"0%"));
			}
		}
		System.out.println();
	}
	
	/**
	 * Print steady stage vector
	 * @param ssv steady state vector
	 */
	public static void printSSV(SteadyStateVector ssv) {
		//System.out.println("Steady State Vector");
		String[] indeces = ssv.getIndices();
		double[] values = ssv.getValues();
		printSSV(indeces, values);
	}
	
	/**
	 * Print steady stage vector
	 * @param indices indices
	 * @param values values
	 */
	public static void printSSV(String[] indices, double[] values) {
		System.out.println("Steady State Vector");
		double sum = 0.0d;
		for (int i=0; i<indices.length; i++) {
			System.out.print(indices[i] + "\t");
			sum = sum + values[i];
			double prob = trunc(values[i]);
			boolean s = (prob*100)%10!=0;
			System.out.print("\t" + ((prob<10)?" ":"") + prob + ((s)?"%":"0%"));
			System.out.println();
		}
		System.out.println();
		System.out.println("-----------------");
		System.out.println("Sum = " + sum);
	}
	
	/**
	 * Prints the query result
	 * @param result QueryResult
	 */
	public static void printQueryResult(QueryResult result) {
		System.out.println("QueryResult");
		double[] values = result.getValues();
		String[] indices = result.getIndices();
		printQueryResult(indices, values);
	}
	
	/**
	 * Prints the query result
	 * @param indices indices
	 * @param values values
	 */
	public static void printQueryResult(String[] indices, double[] values) {
		//System.out.println("QueryResult");
		double sum = 0.0f;
		for (int i=0; i<indices.length; i++) {
			sum = sum + values[i];
			System.out.print(indices[i] + "\t");
			double prob = trunc(values[i]);
			boolean s = (prob*100)%10!=0;
			System.out.print(((prob<10)?" ":"") + prob + ((s)?"%":"0%"));
			System.out.println();
		}
		System.out.println();
		System.out.println("-----------------");
		System.out.println("Sum = " + sum);
	}
	
	/**
	 * truncates a double for better readability (two decimal points)
	 * @param f double
	 * @return truncated double
	 */
	private static double trunc(double f) {
		return (double)(((int)(f*10000))/100.0);
	}
	
	
	
	/**
	 * prints a state 
	 * @param state State
	 */
	public static void printState2(String[] state) {
		if (state!=null) {
			for (int i=0; i<state.length; i++) {
				System.out.print(state[i] +"  ");
			}
		} else {
			System.out.print("no state");
		}
	}
	
	/**
	 * prints the sum of every line of the transition matrix
	 * @param matrix the matrix
	 */
	public static void printMatrixSum(double[][] matrix) {
		double[] test = new double[matrix.length];
		for (int i=0;i<matrix.length; i++) {
			for (int j=0;j<matrix[i].length; j++) {
				test[i]=test[i]+ matrix[i][j];
			}
		}
		
		System.out.println("Consistency Check:");
		for (int i=0; i<test.length; i++) {
			System.out.print("\t[" + ((i<10)?"0":"") + i + "]");
		}
		System.out.println();
		for (int i=0; i<test.length; i++) {
			System.out.print("\t" + test[i]);
		}
		System.out.println();
		System.out.println();
	}
}
