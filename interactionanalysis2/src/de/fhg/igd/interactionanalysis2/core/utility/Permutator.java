package de.fhg.igd.interactionanalysis2.core.utility;

/**
 * This class provides functions for calculation of permutations
 * to calculate all possible states or state abstractions of one state.
 * @author Christian Stab
 */
public class Permutator {

	/**
	 * Returns all Permutations
	 * @param nodes 
	 * @return permutations
	 */
	public static String[][] getPermutations(String[][] nodes) {
		// Calculate Number of States
		int numberOfStates = 1;
		for (int i=0; i<nodes.length; i++) {
			numberOfStates = numberOfStates * nodes[i].length;
		}
		
		// Calculate all States
		String states[][] = new String[numberOfStates][nodes.length];
		int countForNode = 0;
		for (int i=nodes.length-1; i>=0; i--) {
			if (i==nodes.length-1) {
				countForNode = 1;
			} else {
				countForNode = countForNode * nodes[i+1].length;
			}
			for(int j=0; j<numberOfStates; j++) {
				states[j][i]=nodes[i][(j/countForNode)%nodes[i].length];
			}
		}
		return states;
	}
	
//	/**
//	 * Returns all Permutations
//	 * @param nodes 
//	 * @return permutations
//	 */
//	public static int[][] getPermutations(int[][] nodes) {
//		// Calculate Number of States
//		int numberOfStates = 1;
//		for (int i=0; i<nodes.length; i++) {
//			numberOfStates = numberOfStates * nodes[i].length;
//		}
//		
//		// Calculate all States
//		int states[][] = new int[numberOfStates][nodes.length];
//		int countForNode = 0;
//		for (int i=nodes.length-1; i>=0; i--) {
//			if (i==nodes.length-1) {
//				countForNode = 1;
//			} else {
//				countForNode = countForNode * nodes[i+1].length;
//			}
//			for(int j=0; j<numberOfStates; j++) {
//				states[j][i]=nodes[i][(j/countForNode)%nodes[i].length];
//			}
//		}
//		return states;
//	}
	
}
