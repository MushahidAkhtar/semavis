package de.fhg.igd.interactionanalysis2.usermanagement;

import java.util.HashMap;

import de.fhg.igd.interactionanalysis2.core.IPM;
import de.fhg.igd.interactionanalysis2.persistency.PMFactory;
import de.fhg.igd.interactionanalysis2.persistency.Persistency;

/**
 * Holds the models of the active users. This class loads, stores, and updates
 * the probabilistic models for every user.
 * 
 * @author Christian Stab
 */
public class UserManagement {
	
	/** Probabilistic models for the active users */
	private HashMap<String, IPM> pms;
	
	/** instance of this singleton */
	private static UserManagement instance;
	
	/**
	 * Basic Constructor
	 */
	private UserManagement () {
		pms = new HashMap<String, IPM>();
	}
	
	/**
	 * returns the instance of this singleton 
	 * @return instance of this singleton
	 */
	public static synchronized UserManagement getInstance () {
		if (UserManagement.instance == null) {
			UserManagement.instance = new UserManagement ();
	    }
	    return UserManagement.instance;
	}
	
	/**
	 * loads a probabilistic model from the database in the pool or creates
	 * a new one if no exists in database.
	 * @param userID id of the user
	 * @return true if successful
	 */
	public boolean login(String userID) {
		//System.out.println("UserManagement: LOGIN user: " + userID);
		
		//System.out.println("UserManagement: LOGIN user: Load Model" );
		IPM pm = pms.get(userID);
		if (pm!=null) {
			// user is already logged in
			return false;
		} else {
			pm = Persistency.getInstance().loadPM(userID);
			if (pm==null) {
				pm = PMFactory.getInstance().createPM();
				//System.out.println("Usermanagement: Created new model for user " + userID + " ["+ pm.getClass().toString() + "]");
				pms.put(userID, pm);
			} else {
				//System.out.println("Usermanagement: load model for user " + userID + " ["+ pm.getClass().toString() + "]");
				pms.put(userID, pm);
			}
			return true;
		}
	}
	
	/**
	 * stores the probabilistic model and removes it from the pool.
	 * @param userID
	 * @return the probabilistic model and removes it from the pool.
	 */
	public boolean logout(String userID) {
		//System.out.println("UserManagement: LOGOUT user: " + userID);
		//pms.get(userID).resetPreviousObservation();
		if (pms.get(userID)!=null) {
			Persistency.getInstance().savePM(userID, pms.get(userID));
			pms.remove(userID);
			//System.out.println("Usermanagement: Active Models: " + pms.size());
			return true;
		} else {
			return false;
		}
		
	}
	
	/**
	 * returns the probabilistic model of the user
	 * @param userID id of the user
	 * @return probabilistic model of the user
	 */
	public IPM getModel(String userID) {
		//System.out.println("UserManagement: getModel() of user " + userID);
		if (pms.get(userID)==null) {
			IPM pm = Persistency.getInstance().loadPM(userID);
			if (pm!=null) pms.put(userID, pm);
		}
		return pms.get(userID);
	}

}
