package de.fhg.igd.interactionanalysis2.ws;

import java.util.Date;

import flex.messaging.MessageBroker;
import flex.messaging.messages.AsyncMessage;

public class IAResponder {

	public static void sendMessageToClients(String messageBody) {
		AsyncMessage msg = new AsyncMessage();

		msg.setClientId("Java-Based-Producer-For-Messaging");
		msg.setTimestamp(new Date().getTime());
		//you can create a unique id
		msg.setMessageId("Java-Based-Producer-For-Messaging-ID");
		//destination to which the message is to be sent
		msg.setDestination("ia_responder");		
		//set message body
		msg.setBody(messageBody != null?messageBody:"");
		//set message header
		msg.setHeader("sender", "From the server");

		//send message to destination
		MessageBroker.getMessageBroker(null).routeMessageToService(msg, null);			
	}
}
