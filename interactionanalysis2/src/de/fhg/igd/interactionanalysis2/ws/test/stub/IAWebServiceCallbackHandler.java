
/**
 * IAWebServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

    package de.fhg.igd.interactionanalysis2.ws.test.stub;

    /**
     *  IAWebServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class IAWebServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public IAWebServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public IAWebServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getPredictionProbability method
            * override this method for handling normal response from getPredictionProbability operation
            */
           public void receiveResultgetPredictionProbability(
                    de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.GetPredictionProbabilityResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPredictionProbability operation
           */
            public void receiveErrorgetPredictionProbability(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for query method
            * override this method for handling normal response from query operation
            */
           public void receiveResultquery(
                    de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.QueryResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from query operation
           */
            public void receiveErrorquery(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for newObservations method
            * override this method for handling normal response from newObservations operation
            */
           public void receiveResultnewObservations(
                    de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.NewObservationsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from newObservations operation
           */
            public void receiveErrornewObservations(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSteadyStateVector method
            * override this method for handling normal response from getSteadyStateVector operation
            */
           public void receiveResultgetSteadyStateVector(
                    de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.GetSteadyStateVectorResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSteadyStateVector operation
           */
            public void receiveErrorgetSteadyStateVector(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getRoleSSV method
            * override this method for handling normal response from getRoleSSV operation
            */
           public void receiveResultgetRoleSSV(
                    de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.GetRoleSSVResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getRoleSSV operation
           */
            public void receiveErrorgetRoleSSV(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for login method
            * override this method for handling normal response from login operation
            */
           public void receiveResultlogin(
                    de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.LoginResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from login operation
           */
            public void receiveErrorlogin(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getNextActions method
            * override this method for handling normal response from getNextActions operation
            */
           public void receiveResultgetNextActions(
                    de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.GetNextActionsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getNextActions operation
           */
            public void receiveErrorgetNextActions(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for newObservation method
            * override this method for handling normal response from newObservation operation
            */
           public void receiveResultnewObservation(
                    de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.NewObservationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from newObservation operation
           */
            public void receiveErrornewObservation(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for logoff method
            * override this method for handling normal response from logoff operation
            */
           public void receiveResultlogoff(
                    de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.LogoffResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from logoff operation
           */
            public void receiveErrorlogoff(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for queryRole method
            * override this method for handling normal response from queryRole operation
            */
           public void receiveResultqueryRole(
                    de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.QueryRoleResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from queryRole operation
           */
            public void receiveErrorqueryRole(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getActiveRole method
            * override this method for handling normal response from getActiveRole operation
            */
           public void receiveResultgetActiveRole(
                    de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.GetActiveRoleResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getActiveRole operation
           */
            public void receiveErrorgetActiveRole(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getRoleCounts method
            * override this method for handling normal response from getRoleCounts operation
            */
           public void receiveResultgetRoleCounts(
                    de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.GetRoleCountsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getRoleCounts operation
           */
            public void receiveErrorgetRoleCounts(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getNextAction method
            * override this method for handling normal response from getNextAction operation
            */
           public void receiveResultgetNextAction(
                    de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.GetNextActionResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getNextAction operation
           */
            public void receiveErrorgetNextAction(java.lang.Exception e) {
            }
                


    }
    