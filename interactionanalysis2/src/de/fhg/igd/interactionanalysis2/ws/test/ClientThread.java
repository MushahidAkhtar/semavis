package de.fhg.igd.interactionanalysis2.ws.test;

import de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.SteadyStateVector;

/**
 * A Tread for a client
 * 
 * @author Christian Stab
 */
public class ClientThread  extends Thread {
	
	/** the interactions */
	private String[] interactions;
	
	/** Client for this thread */
	private Client client;
	
	/** the maximum delay between sending observations */
	private long maxDelay;
	
	/**
	 * Basic Constructor
	 * @param interactions
	 */
	public ClientThread(String name, String[] interactions, long maxDelay) {
		this.interactions=interactions;
		client = new Client(name);
		
	}
	
	@Override
	public void run() {
		System.out.println(client.getName() + " logs in");
		client.login();
		
		for (int i=0;i<interactions.length; i++) {
			client.newObservation(interactions[i]);
			System.out.println(client.getName() + " sends an interaction " + i + " to the server");
			try {
				Thread.sleep((long)Math.random()*maxDelay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("Steady State Vector for client " + client.getName());
		SteadyStateVector ssv = client.getSSV();
		String[] indeces = ssv.getIndices();
		double[] values = ssv.getValues();
		for (int i=0; i<ssv.getIndices().length; i++) {
			System.out.print(indeces[i] + "\t");
			
			float prob = (float)(((int)(values[i]*10000))/100.0);
			boolean s = (prob*100)%10!=0;
			System.out.print("\t" + ((prob<10)?" ":"") + prob + ((s)?"%":"0%"));
			System.out.println();
		}
		
		System.out.println(client.getName() + " logs out");
		client.logoff();
	}

}
