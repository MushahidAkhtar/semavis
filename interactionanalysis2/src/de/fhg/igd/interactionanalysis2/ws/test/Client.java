package de.fhg.igd.interactionanalysis2.ws.test;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;

import de.fhg.igd.interactionanalysis2.core.utility.Printer;
import de.fhg.igd.interactionanalysis2.core.utility.Reader;
import de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub;
import de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.GetActiveRoleResponse;
import de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.GetPredictionProbabilityResponse;
import de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.GetRoleCountsResponse;
import de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.GetRoleSSVResponse;
import de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.GetSteadyStateVectorResponse;
import de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.Logoff;
import de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.QueryResponse;
import de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.QueryResult;
import de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.QueryRoleResponse;
import de.fhg.igd.interactionanalysis2.ws.test.stub.IAWebServiceStub.SteadyStateVector;


/**
 * Class for testing the Service
 * 
 * @author Christian Stab
 */
public class Client {
	
	/** id of the user */
	private String userID;
	
	/** Web Service stub*/
	private IAWebServiceStub stub;
	
	/**
	 * Basic Constructor
	 * @param userID
	 */
	public Client(String userID) {
		this.userID = userID;
		try {
			stub = new IAWebServiceStub();
		} catch (AxisFault e) {
			e.printStackTrace();
		}
	}
 
	/**
	 * logs the user in
	 */
	public void login() {
		//Create the request
        IAWebServiceStub.Login request = new IAWebServiceStub.Login();
        request.setUserID(userID);
        
        //Invoke the service
        IAWebServiceStub.LoginResponse response;
        try {
			response = stub.login(request);
			System.out.println(response.get_return());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * logs the user out.
	 */
	public void logoff() {
		//Create the request
        Logoff request = new IAWebServiceStub.Logoff();
        request.setUserID(userID);
        
        //Invoke the service
        IAWebServiceStub.LogoffResponse response;
        try {
			response = stub.logoff(request);
			System.out.println(response.get_return());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * reports a new observation (interaction)
	 * @param interaction the interaction
	 */
	public void newObservation(String interaction) {
		//Create the request
		IAWebServiceStub.NewObservation request = new IAWebServiceStub.NewObservation();
		request.setUserID(userID);
		request.setInteraction(interaction);
		
		//Invoke the service
		IAWebServiceStub.NewObservationResponse response;
		try {
			response = stub.newObservation(request);
			System.out.println(response.get_return());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * reports new observations (interactions)
	 * @param interactions the interactions
	 */
	public void newObservations(String[] interactions) {
		String interactions_tmp = "";
		for (int i=0; i< interactions.length; i++) {
			if (i>0) {
				interactions_tmp = interactions_tmp + "," + interactions[i];
			} else {
				interactions_tmp = interactions[i];
			}
		}
		
		//Create the request
		IAWebServiceStub.NewObservations request = new IAWebServiceStub.NewObservations();
		request.setUserID(userID);
		request.setInteractions(interactions_tmp);
		
		//Invoke the service
		IAWebServiceStub.NewObservationsResponse response;
		try {
			response = stub.newObservations(request);
			System.out.println(response.get_return());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * returns the next possible action 
	 * @return next possible action
	 */
	public String getNextAction() {
		//Create the request
		IAWebServiceStub.GetNextAction request = new IAWebServiceStub.GetNextAction();
		request.setUserID(userID);
		
		//Invoke the service
		String result = "";
		IAWebServiceStub.GetNextActionResponse response;
		try {
			response = stub.getNextAction(request);
			result = response.get_return();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * returns the next most possible actions
	 * @return next most possible actions
	 */
	public String[] getNextActions(int number) {
		//Create the request
		IAWebServiceStub.GetNextActions request = new IAWebServiceStub.GetNextActions();
		request.setUserID(userID);
		request.setNumber(number);
		
		//Invoke the service
		String[] result = null;
		IAWebServiceStub.GetNextActionsResponse response;
		try {
			response = stub.getNextActions(request);
			result = response.get_return();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public SteadyStateVector getSSV() {
		//Create the request
		IAWebServiceStub.GetSteadyStateVector request = new IAWebServiceStub.GetSteadyStateVector();
		request.setUserID(userID);
		
		//Invoke the service
		GetSteadyStateVectorResponse response = null;
		try {
			response = stub.getSteadyStateVector(request);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return response.get_return();
	}
	
	public QueryResult query(int[] ranks) {
		String ranks_tmp = "";
		for (int i=0; i<ranks.length; i++) {
			if (i>0) {
				ranks_tmp = ranks_tmp + "," + Integer.toString(ranks[i]);
			} else {
				ranks_tmp = Integer.toString(ranks[i]);
			}
		}
		
		//Create the request
		IAWebServiceStub.Query request = new IAWebServiceStub.Query();
		request.setRanks(ranks_tmp);
		request.setUserID(userID);
		
		//Invoke the service
		QueryResponse response = null;
		try {
			response = stub.query(request);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return response.get_return();
	}
	
	public double getPredictionProbability(String state) {
		IAWebServiceStub.GetPredictionProbability request = new IAWebServiceStub.GetPredictionProbability();
		request.setUserID(userID);
		request.setInteraction(state);
		
		GetPredictionProbabilityResponse response = null;
		try {
			response = stub.getPredictionProbability(request);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return response.get_return();
	}

	public int getRoleCounts() {
		IAWebServiceStub.GetRoleCounts request = new IAWebServiceStub.GetRoleCounts();
		request.setUserID(userID);
		
		GetRoleCountsResponse response = null;
		try {
			response = stub.getRoleCounts(request);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return response.get_return();
	}
	
	public int getActiveRole() {
		IAWebServiceStub.GetActiveRole request = new IAWebServiceStub.GetActiveRole();
		request.setUserID(userID);
		
		GetActiveRoleResponse response = null;
		try {
			response = stub.getActiveRole(request);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return response.get_return();
	}
	
	public QueryResult queryRole(int[] ranks, int roleIndex) {
		String ranks_tmp = "";
		for (int i=0; i<ranks.length; i++) {
			if (i>0) {
				ranks_tmp = ranks_tmp + "," + Integer.toString(ranks[i]);
			} else {
				ranks_tmp = Integer.toString(ranks[i]);
			}
		}
		
		IAWebServiceStub.QueryRole request = new IAWebServiceStub.QueryRole();
		request.setUserID(userID);
		request.setRanks(ranks_tmp);
		request.setRoleIndex(roleIndex);
		
		QueryRoleResponse response = null;
		try {
			response = stub.queryRole(request);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return response.get_return();
	}
	
	public SteadyStateVector getRoleSSV(int roleIndex) {
		IAWebServiceStub.GetRoleSSV request = new IAWebServiceStub.GetRoleSSV();
		request.setUserID(userID);
		request.setRoleIndex(roleIndex);
		
		GetRoleSSVResponse response = null;
		try {
			response = stub.getRoleSSV(request);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return response.get_return();
	}
	
	/**
	 * returns the name of the client
	 * @return the name of the client
	 */
	public String getName() {
		return userID;
	}
	
	/**
	 * tester
	 * @param args parameters
	 */
	public static void main(String[] args) {
		Client client = new Client("User667");
		
		//login
		System.out.println("Client logs in");
		client.login();
		System.out.println();
		
		//First Action
		String firstAction = client.getNextAction();
		System.out.println("First action is: " + firstAction);
		
		
		//newObservation
		
		System.out.println("Client sends interactions to the server");
		System.out.println();
		String[] interactions = Reader.readData2("interactions/PKITool1.txt");
		for (int i=0; i<interactions.length; i++) {

			client.newObservation(interactions[i]);
			
			System.out.println("Observed Interaction: "+ interactions[i] + " [" + interactions[i] + "]");
			String nextAction = client.getNextAction();
			System.out.println("Next possible action is: " + nextAction );
			
			
			System.out.println("Next possible actions:");
			String[] result = client.getNextActions(5);
			for (int j=0; j<result.length; j++) {
				System.out.print(result[j] + "\t");
			}
			System.out.println();
			System.out.println();
		}
		System.out.println();
		
		//newObservations
		interactions = Reader.readData2("interactions/PKITool2.txt");
		System.out.println("Client sends " + interactions.length + " interactions to the server");
		
		client.newObservations(interactions);
		System.out.println();
		
		
		
		System.out.println("########## Query: ");
		
		
		
		//getNextAction
		System.out.println("Next possible action is: " + client.getNextAction());
		System.out.println();
		
		//getNextActions
		System.out.println("Next possible actions:");
		String[] result = client.getNextActions(5);
		for (int i=0; i<result.length; i++) {
			System.out.print(result[i] + "\t");
		}
		System.out.println();
		System.out.println();
		
		//System.out.println("Steady State Vector:");
		SteadyStateVector ssv = client.getSSV();
		Printer.printSSV(ssv.getIndices(), ssv.getValues());
		System.out.println();
		
		QueryResult queryResult = client.query(new int[] {2,0});
		Printer.printQueryResult(queryResult.getIndices(), queryResult.getValues());
		
		System.out.println(client.getPredictionProbability(client.getNextAction()));
		System.out.println(client.getRoleCounts());
		System.out.println(client.getActiveRole());
		
		
		SteadyStateVector roleSSV = client.getRoleSSV(client.getActiveRole());
		Printer.printSSV(roleSSV.getIndices(), roleSSV.getValues());
		
		QueryResult roleQueryResult = client.queryRole(new int[] {2,0},client.getActiveRole());
		Printer.printQueryResult(roleQueryResult.getIndices(), roleQueryResult.getValues());
	
		
		//logout
		System.out.println();
		System.out.println("Logout:");
		client.logoff();
	}
	
}
