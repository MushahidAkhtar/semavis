package de.fhg.igd.interactionanalysis2.ws;

import org.apache.log4j.Logger;

import de.fhg.igd.interactionanalysis2.core.IPM;
import de.fhg.igd.interactionanalysis2.core.IRolePM;
import de.fhg.igd.interactionanalysis2.core.data.PredictionProbabilities;
import de.fhg.igd.interactionanalysis2.core.data.QueryResult;
import de.fhg.igd.interactionanalysis2.core.data.SteadyStateVector;
import de.fhg.igd.interactionanalysis2.persistency.Persistency;
import de.fhg.igd.interactionanalysis2.usermanagement.UserManagement;

/**
 * This Class is deployed as web service for the interaction analysis tool
 * 
 * @author Christian Stab
 */
public class IAService {
	
	/** logger for this class */
	static Logger logger = Logger.getLogger(IAService.class);
	
	/**
	 * Loads the probabilistic model for a user or creates a new one if no
	 * model exists
	 * @param userID id of the user
	 * @return result true if user successfully logged in. False if user is already logged in.
	 */
	public boolean login(String userID) {
		logger.info("User '" + userID + "' logged in");
		return UserManagement.getInstance().login(userID);
	}
	
	/**
	 * Stores the Model for the given user
	 * @param userID id of the user
	 * @return true if user successfully logged out
	 */
	public boolean logoff(String userID) {
		logger.info("User '" + userID + "' logged out");
		//System.out.println();
		return UserManagement.getInstance().logout(userID);
	}
	
	/**
	 * This method updates the model when new a observation (interaction)
	 * occurs.
	 * @param userID id of the user
	 * @param interaction String representation of the interaction
	 * @return true if observation captured successfully
	 */
	public boolean newObservation(String userID, String interaction) {
		logger.info("New observation for user '" + userID + "' archieved.");
		//System.out.println("New observation for user " + userID);
		IPM pm = UserManagement.getInstance().getModel(userID);
		
		//System.out.println("Initial probabilities before update");
		//Printer.printVector(pm.getInitialProbabilities());
		
		Persistency.getInstance().saveInteraction(userID, interaction);
		pm.newObservation(interaction);
		
		//Printer.printStateTransitionMatrix(pm);
		//System.out.println("Initial probabilities after update");
		//Printer.printVector(pm.getInitialProbabilities());
		//System.out.println();
		
		IAResponder.sendMessageToClients(userID);
		return true;
	} 
	
	/**
	 * This method updates the model when new observations (interactions)
	 * occur. 
	 * @param userID id of the user
	 * @param interactions String representations of the interactions
	 * @return true if observation captured successfully
	 */
	public boolean newObservations(String userID, String interactions) {
		String tmp[] = interactions.split(",");
		
		//debug
		//for (int i=0; i<tmp.length; i++) {
			//System.out.println("INTERACTION: " + tmp[i]);
		//}
		
		logger.info(tmp.length + " new observations for user '" + userID + "' archieved.");
		//System.out.println(tmp.length + " new observation for user " + userID);
		IPM pm = UserManagement.getInstance().getModel(userID);
		for (int i=0;i<tmp.length; i++) {
			Persistency.getInstance().saveInteraction(userID, tmp[i]);
		}
		pm.newObservations(tmp);
		IAResponder.sendMessageToClients(userID);
		return true;
	}
	
	/**
	 * Returns the next most possible actions of the given user
	 * @param userID id of the user
	 * @param number number of actions
	 * @return the next most possible actions the given user
	 */
	public String[] getNextActions(String userID, int number) {
		logger.info("Delivering the " + number + " next possible actions for user '" + userID + "'");
		String[] actions = UserManagement.getInstance().getModel(userID).getNextActions(number);
		return actions;
	}
	
	/**
	 * returns the next possible action
	 * @param userID id of the user
	 * @return the next possible action
	 */
	public String getNextAction(String userID) {
		logger.info("Delivering the next possible action for user '" + userID + "'");
		return UserManagement.getInstance().getModel(userID).getNextAction();
	}
	
	/**
	 * returns the steady state vector of the given user with the given accuracy
	 * @param userID id of the user
	 * @return the steady state vector 
	 */
	public SteadyStateVector getSteadyStateVector(String userID) {
		logger.info("Delivering steady-state-vector for user '" + userID + "'");
		return UserManagement.getInstance().getModel(userID).getSteadyStateVector();
	}

	/**
	 * queries a model with the given rank and returns the query result 
	 * @param userID id of the user
	 * @param ranks String the ranks of the domain for this query (separated with ',' cause the flex client issue)
	 * @return query result
	 */
	public QueryResult query(String userID, String ranks) {
		logger.info("Delivering query results for user '" + userID + "'");
		String tmp[] = ranks.split(",");
		int[] ranks_tmp = new int[tmp.length];
		for (int i=0; i<tmp.length; i++) {
			ranks_tmp[i] = Integer.parseInt(tmp[i]);
			//System.out.println("RANKS: " + ranks_tmp[i]);
		}
		return UserManagement.getInstance().getModel(userID).query(ranks_tmp);
	}
	
	
	/**
	 * returns the number of roles
	 * @param userID id of the user
	 * @return the number of roles
	 */
	public int getRoleCounts(String userID) {
		logger.info("Delivering role counts for '" + userID + "'");
		IPM pm = UserManagement.getInstance().getModel(userID);
		if (pm instanceof IRolePM) {
			return ((IRolePM)pm).getRoleCounts();
		} else {
			return -1;
		}
	}
	
	/**
	 * returns all prediction probabilities
	 * @param userID id of the user
	 * @return all prediction probabilities
	 */
	public PredictionProbabilities getPredictionProbabilities(String userID) {
		logger.info("Delivering prediction probabilities for '" + userID + "'");
		return UserManagement.getInstance().getModel(userID).getPredictionProbabilities();
		
	}
	
	/**
	 * returns the index of the active role. 
	 * If no role is known this method returns -1
	 * @param userID id of the user
	 * @return the active role
	 */
	public int getActiveRole(String userID) {
		logger.info("Delivering active role for '" + userID + "'");
		IPM pm = UserManagement.getInstance().getModel(userID);
		if (pm instanceof IRolePM) {
			return ((IRolePM)pm).getActiveRole();
		} else {
			return -1;
		}
	}
	
	/**
	 * queries the active role
	 * @param ranks ranks for the query
	 * @param userID id of the user
	 * @param roleIndex
	 * @return QueryResult
	 */
	public QueryResult queryRole(String userID, String ranks, int roleIndex) {
		logger.info("Delivering role query for '" + userID + "'");
		IPM pm = UserManagement.getInstance().getModel(userID);
		String tmp[] = ranks.split(",");
		int[] ranks_tmp = new int[tmp.length];
		for (int i=0; i<tmp.length; i++) {
			ranks_tmp[i] = Integer.parseInt(tmp[i]);
			//System.out.println("RANKS: " + ranks_tmp[i]);
		}
		if (pm instanceof IRolePM) {
			return ((IRolePM)pm).queryRole(ranks_tmp, roleIndex);
		} else {
			return null;
		}
	}
	
	/**
	 * returns the steady state vector of the given role
	 * @param userID id of the user
	 * @param roleIndex the index of the role
	 * @return the steady state vector of the given role
	 */
	public SteadyStateVector getRoleSSV(String userID, int roleIndex) {
		logger.info("Delivering role ssv for '" + userID + "'");
		IPM pm = UserManagement.getInstance().getModel(userID);
		if (pm instanceof IRolePM) {
			return ((IRolePM)pm).getRoleSSV(roleIndex);
		} else {
			return null;
		}
	}
	
	/**
	 * returns an array with all prediction probabilities 
	 * @param userID id of the user
	 * @return an array with all prediction probabilities 
	 */
	public double[] getPredictionProbabilityList(String userID) {
		logger.info("Delivering prediction proability list for '" + userID + "'");
		return UserManagement.getInstance().getModel(userID).getPredictionProbabilityList();
	}
	
	/**
	 * returns an array with all prediction ranks
	 * @param userID id of the user
	 * @return an array with all prediction ranks
	 */
	public int[] getPredictionRankList(String userID) {
		logger.info("Delivering prediction rank list for '" + userID + "'");
		return UserManagement.getInstance().getModel(userID).getPredictionRankList();
	}
	
	/**
	 * returns an array with all perfect matches
	 * @param userID id of the user
	 * @return an array with all perfect matches
	 */
	public int[] getPerfectMatchesList(String userID) {
		logger.info("Delivering perfect matches list for '" + userID + "'");
		return UserManagement.getInstance().getModel(userID).getperfectMatchesList();
	}
	
	/**
	 * returns the sequences of the given Role
	 * @param userID id of the user
	 * @param role the role
	 * @return role sequences
	 */
	public String[] getRoleSequences(String userID, int role) {
		logger.info("Delivering role sequences for '" + userID + "'");
		IPM pm = UserManagement.getInstance().getModel(userID);
		if (pm instanceof IRolePM) {
			//logger.info("  RolePM found  ");
			String[] tmp = ((IRolePM)pm).getRoleSequences(role);
			if (tmp.length==1) {
				String[] tmp2 = new String[2];
				tmp2[0] = tmp[0];
				tmp2[1] = "";
				return tmp2;
			}
			//logger.info("  Result: " + tmp.length);
			//System.out.println("###### IAService getRoleSequence: " + tmp.length);
			return tmp;
		} else {
			//logger.info("###### IAService getRoleSequence: MODEL NOT FOUND!!! (returning NULL)");
			System.out.println("###### IAService getRoleSequence: MODEL NOT FOUND!!! (returning NULL)");
			return null;
		}
	}
}
