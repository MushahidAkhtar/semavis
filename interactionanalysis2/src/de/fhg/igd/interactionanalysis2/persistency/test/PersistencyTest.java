package de.fhg.igd.interactionanalysis2.persistency.test;

import de.fhg.igd.interactionanalysis2.core.IPM;
import de.fhg.igd.interactionanalysis2.core.utility.Printer;
import de.fhg.igd.interactionanalysis2.core.utility.Reader;
import de.fhg.igd.interactionanalysis2.persistency.PMFactory;
import de.fhg.igd.interactionanalysis2.persistency.Persistency;

public class PersistencyTest {
	
	public static void main(String[] args) {
		String fileName = "interactions/PKITool1.txt";
		String[] data = Reader.readData2(fileName);
		
		IPM pm = PMFactory.getInstance().createPM();
		
		// learn model
		for (int i=0;i<data.length; i++) {
			pm.newObservation(data[i]);
			//System.out.println(i + " " + pm.getNextAction());
		}
		
		Printer.printSSV(pm.getSteadyStateVector());
		System.out.println();
		System.out.println("nextAction: " + pm.getNextAction());
		System.out.println();
		Printer.printQueryResult(pm.query(new int[] {1,1}));
		
		System.out.println();
		System.out.println("#################");
		System.out.println();
		
		Persistency.getInstance().savePM("user", pm);
		pm=null;
		pm = Persistency.getInstance().loadPM("user");
		if (pm==null) System.out.println("pm = NULL");
		
		Printer.printSSV(pm.getSteadyStateVector());
		System.out.println();
		System.out.println("nextAction: " + pm.getNextAction());
		System.out.println();
		Printer.printQueryResult(pm.query(new int[] {1,1}));
		
	}
}
