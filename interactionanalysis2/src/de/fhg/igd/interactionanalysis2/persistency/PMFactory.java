package de.fhg.igd.interactionanalysis2.persistency;

import de.fhg.igd.interactionanalysis2.core.IPM;
import de.fhg.igd.interactionanalysis2.core.IRolePM;
import de.fhg.igd.interactionanalysis2.core.KOStar19;
import de.fhg.igd.interactionanalysis2.core.utility.Config;
import de.fhg.igd.interactionanalysis2.core.utility.Reader;

/**
 * Creates probabilistic models dependend on the model defined in the 
 * configuration file
 * 
 * @author Christian Stab
 */
public class PMFactory {

	/** name of the model */
	private String modelName;
	
	/** the instance of this singleton */
	private static PMFactory instance;
	
	/**
	 * Basic Constructor
	 */
	private PMFactory () {
		modelName = Config.getInstance(null).getModelName();
	}
	
	/**
	 * returns the instance of this singleton
	 * @return the instance of this singleton
	 */
	public static synchronized PMFactory getInstance () {
		if (PMFactory.instance == null) {
			PMFactory.instance = new PMFactory ();
	    }
	    return PMFactory.instance;
	}
	
	/**
	 * creates and returns an instance of a probabilistic model
	 * @return an instance of a probabilistic model
	 */
	public IPM createPM() {
		IPM pm = null;
		if (modelName.equals("KOStar19")) {
			pm = new KOStar19();
		}
		
		// train model
		if (pm!=null) {
			String training = Config.getInstance(null).getTrainingsFile();
			System.out.println("TrainingsFile: " + training);
			String[] interactions = Reader.readData2(training);
			
			pm.setTrainingsData(interactions);
//			for (int i=0; i<interactions.length; i++) {
//				pm.newObservation(interactions[i]);
//			}
		}
		if (pm instanceof IRolePM) {
			((IRolePM) pm).enableRoleLearning(true);
		}
		return pm;
	}
}
