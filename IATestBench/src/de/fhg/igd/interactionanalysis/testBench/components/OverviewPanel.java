package de.fhg.igd.interactionanalysis.testBench.components;


import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import de.fhg.igd.interactionanalysis.testBench.IATestBench;
import de.fhg.igd.interactionanalysis.testBench.data.TestData;


/**
 *
 * @author Christian
 */
public class OverviewPanel extends javax.swing.JPanel {

    /** its serial number */
	private static final long serialVersionUID = 1397328934412257996L;
	
	/** Test Data */
	private TestData data;
	
	private IATestBench bench;
	
	private boolean[] perfectMatchesMax;
	private boolean[] mppMax;
	private boolean[] mpaMax;
	private boolean[] ranksMax;
	
	/** Creates new form TestResultPanel */
    public OverviewPanel(TestData data, IATestBench bench) {
        initComponents();
        this.data=data;
        this.bench = bench;
        perfectMatchesMax = new boolean[data.getModels().length];
        mppMax = new boolean[data.getModels().length];
        mpaMax = new boolean[data.getModels().length];
        ranksMax = new boolean[data.getModels().length];
        
        jLabel4.setText(this.data.getConfigFile());
        jLabel5.setText(this.data.getUserFile());
        jLabel9.setText(this.data.getTrainingsFile());
        
        for (int i=0; i<this.data.getModels().length; i++) {
        	jTable1.getModel().setValueAt(this.data.getModels()[i], i, 0);
        }
        
    }

    /** 
     * This method is called from within the constructor to
     * initialize the form.
     */
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        diagramm = new CoordinatePanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Mean Prediction Probability"));

        jButton1.setText("Pause");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout diagrammLayout = new javax.swing.GroupLayout(diagramm);
        diagramm.setLayout(diagrammLayout);
        diagrammLayout.setHorizontalGroup(
            diagrammLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 476, Short.MAX_VALUE)
        );
        diagrammLayout.setVerticalGroup(
            diagrammLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 145, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(397, Short.MAX_VALUE)
                .addComponent(jButton1))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(diagramm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(diagramm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1))
        );

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Model", "Prediction Rank", "Perfect Matches", "MPP", "MPA"
            }
        ));
        jTable1.setEnabled(false);
        jScrollPane1.setViewportView(jTable1);
        
        jTable1.setDefaultRenderer(Object.class, new CustomCellRenderer());
        

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Results");

        jLabel2.setText("Config File: ");

        jLabel3.setText("User Data: ");

        jLabel4.setText("ConfigFileName");

        jLabel5.setText("UserDataFileName");

        jLabel6.setText("with xxx states");

        jLabel7.setText("with xxx entries");
        
        jLabel8.setText("Trainings Data: ");
        jLabel9.setText("TrainingsDataFileName");
        jLabel10.setText("with xxx entries");
        
        

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 488, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10))
                .addGap(41, 41, 41)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>


    public void setPerfectMatches(int[] matches) {
    	try {
	    	int max=-1;
	    	
	    	for (int i=0; i<matches.length; i++) {
	    		if (matches[i]>=max) {
	    			max = matches[i];
	    		}
	    	}
	    	
	    	for (int i=0; i<matches.length; i++) {
	    		if (matches[i]==max) {
	    			perfectMatchesMax[i] = true;
	    		} else {
	    			perfectMatchesMax[i] = false;
	    		}
	    		jTable1.getModel().setValueAt(matches[i], i, 2);
	    	}
    	}catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
    public void setPredictionRank(double[] ranks) {
    	try {
			double max=Double.POSITIVE_INFINITY;
			
			for (int i=0; i<ranks.length; i++) {
				if (ranks[i]<=max) {
					max = ranks[i];
				}
			}
			
			for (int i=0; i<ranks.length; i++) {
				if (ranks[i]==max) {
					ranksMax[i] = true;
				} else {
					ranksMax[i] = false;
				}
				jTable1.getModel().setValueAt(ranks[i], i, 1);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public void setMPP(double[] accuracy) {
    	try {
	    	double max=-1;
	    	
	    	for (int i=0; i<accuracy.length; i++) {
	    		if (accuracy[i]>=max) {
	    			max = accuracy[i];
	    		}
	    	}
	    	for (int i=0; i<accuracy.length; i++) {
	    		if (accuracy[i]==max) {
	    			mppMax[i] = true;
	    		} else {
	    			mppMax[i] = false;
	    		}
	    		jTable1.getModel().setValueAt(Double.toString((double)(((int)(accuracy[i]*10000))/100.0)) + "%", i, 3);
	    	}
    	}catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
    public void setMPA(double[] accuracy) {
    	try {
	    	double max=-1;
	    	
	    	for (int i=0; i<accuracy.length; i++) {
	    		if (accuracy[i]>=max) {
	    			max = accuracy[i];
	    		}
	    	}
	    	for (int i=0; i<accuracy.length; i++) {
	    		if (accuracy[i]==max) {
	    			mpaMax[i] = true;
	    		} else {
	    			mpaMax[i] = false;
	    		}
	    		jTable1.getModel().setValueAt(Double.toString((double)(((int)(accuracy[i]*10000))/100.0)) + "%", i, 4);
	    	}
    	}catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
    	System.out.println("Pause");
    	if (bench.pause()) {
    		jButton1.setText("Resume");
    	} else {
    		jButton1.setText("Pause");
    	}
    }
    
    public void setPauseEnabled(boolean b) {
    	jButton1.setEnabled(b);
    }
    
    public void setStatesCount(int statesCount) {
    	jLabel6.setText("with " + statesCount + " states");
    }
    
    public void setUserInteractionCount(int interactionCount) {
        jLabel7.setText("with " + interactionCount + " entries");	
    }
    
    public void setTrainingsInteractionCount(int interactionCount) {
        jLabel10.setText("with " + interactionCount + " entries");	
    }
    
    public CoordinatePanel getCoordinatePanel() {
    	return diagramm;
    }


    // Variables declaration - do not modify
    private CoordinatePanel diagramm;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration

    public class CustomCellRenderer extends DefaultTableCellRenderer{
    	
        /**its serialNumber*/
		private static final long serialVersionUID = -6349832662347263057L;

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,boolean hasFocus, int row, int column){
        	
                Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                
                Color c = new Color(200,200,255);
                if ((row<perfectMatchesMax.length && column==2 && perfectMatchesMax[row]) ||
                		(row<mppMax.length && column==3 && mppMax[row]) ||
                		(row<ranksMax.length && column==1 && ranksMax[row]) ||
                		(row<mpaMax.length && column==4 && mpaMax[row])) {
                	cell.setBackground( c );
                	
                } else {
                   	cell.setBackground( Color.white );
                }
                
                return cell;
        }
    }

    
}
