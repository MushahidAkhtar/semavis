package de.fhg.igd.interactionanalysis.testBench.components;

import de.fhg.igd.interactionanalysis.core.IPM;
import de.fhg.igd.interactionanalysis.core.data.Interactions;
import de.fhg.igd.interactionanalysis.core.data.SteadyStateVector;
import de.fhg.igd.interactionanalysis.core.utility.InteractionTranslator;
import de.fhg.igd.interactionanalysis.testBench.data.TestResults;

/**
 *
 * @author cstab
 */
public class SSVPanel extends javax.swing.JPanel {

    /** its serial number */
	private static final long serialVersionUID = 7478181690287705777L;
	
	//TestResults results;
	
	/** Creates new form SSVPanel */
    public SSVPanel() {
        initComponents();
    }
    
    public void update(TestResults results) {
    	IPM[] models = results.getModels();
    	String[] headers = new String[models.length+1];
    	double[] sums = new double[models.length];
    	
    	headers[0] = "States";
    	for (int i=1; i<headers.length; i++) {
    		headers[i] = results.getData().getModels()[i-1];
    	}
    	
    	String[][] tableContents = new String[Interactions.getInstance().getSize()+2][models.length+1];
    	
    	String[][] states = Interactions.getInstance().getInteractions();
    	for (int i=0; i<Interactions.getInstance().getSize();i++) {
    		tableContents[i][0] = InteractionTranslator.translateInteraction(states[i]);
    	}
    	
    	for (int i=0; i<models.length; i++) {
    		SteadyStateVector ssv = models[i].getSteadyStateVector();
    		for (int j=0; j<Interactions.getInstance().getSize(); j++) {
    			tableContents[j][i+1] = Double.toString((double)(((int)(ssv.getValues()[j]*10000))/100.0)) + "%";
    			sums[i] = sums[i] + ssv.getValues()[j];
    		}
    	}
    	
    	// Sum
    	for (int i=0; i<models.length+1; i++) {
    		tableContents[Interactions.getInstance().getSize()][i] = "-------";
    	}
   		tableContents[Interactions.getInstance().getSize()+1][0] = "Sum";
   		for (int i=0;i<models.length; i++) {
   			tableContents[Interactions.getInstance().getSize()+1][i+1] = Double.toString(sums[i]);
   		}
   		
    	
    	jTable1.setModel(new javax.swing.table.DefaultTableModel(
    			tableContents,headers));
    }

    /** 
     * This method is called from within the constructor to
     * initialize the form.
     */
    private void initComponents() {
    	jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jTable1.setEnabled(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Steady State Vector");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "States", "Model1", "Model2", "Model3", "Model4", "Model5", "Model6"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addComponent(jLabel1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>


    // Variables declaration - do not modify
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration

}
