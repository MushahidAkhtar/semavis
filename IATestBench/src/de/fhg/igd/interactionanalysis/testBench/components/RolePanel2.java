package de.fhg.igd.interactionanalysis.testBench.components;


import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import de.fhg.igd.interactionanalysis.core.IRolePM;
import de.fhg.igd.interactionanalysis.core.data.Domains;
import de.fhg.igd.interactionanalysis.core.data.QueryResult;

/**
 * Panel for the role model
 * @author Christian Stab
 */
public class RolePanel2 extends javax.swing.JPanel {

    /** its serial number */
	private static final long serialVersionUID = 8603280974139184432L;
	
	private int[] ranks;
	
	private IRolePM pm;
	
	private String pmName;
	
	/** Creates new form RolePanel */
    public RolePanel2(IRolePM pm, String pmName) {
    	this.pm = pm;
    	this.pmName = pmName;
        initComponents();
    }

    /** 
     * This method is called from within the constructor to
     * initialize the form.
     */
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable1);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12));
        jLabel1.setText("Roles");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Query"));

        jLabel2.setText("Ranks:");

        jButton1.setText("query");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        
        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Abstraction", "Probability"
            }
        ));
        jScrollPane3.setViewportView(jTable2);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Roles"));

        jList1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jList1.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				selectionChanged(e);
			}
        });
        jScrollPane1.setViewportView(jList1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 351, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Sequences"));

        jList2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        jScrollPane4.setViewportView(jList2);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 351, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 383, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>

    
    private void selectionChanged(ListSelectionEvent e) {
    	updateTable();
    	updateSequenceList();
    }
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
    	String[] strRanks = jTextField1.getText().split(",");
    	
    	if (strRanks.length!= Domains.getInstance().getSize()) {
    		JOptionPane.showMessageDialog(this, Domains.getInstance().getSize() + " ranks needed!", "Error", JOptionPane.ERROR_MESSAGE);
    		ranks = null;
    		return;
    	}
    	
    	ranks = new int[strRanks.length];
    	for (int i=0; i<ranks.length; i++) {
    		try {
    			ranks[i] = Integer.parseInt(strRanks[i]);
    		} catch (NumberFormatException e) {
    			JOptionPane.showMessageDialog(this, "Ranks must be like 'x,y,z,...'", "Error", JOptionPane.ERROR_MESSAGE);
    			ranks = null;
    		}
    	}
    	
    	update();
    }
    
    public void updateSequenceList() {
    	int selectedIndex = jList1.getSelectedIndex();
    	if (selectedIndex!=-1) {
	    	int[][] sequences = pm.getRoleSequences(selectedIndex);
	    	
	    	DefaultListModel listModel = new DefaultListModel();
	    	for (int i=0; i<sequences.length; i++) {
	    		String seq = "";
	    		for (int j=0; j<sequences[i].length; j++) {
	    			seq = seq + sequences[i][j] + "  ";
	    		}
	    		listModel.addElement(seq);
	    	}
	    	
	    	jList2.setModel(listModel);
    	}
    }
    
    public void update() {
    	// update List
    	int selectedIndex = jList1.getSelectedIndex();
    	DefaultListModel listModel = new DefaultListModel();
    	
    	for (int i=0; i<pm.getRoleCounts();i++) {
    		if (i==pm.getActiveRole()) {
    			listModel.addElement("Role " + i + "*");
    		} else {
    			listModel.addElement("Role " + i);
    		}
    	}
    	jList1.setModel(listModel);
    	jList1.setSelectedIndex(selectedIndex);
    	
    	updateTable();
    }
    
    public void updateTable() {
    	
    	if (ranks!=null && jList1.getSelectedIndex()!=-1) {
    		String[] headers = new String[] {"Abstractions", "Probabilities"};
    		QueryResult result = pm.queryRole(ranks, jList1.getSelectedIndex());
    		
    		String[][] tableContents = new String[result.getIndices().length+2][2];
    		
    		double sum = 0.0d;
    		for (int i=0; i<result.getIndices().length;i++) {
	    		tableContents[i][0] = result.getIndices()[i];
	    		tableContents[i][1] = Double.toString((double)(((int)(result.getValues()[i]*10000))/100.0)) + "%";
	    		sum = sum + result.getValues()[i];
	    	}
    		tableContents[result.getIndices().length][0] = "-------";
    		tableContents[result.getIndices().length][1] = "-------";
    		tableContents[result.getIndices().length+1][0] = "Sum";
    		tableContents[result.getIndices().length+1][1] = Double.toString(sum);
    		
    		jTable2.setModel(new javax.swing.table.DefaultTableModel(
	    			tableContents,headers));
    	}
    }
    
    public String getModelName() {
    	return pmName;
    }

    // Variables declaration - do not modify
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JList jList1;
    private javax.swing.JList jList2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration

}
