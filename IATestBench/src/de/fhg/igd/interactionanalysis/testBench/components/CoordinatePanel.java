package de.fhg.igd.interactionanalysis.testBench.components;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Line2D;

import javax.swing.JPanel;

import de.fhg.igd.interactionanalysis.testBench.data.TestResults;


public class CoordinatePanel extends JPanel implements MouseListener{
	
	/** the border  around the coordinate system */
	private int xborder=40;
	private int yborder=30;
	
	/** X-Axis description */
	private int xSize;
	private int xDescCount = 10;
	private int xDescSpace = 17;
	
	/** Y-Axis description */
	
	private int ySize;
	private int yDescCount = 4;
	private int yDescSpace = 22;
	
	
	/** the size of the arrow **/
	private int arrowSize1=3;
	private int arrowSize2=5;
	
	/** the length of description lines */
	private int descriptionLines = 2;
	
	/** colors for different models */
	private Color[] colors;
	
	private TestResults results;
	
	/** its serial number */
	private static final long serialVersionUID = 3307381769608273092L;
	
	private double[][] nameCoord;
	private boolean[] showModel;
	
	
	public CoordinatePanel() {
		super();
		addMouseListener(this);
		colors = new Color[] {Color.BLACK, Color.BLUE, Color.CYAN, Color.GRAY, Color.GREEN, Color.MAGENTA, Color.RED, Color.ORANGE,Color.WHITE, Color.darkGray};
	}
	
	public void setResults(TestResults result) {
		this.results = result;
		ySize =1;
		xSize= results.getInteractionCount();
		setBackground(Color.WHITE);
		
		if (nameCoord==null) {
			nameCoord = new double[result.getModels().length][4];
		}
		if (showModel== null) {
			showModel = new boolean[result.getModels().length];
			for (int i=0; i<showModel.length; i++) {
				showModel[i]=true;
			}
		}
		
	}

	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		
		// bottom line with arrow
		g2.drawLine(xborder, getHeight()-yborder, getWidth()-xborder, getHeight()-yborder);
		g2.drawLine(getWidth()-xborder, getHeight()-yborder,getWidth()-xborder-arrowSize2, getHeight()-yborder-arrowSize1);
		g2.drawLine(getWidth()-xborder, getHeight()-yborder,getWidth()-xborder-arrowSize2, getHeight()-yborder+arrowSize1);
		
		// left line
		g2.drawLine(xborder, yborder, xborder, getHeight()-yborder);
		g2.drawLine(xborder, yborder, xborder+arrowSize1, yborder+arrowSize2);
		g2.drawLine(xborder, yborder, xborder-arrowSize1, yborder+arrowSize2);
		
		// zero
		//g2.drawString("0", xborder-10, getHeight()-yborder+10);
		
		// draw x-axis description
		int space = (getWidth()-(2*xborder)) / (xDescCount);
		for (int i=0; i<xDescCount+1; i++) {
			// draw x-axis lines
			if (i!=xDescCount) {
				g2.drawLine(xborder+i*space, getHeight()-yborder, xborder+i*space, getHeight()-yborder+descriptionLines);
			}
			
			if (i>0 && i!=xDescCount) {
				// write description
				double width = g2.getFontMetrics(getFont()).getStringBounds(Integer.toString((xSize/xDescCount)*i), g2).getWidth();
				g2.drawString(Integer.toString((xSize/xDescCount)*i), xborder+i*space-(int)(width/2), getHeight()-yborder+xDescSpace);
			}
		}
		
		// draw y-axis description
		space = (getHeight()-(2*yborder)) / (yDescCount); 
		for (int i=0; i<yDescCount+1; i++) {
			// draw x-axis lines
			if (i!=yDescCount) {
				g2.drawLine(xborder, getHeight()-yborder-i*space, xborder-descriptionLines, getHeight()-yborder-i*space);
			}
			
			if (i>0 && i!=yDescCount) {
				// write description
				double height = g2.getFontMetrics(getFont()).getStringBounds(Float.toString((ySize/yDescCount)*i), g2).getHeight();
				double descent = g2.getFontMetrics(getFont()).getDescent();
				g2.drawString(Double.toString((((double)ySize)/((double)yDescCount))*i), xborder-yDescSpace, getHeight()-yborder-i*space+(int)((height/2)-descent));
			}
		}
		
		if (results!=null) {
			//draw results
			Color tmp = g2.getColor();
			double[][] results = this.results.getAllMPP();
			for (int i=0; i<results.length; i++) {
				if (showModel[i]) {
					double[] res = results[i];
					g2.setColor(colors[i]);
					for (int j=0; j<res.length; j++) {
						if (j>0 && j<res.length) {
							drawLine(g2, j-1, res[j-1], j, res[j]);
						} else {
							drawPoint(g2, j, res[j]);
						}
					}
				}
			}
			
			
			//drawNames
			int fontHeight=g2.getFontMetrics().getHeight();
			
			for (int i=0; i<this.results.getData().getModels().length; i++) {
				g.setColor(colors[i]);
				g2.drawString(this.results.getData().getModels()[i], getWidth()-xborder-40, yborder+i*fontHeight);
				nameCoord[i][0] = getWidth()-xborder-40;
				nameCoord[i][1] = (yborder+i*fontHeight) -fontHeight;
				nameCoord[i][2] = getWidth()-xborder-40 + g2.getFontMetrics().stringWidth(this.results.getData().getModels()[i]);
				nameCoord[i][3] = yborder+i*fontHeight;
			}
			g2.setColor(tmp);
		}
		
	}
	
	public void drawPoint(Graphics2D g2, double x, double y) {
		double px = xborder + (((float)getWidth()-2*xborder)/(float)xSize) * x;
		double py = getHeight() - yborder - (((float)getHeight()-2*yborder)/(float)ySize) * y;
		g2.drawLine((int)px,(int)py,(int)px,(int)py);
	}
	
	public void drawLine(Graphics2D g2, double x1, double y1, double x2, double y2) {
		double px1 = xborder + (((float)getWidth()-2*xborder)/(float)xSize) * x1;
		double py1 = getHeight() - yborder - (((float)getHeight()-2*yborder)/(float)ySize) * y1;
		double px2 = xborder + (((float)getWidth()-2*xborder)/(float)xSize) * x2;
		double py2 = getHeight() - yborder - (((float)getHeight()-2*yborder)/(float)ySize) * y2;
		//g2.drawLine((int)px1,(int)py1,(int)px2,(int)py2);
		
		g2.draw(new Line2D.Double((int)px1, (int)py1, (int)px2, (int)py2));
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		//System.out.println("klick");
		int x = arg0.getX();
		int y = arg0.getY();
		
		if (nameCoord!=null) {
			for (int i=0; i<nameCoord.length; i++) {
				if (nameCoord[i][0]<x && nameCoord[i][1]<y && nameCoord[i][2]>x && nameCoord[i][3]>y) {
					showModel[i] = !showModel[i];
					getParent().repaint();
					revalidate();
				}
			}
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) 	{}

	@Override
	public void mouseExited(MouseEvent arg0) 	{}

	@Override
	public void mousePressed(MouseEvent arg0) 	{}

	@Override
	public void mouseReleased(MouseEvent arg0) 	{}
	
}
