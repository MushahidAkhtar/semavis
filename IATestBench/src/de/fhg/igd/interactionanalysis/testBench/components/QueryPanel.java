package de.fhg.igd.interactionanalysis.testBench.components;

import javax.swing.JOptionPane;

import de.fhg.igd.interactionanalysis.core.IPM;
import de.fhg.igd.interactionanalysis.core.data.Domains;
import de.fhg.igd.interactionanalysis.core.data.QueryResult;
import de.fhg.igd.interactionanalysis.testBench.IATestBench;
import de.fhg.igd.interactionanalysis.testBench.data.TestResults;


/**
 *
 * @author cstab
 */
public class QueryPanel extends javax.swing.JPanel {

    /** its serialnumber*/
	private static final long serialVersionUID = -8628420082946653328L;
	
	private int[] ranks;
	
	private IATestBench bench;
	
	/** Creates new form QueryPanel */
    public QueryPanel(IATestBench bench) {
    	this.bench = bench;
        initComponents();
    }
    
    public void update(TestResults results) {
    	if (ranks!=null && results.getModels().length>0) {
    		IPM[] models = results.getModels();
	    	String[] headers = new String[models.length+1];
	    	
	    	double[] sums = new double[models.length];
	    	
	    	headers[0] = "Abstractions";
	    	for (int i=1; i<headers.length; i++) {
	    		headers[i] = results.getData().getModels()[i-1];
	    	}
	    	
	    	QueryResult queryResult1 = models[0].query(ranks);
	    	
	    	
	    	String[][] tableContents = new String[queryResult1.getIndices().length+2][models.length+1];
	    	
	    	
	    	for (int i=0; i<queryResult1.getIndices().length;i++) {
	    		tableContents[i][0] = queryResult1.getIndices()[i];
	    		tableContents[i][1] = Double.toString((double)(((int)(queryResult1.getValues()[i]*10000))/100.0)) + "%";
	    		sums[0] = sums[0] + queryResult1.getValues()[i];
	    	}
	    	
	    	for (int i=1; i<models.length; i++) {
	    		QueryResult queryResult = models[i].query(ranks);
	    		for (int j=0; j<queryResult.getValues().length; j++) {
	    			tableContents[j][i+1] = Double.toString((double)(((int)(queryResult.getValues()[j]*10000))/100.0)) + "%";
	    			sums[i] = sums[i] + queryResult.getValues()[j];
	    		}
	    	}
	    	
	    	// Sum
	    	for (int i=0; i<models.length+1; i++) {
	    		tableContents[queryResult1.getIndices().length][i] = "-------";
	    	}
	   		tableContents[queryResult1.getIndices().length+1][0] = "Sum";
	   		for (int i=0;i<models.length; i++) {
	   			tableContents[queryResult1.getIndices().length+1][i+1] = Double.toString(sums[i]);
	   		}
	   		
	   		jTable1.setModel(new javax.swing.table.DefaultTableModel(
	    			tableContents,headers));
    	}
    	
    }

    /** 
     * This method is called from within the constructor to
     * initialize the form.
     */
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jTable1.setEnabled(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Query");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Ranks"));

        jLabel2.setText("Ranks:");

        jButton1.setText("query");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        
        
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Abstractions", "Model1", "Model2", "Model3", "Model4", "Model5", "Model6"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 394, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
    	String[] strRanks = jTextField1.getText().split(",");
    	
    	if (strRanks.length!= Domains.getInstance().getSize()) {
    		JOptionPane.showMessageDialog(this, Domains.getInstance().getSize() + " ranks needed!", "Error", JOptionPane.ERROR_MESSAGE);
    		ranks = null;
    		return;
    	}
    	
    	ranks = new int[strRanks.length];
    	for (int i=0; i<ranks.length; i++) {
    		try {
    			ranks[i] = Integer.parseInt(strRanks[i]);
    		} catch (NumberFormatException e) {
    			JOptionPane.showMessageDialog(this, "Ranks must be like 'x,y,z,...'", "Error", JOptionPane.ERROR_MESSAGE);
    			ranks = null;
    		}
    	}
    	
    	update(bench.getResults());
    }


    // Variables declaration - do not modify
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration

}
