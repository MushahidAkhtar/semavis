package de.fhg.igd.interactionanalysis.testBench.components;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class StatusBar extends JPanel {
    
	/** its serial number */
	private static final long serialVersionUID = -4771483890976579571L;
	
	private JProgressBar bar;
	private JLabel label;
	
    /** Creates a new instance of StatusBar */
    public StatusBar() {
        super();
        super.setPreferredSize(new Dimension(100, 25));
        bar = new JProgressBar(0,100);
        label = new JLabel();
        setLayout(new BorderLayout());
        add(bar, BorderLayout.CENTER);
        add(label, BorderLayout.AFTER_LINE_ENDS);
        setMessage("Ready");
        bar.setVisible(false);
    }
    
    public void setMessage(String message) {
        label.setText(" "+message);        
    }
    
    public void setPercentage(int per) {
    	if (per<100) {
    		bar.setVisible(true);
    		setMessage("Learning (" + per + "%)");
    		bar.setValue(per);
    	} else {
    		setMessage("Ready");
    		bar.setVisible(false);
    	}
    }
}