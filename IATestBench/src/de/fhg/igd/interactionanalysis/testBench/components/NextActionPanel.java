package de.fhg.igd.interactionanalysis.testBench.components;

import de.fhg.igd.interactionanalysis.core.IPM;
import de.fhg.igd.interactionanalysis.core.utility.InteractionTranslator;
import de.fhg.igd.interactionanalysis.testBench.data.TestResults;

/**
 *
 * @author cstab
 */
public class NextActionPanel extends javax.swing.JPanel {

    /** its serial number */
	private static final long serialVersionUID = 470101775169808199L;
	
	/** Creates new form NextAction */
    public NextActionPanel() {
        initComponents();
    }
    
    public void update(TestResults results) {
    	if (results!=null) {
	    	IPM[] models = results.getModels();
	    	
	    	String[] header1  = new String[] { "Model","Action"};
	    	String[] header2  = new String[] { "Model","Actions"};
	    	String[][] tableContents1 = new String[models.length][2];
	    	String[][] tableContents2 = new String[models.length][2];
	    	
	    	for (int i=0; i<models.length; i++) {
	    		tableContents1[i][0] = results.getData().getModels()[i];
	    		tableContents1[i][1] = InteractionTranslator.translateInteraction(models[i].getNextAction());
	    		
	    		tableContents2[i][0] = results.getData().getModels()[i];
	    		String nextActions = "";
	    		int[] nextAct = models[i].getNextActions(4);
	    		for (int j=0; j<nextAct.length; j++) {
	    			if (nextAct[j]!=-1) {
	    				if (nextActions.equals("")) {
	    					nextActions = "[" +InteractionTranslator.translateInteraction(nextAct[j]) + "]";
	    				} else {
	    					nextActions = nextActions + ", [" +InteractionTranslator.translateInteraction(nextAct[j])+ "]";
	    				}
	    			}
	    		}
	    		tableContents2[i][1] = nextActions;
	    	}
	    	
	    	
	    	jTable1.setModel(new javax.swing.table.DefaultTableModel(
	    			tableContents1,header1));
	    	
	    	jTable4.setModel(new javax.swing.table.DefaultTableModel(
	    			tableContents2,header2));
	    	
    	}
    }

    /** 
     * This method is called from within the constructor to
     * initialize the form.
     */
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable4 = new javax.swing.JTable();

        jTable1.setEnabled(false);
        jTable4.setEnabled(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Next Action");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Model", "Action"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Next Actions");

        jTable4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Model", "Actions"
            }
        ));
        jScrollPane4.setViewportView(jTable4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(175, Short.MAX_VALUE))
        );
    }// </editor-fold>


    // Variables declaration - do not modify
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable4;
    // End of variables declaration

}
