package de.fhg.igd.interactionanalysis.testBench;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import de.fhg.igd.interactionanalysis.core.IRolePM;
import de.fhg.igd.interactionanalysis.core.data.Interactions;
import de.fhg.igd.interactionanalysis.core.utility.Config;
import de.fhg.igd.interactionanalysis.testBench.components.NextActionPanel;
import de.fhg.igd.interactionanalysis.testBench.components.QueryPanel;
import de.fhg.igd.interactionanalysis.testBench.components.RolePanel2;
import de.fhg.igd.interactionanalysis.testBench.components.SSVPanel;
import de.fhg.igd.interactionanalysis.testBench.components.StatusBar;
import de.fhg.igd.interactionanalysis.testBench.components.OverviewPanel;
import de.fhg.igd.interactionanalysis.testBench.data.TestData;
import de.fhg.igd.interactionanalysis.testBench.data.TestResults;
import de.fhg.igd.interactionanalysis.testBench.dialogs.NewTestDialog;
import de.fhg.igd.interactionanalysis.testBench.tester.ResultListener;
import de.fhg.igd.interactionanalysis.testBench.tester.Tester;


public class IATestBench extends JFrame implements ResultListener{

	/** its serial number */
	private static final long serialVersionUID = 4230057496470375427L;

	/** log4j logger */
	static Logger logger = Logger.getLogger(IATestBench.class);
	
	/** Menu Bar*/
	private JMenuBar menuBar;
	
	/** the tabbed pane */
	private JTabbedPane tabPane;
	
	private OverviewPanel overviewPanel;
	private SSVPanel ssvPanel;
	private QueryPanel queryPanel;
	private NextActionPanel nextActionPanel;
	
	private ArrayList<RolePanel2> rolePanels;
	
	/** its status bar */ 
	private StatusBar statusBar;
	
	private Thread learner;
	private boolean wait;
	
	private TestResults results;
	
	/**
	 * Basic Constructor
	 */
	public IATestBench() {
		super("IATestBench");
		rolePanels = new ArrayList<RolePanel2>();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBackground(Color.WHITE);
		setPreferredSize(new Dimension(700,600));
		
		wait = false;
	
		init();
		initMenus();
		
        //Display the window.
        pack();
        setVisible(true);
	}
	
	
	private void init() {
		
		// Tabbed Pane
		tabPane = new JTabbedPane();
		
		// Status Bar
		statusBar = new StatusBar();
		//statusBar.setPercentage(50);
		getContentPane().add(statusBar, java.awt.BorderLayout.SOUTH);

		getContentPane().add(tabPane);
	}
	
	private void initMenus() {
		menuBar = new JMenuBar();

		// Test Menu
		JMenu menu;
		menu = new JMenu("Test");
		menu.setMnemonic(KeyEvent.VK_A);
		
		JMenuItem item = new JMenuItem("New Test");
		item.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				NewTestDialog dialog = new NewTestDialog((IATestBench)getFrames()[0]);
				dialog.pack();
				dialog.setLocationRelativeTo((IATestBench)getFrames()[0]);
				dialog.setVisible(true);
			}
		});
		menu.add(item);
		
		item = new JMenuItem("Open Test File");
		item.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser chooser = new JFileChooser(System.getProperty("user.dir"));
			    int returnVal = chooser.showOpenDialog(getFrames()[0]);
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
		            loadTest(chooser.getSelectedFile().getAbsolutePath());
			    }
			}
		});
		menu.add(item);
		menu.add(new JSeparator());
		
		
		item = new JMenuItem("Save Test");
		item.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (results == null) {
					JOptionPane.showMessageDialog(getFrames()[0], "No results available", "Error", JOptionPane.ERROR_MESSAGE);
				} else {
					JFileChooser chooser = new JFileChooser(System.getProperty("user.dir"));
				    int returnVal = chooser.showOpenDialog(getFrames()[0]);
				    if(returnVal == JFileChooser.APPROVE_OPTION) {
			            saveTest(chooser.getSelectedFile().getAbsolutePath());
				    }
				}
			}
		});
		menu.add(item);
		
		item = new JMenuItem("Export CSV");
		item.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (results == null) {
					JOptionPane.showMessageDialog(getFrames()[0], "No results available", "Error", JOptionPane.ERROR_MESSAGE);
				} else {
					JFileChooser chooser = new JFileChooser(System.getProperty("user.dir"));
				    int returnVal = chooser.showOpenDialog(getFrames()[0]);
				    if(returnVal == JFileChooser.APPROVE_OPTION) {
			            exportCSV(chooser.getSelectedFile().getAbsolutePath());
				    }
				}
			}
		});
		menu.add(item);
		
		menuBar.add(menu);
		
		
		
		setJMenuBar(menuBar);
	}
	
	public void newTest(String config, String training, String user, String[] models) {
		tabPane.removeAll();
		
		TestData data = new TestData(config, training,user, models);
		
		overviewPanel =  new OverviewPanel(data, this);
		overviewPanel.setPauseEnabled(true);
		tabPane.add("Overview", overviewPanel);
		
		ssvPanel = new SSVPanel();
		tabPane.add("SSV", ssvPanel);
		
		queryPanel = new QueryPanel(this);
		tabPane.add("Query", queryPanel);
		
		nextActionPanel = new NextActionPanel();
		tabPane.add("Actions", nextActionPanel);
		
		rolePanels.clear();
		
		// run Test
		Tester tester = new Tester(data, this);
		learner = new Thread(tester);
		learner.start();
		
		//System.out.println("Thread is running");
	}
	
	public void addRolePanel(IRolePM pm, String name) {
		RolePanel2 panel = new RolePanel2(pm,name);
		rolePanels.add(panel);
		tabPane.add("Roles (" + name + ")", panel);
	}
	
	@SuppressWarnings("deprecation")
	public boolean pause() {
		if (!wait) {
			learner.suspend();
			wait = true;
		} else {
			learner.resume();
			wait = false;
		}
		return wait;
	}
	
	

	@Override
	public void newResult(int progress, TestResults results) {
		this.results = results;
		overviewPanel.setUserInteractionCount(results.getData().getUserSize());
		overviewPanel.setTrainingsInteractionCount(results.getData().getTrainingsSize());
		overviewPanel.setStatesCount(Interactions.getInstance().getSize());
		overviewPanel.setPerfectMatches(results.getPerfectMatches());
		overviewPanel.setPredictionRank(results.getPredictionRanks());
		overviewPanel.setMPP(results.getMPP());
		overviewPanel.setMPA(results.getMPA());
		overviewPanel.getCoordinatePanel().setResults(results);
		overviewPanel.repaint();
		
		ssvPanel.update(results);
		
		queryPanel.update(results);
		
		nextActionPanel.update(results);
		
		//System.out.println("New Results " + progress);
		statusBar.setPercentage(progress);
		
		if (rolePanels.isEmpty() && !results.getRoleModels().isEmpty()) {
			List<IRolePM> list = results.getRoleModels();
			List<String> names = results.getRoleModelNames();
			for (int i=0; i<list.size(); i++) {
				addRolePanel(list.get(i), names.get(i));
			}
		}
		for (int i=0;i<rolePanels.size(); i++) {
			rolePanels.get(i).update();
		}
		
		if (progress==100) {
			overviewPanel.setPauseEnabled(false);
			// finished
		}
	}
	
	
	/**
	 * main method for testing the tester ;)
	 * @param args standard args
	 */
	public static void main (String[] args) {
		PropertyConfigurator.configure("log4j.properties");
		
		try {
			 UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    } catch (Exception e) {
	    }

		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new IATestBench();
            }
        });
	}
	
	/**
	 * Saves results to a file
	 * @param fileName name of the file
	 */
	private void saveTest(String fileName) {
		try {
			FileOutputStream fos = new FileOutputStream(fileName);
			BufferedOutputStream out = new BufferedOutputStream(fos);
			ObjectOutputStream oos = new ObjectOutputStream(out);
			oos.writeObject(results);
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	private void loadTest(String fileName) {
		tabPane.removeAll();
		
		FileInputStream fis;
		try {
			fis = new FileInputStream(fileName);
			BufferedInputStream in = new BufferedInputStream(fis);
			ObjectInputStream ois = new ObjectInputStream(in);
			results = (TestResults) ois.readObject();;
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		Config.getInstance(results.getData().getConfigFile());
		
		overviewPanel =  new OverviewPanel(results.getData(), this);
		overviewPanel.setPauseEnabled(false);
		overviewPanel.setUserInteractionCount(results.getData().getUserSize());
		overviewPanel.setStatesCount(Interactions.getInstance().getSize());
		overviewPanel.setPerfectMatches(results.getPerfectMatches());
		overviewPanel.setPredictionRank(results.getPredictionRanks());
		overviewPanel.setMPP(results.getMPP());
		overviewPanel.setMPA(results.getMPA());
		overviewPanel.getCoordinatePanel().setResults(results);
		overviewPanel.repaint();
		
		ssvPanel=new SSVPanel();
		ssvPanel.update(results);
		
		queryPanel=new QueryPanel(this);
		
		nextActionPanel = new NextActionPanel();
		nextActionPanel.update(results);
		
		tabPane.add("Overview", overviewPanel);
		tabPane.add("SSV", ssvPanel);
		tabPane.add("Query", queryPanel);
		tabPane.add("Actions", nextActionPanel);

		List<IRolePM> list = results.getRoleModels();
		List<String> names = results.getRoleModelNames();
		for (int i=0; i<list.size(); i++) {
			addRolePanel(list.get(i), names.get(i));
		}
		
		for (int i=0;i<rolePanels.size(); i++) {
			rolePanels.get(i).update();
		}
	}
	
	/**
	 * exports results to a CSV File
	 * @param fileName name of the csv
	 */
	private void exportCSV(String fileName) {
		String[] tmp = new String[results.getModels().length];
		double[][] result = results.getAllMPP();
		for (int i=0; i<results.getModels().length; i++) {
			double[] res = result[i];
			tmp[i] = results.getData().getModels()[i] + "; ";
			for (int j=0; j<res.length; j++) {
				tmp[i] = tmp[i] + Double.toString(res[j]).replace('.', ',') + "; ";
			}
		}
		
		try {
			FileOutputStream writer = new FileOutputStream(fileName);
			BufferedOutputStream out = new BufferedOutputStream(writer);
			byte[] crlf = new byte[] {13, 10};
			for (int i=0; i<tmp.length; i++) {
				out.write(tmp[i].getBytes());
				out.write(crlf);
			}
			out.close();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public TestResults getResults() {
		return results;
	}

	
}
