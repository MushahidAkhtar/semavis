package de.fhg.igd.interactionanalysis.testBench.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//import de.fhg.igd.interactionanalysis.core.IRolePM;
import de.fhg.igd.interactionanalysis.core.IPM;
import de.fhg.igd.interactionanalysis.core.IRolePM;

public class TestResults implements Serializable {

	/** its serialnumber */
	private static final long serialVersionUID = -3082100719028707951L;

	private double[] predictionRanks;
	
	private int[] perfectMatches;
	
	/** mean prediction probability */
	private double[] mpp;
	
	/** mean prediction accuracy */
	private double[] mpa;
	
	private double[][] mpp_all;
	
	private TestData data;
	
	private int interactionCount;
	
	private int statesCount;
	
	private IPM[] models;

	public TestResults(int models, int data, TestData testData) {
		this.data = testData;
		mpp_all = new double[models][data];
		perfectMatches = new int[models];
		mpp = new double[models];
		mpa = new double[models];
		predictionRanks = new double[models];
	}
	
	public IPM[] getModels() {
		return models;
	}

	public void setModels(IPM[] models) {
		this.models = models;
	}

	public int getInteractionCount() {
		return interactionCount;
	}

	public void setInteractionCount(int interactionCount) {
		this.interactionCount = interactionCount;
	}

	public int getStatesCount() {
		return statesCount;
	}

	public void setStatesCount(int statesCount) {
		this.statesCount = statesCount;
	}

	public TestData getData() {
		return data;
	}

	public double[] getPredictionRanks() {
		return predictionRanks;
	}

	public void setPredictionRanks(int model, double predictionRanks) {
		this.predictionRanks[model] = predictionRanks;
	}

	public int[] getPerfectMatches() {
		return perfectMatches;
	}

	public void setPerfectMatches(int model, int perfectMatches) {
		this.perfectMatches[model] = perfectMatches;
	}

	public double[] getMPP() {
		return mpp;
	}

	public void setMPP(int model, double mpp) {
		this.mpp[model] = mpp;
	}

	public double[][] getAllMPP() {
		return mpp_all;
	}

	public void setAllMPP(double[][] ranks) {
		this.mpp_all = ranks;
	}

	public double[] getMPA() {
		return mpa;
	}

	public void setMPA(int model, double mpa) {
		this.mpa[model] = mpa;
	}
	
	public List<IRolePM> getRoleModels() {
		ArrayList<IRolePM> list = new ArrayList<IRolePM>(); 
		for (int i=0; i<models.length; i++) {
			if (models[i] instanceof IRolePM) {
				list.add((IRolePM) models[i]);
			}
		}
		return list;
	}
	
	public List<String> getRoleModelNames() {
		ArrayList<String> list = new ArrayList<String>(); 
		for (int i=0; i<models.length; i++) {
			if (models[i] instanceof IRolePM) {
				list.add(data.getModels()[i]);
			}
		}
		return list;
	}
	
}
