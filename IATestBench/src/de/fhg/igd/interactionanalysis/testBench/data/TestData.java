package de.fhg.igd.interactionanalysis.testBench.data;

import java.io.Serializable;

public class TestData implements Serializable {

	/** its serialNumber */
	private static final long serialVersionUID = 1675087908162166405L;

	private String configFile;
	
	private String trainingsFile;
	
	private String userFile;
	
	private int trainingsSize;
	
	private int userSize;
	
	public String getUserFile() {
		return userFile;
	}

	private String[] models;
	
	public TestData(String configFile, String trainingsFile,String userFile, String[] models) {
		this.configFile = configFile;
		this.userFile = userFile;
		this.trainingsFile = trainingsFile;
		this.models = models;
	}
	
	public String getConfigFile() {
		return configFile;
	}

	public String getTrainingsFile() {
		return trainingsFile;
	}

	public String[] getModels() {
		return models;
	}
	
	public int getTrainingsSize() {
		return trainingsSize;
	}

	public void setTrainingsSize(int trainingsSize) {
		this.trainingsSize = trainingsSize;
	}

	public int getUserSize() {
		return userSize;
	}

	public void setUserSize(int userSize) {
		this.userSize = userSize;
	}
	
}
