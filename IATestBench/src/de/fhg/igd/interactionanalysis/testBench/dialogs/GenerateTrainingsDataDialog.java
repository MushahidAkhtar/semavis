package de.fhg.igd.interactionanalysis.testBench.dialogs;

import javax.swing.JOptionPane;
import javax.swing.JDialog;
import javax.swing.JTextField;

import de.fhg.igd.interactionanalysis.testBench.IATestBench;

import java.beans.*; //property change stuff
import java.awt.Dimension;
import java.awt.event.*;

/* 1.4 example used by DialogDemo.java. */
public class GenerateTrainingsDataDialog extends JDialog
                   implements ActionListener,
                              PropertyChangeListener {
    /**	its serial number */
	private static final long serialVersionUID = -4257835032531144799L;
	
	private String typedText = null;
    private JTextField textField;
    private JOptionPane optionPane;

    private String btnString1 = "Ok";
    private String btnString2 = "Cancel";

    //private IATestBench bench;
    
    /**
     * Returns null if the typed string was invalid;
     * otherwise, returns the string as the user entered it.
     */
    public String getValidatedText() {
        return typedText;
    }

    /** Creates the reusable dialog. */
    public GenerateTrainingsDataDialog(IATestBench aFrame) {
        super(aFrame, true);

        //bench = aFrame;
        
        setTitle("Number of interactions");

        textField = new JTextField(10);
        textField.setPreferredSize(new Dimension(200,150));

        //Create an array of the text and components to be displayed.
        String msgString1 = "How many interactions?";
        
        Object[] array = {msgString1, textField};

        //Create an array specifying the number of dialog buttons
        //and their text.
        Object[] options = {btnString1, btnString2};

        //Create the JOptionPane.
        optionPane = new JOptionPane(array,
                                    JOptionPane.QUESTION_MESSAGE,
                                    JOptionPane.YES_NO_OPTION,
                                    null,
                                    options,
                                    options[0]);

        optionPane.setPreferredSize(new Dimension(200,150));
        
        //Make this dialog display it.
        setContentPane(optionPane);

        //Handle window closing correctly.
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
                /*
                 * Instead of directly closing the window,
                 * we're going to change the JOptionPane's
                 * value property.
                 */
                    optionPane.setValue(new Integer(
                                        JOptionPane.CLOSED_OPTION));
            }
        });

        //Ensure the text field always gets the first focus.
        addComponentListener(new ComponentAdapter() {
            public void componentShown(ComponentEvent ce) {
                textField.requestFocusInWindow();
            }
        });

        //Register an event handler that puts the text into the option pane.
        textField.addActionListener(this);

        //Register an event handler that reacts to option pane state changes.
        optionPane.addPropertyChangeListener(this);
    }

    /** This method handles events for the text field. */
    public void actionPerformed(ActionEvent e) {
        optionPane.setValue(btnString1);
    }

    /** This method reacts to state changes in the option pane. */
    public void propertyChange(PropertyChangeEvent e) {
        String prop = e.getPropertyName();

        if (isVisible()
         && (e.getSource() == optionPane)
         && (JOptionPane.VALUE_PROPERTY.equals(prop) ||
             JOptionPane.INPUT_VALUE_PROPERTY.equals(prop))) {
            Object value = optionPane.getValue();

            if (value == JOptionPane.UNINITIALIZED_VALUE) {
                //ignore reset
                return;
            }

            //Reset the JOptionPane's value.
            //If you don't do this, then if the user
            //presses the same button next time, no
            //property change event will be fired.
            optionPane.setValue(
                    JOptionPane.UNINITIALIZED_VALUE);

            if (btnString1.equals(value)) {
                    typedText = textField.getText();
                String ucText = typedText.toUpperCase();
                if (isNumber(ucText)) {
                    //bench.generateTrainingsData(Integer.parseInt(ucText));
                    clearAndHide();
                } else {
                    //text was invalid
                    textField.selectAll();
                    JOptionPane.showMessageDialog(
                    		GenerateTrainingsDataDialog.this,
                                    "Please enter a number",
                                    "Try again",
                                    JOptionPane.ERROR_MESSAGE);
                    typedText = null;
                    textField.requestFocusInWindow();
                }
            } else { //user closed dialog or clicked cancel
                //dd.setLabel("It's OK.  "
                         //+ "We won't force you to type "
                         //+ magicWord + ".");
                typedText = null;
                clearAndHide();
            }
        }
    }
    
    private boolean isNumber(String str) {
    	try {
    		Integer.parseInt(str);
    	} catch (Exception e) {
    		return false;
    	}
    	return true;
    }

    /** This method clears the dialog and hides it. */
    public void clearAndHide() {
        textField.setText(null);
        setVisible(false);
    }
}
