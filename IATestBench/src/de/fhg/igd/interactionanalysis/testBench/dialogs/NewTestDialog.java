package de.fhg.igd.interactionanalysis.testBench.dialogs;

import javax.swing.JDialog;

import de.fhg.igd.interactionanalysis.testBench.IATestBench;
import de.fhg.igd.interactionanalysis.testBench.dialogs.panels.NewTestPanel;


public class NewTestDialog extends JDialog  {
    /**	its serial number */
	private static final long serialVersionUID = -4257835032531144799L;
	
    private IATestBench bench;
    
    private NewTestPanel testPanel;

    /** Creates the reusable dialog. */
    public NewTestDialog(IATestBench aFrame) {
        super(aFrame, true);
        setTitle("New Test");

        bench = aFrame;
        
        testPanel=new NewTestPanel(this);
        setContentPane(testPanel);
    }
    
    public void commit(String config, String training,String user, String[] models) {
    	bench.newTest(config, training, user, models);
    }
}
