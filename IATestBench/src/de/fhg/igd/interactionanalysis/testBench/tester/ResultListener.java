package de.fhg.igd.interactionanalysis.testBench.tester;

import de.fhg.igd.interactionanalysis.testBench.data.TestResults;

public interface ResultListener {

	public void newResult(int progress, TestResults results);
}
