package de.fhg.igd.interactionanalysis.testBench.tester;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import de.fhg.igd.interactionanalysis.core.IPAM;
import de.fhg.igd.interactionanalysis.core.IPM;
import de.fhg.igd.interactionanalysis.core.IRolePM;
import de.fhg.igd.interactionanalysis.core.KO;
import de.fhg.igd.interactionanalysis.core.LEV;
import de.fhg.igd.interactionanalysis.core.MK;
import de.fhg.igd.interactionanalysis.core.KOStar19;
import de.fhg.igd.interactionanalysis.core.RMMRank;
import de.fhg.igd.interactionanalysis.core.RMMUniform;
import de.fhg.igd.interactionanalysis.core.data.Interactions;
import de.fhg.igd.interactionanalysis.core.utility.Config;
import de.fhg.igd.interactionanalysis.testBench.data.TestData;
import de.fhg.igd.interactionanalysis.testBench.data.TestResults;

public class Tester implements Runnable {

	public TestData data;
	
	private IPM[] models;
	
	private int[] interactionsTraining;
	private int[] interactionsUser;
	
	private ResultListener listener;
	
	private TestResults results;
	
	private List<IRolePM> roleModels;
	
	public Tester(TestData data, ResultListener listener) {
		this.data = data;
		this.listener = listener;
		models = new IPM[data.getModels().length];
		roleModels = new ArrayList<IRolePM>();
	}
	
	@Override
	public void run() {
		Config.getInstance(data.getConfigFile());
		
		// initialize Models
		for (int i=0; i<models.length; i++) {
			models[i] = getModelInstance(data.getModels()[i]);
		}
		
		// read interaction file
		interactionsTraining = readData(data.getTrainingsFile());
		interactionsUser = readData(data.getUserFile());
		if (interactionsTraining == null) {
			data.setTrainingsSize(0);
		} else {
			data.setTrainingsSize(interactionsTraining.length);
		}
		data.setUserSize(interactionsUser.length);
		
		results = new TestResults(data.getModels().length, interactionsUser.length, data);
		results.setStatesCount(Interactions.getInstance().getSize());
		results.setInteractionCount(interactionsUser.length);
		results.setModels(models);

		// learn models
		learnModels();
		
		// save Results
		
	}
	
	/**
	 * reads the interactions from the given file
	 * @param fileName filename of the trainingsdata file
	 */
	private int[] readData(String fileName) {
		if (fileName!= null && (new File(fileName)).exists()) {
			ArrayList<String> inter = new ArrayList<String>();
			try {
				FileReader reader = new FileReader(fileName);
				BufferedReader in = new BufferedReader(reader);
				while (in.ready()) {
					String line = in.readLine();
					//System.out.println(line);
					inter.add(line);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			int[] interactions = new int[inter.size()];
			for (int i=0; i<inter.size(); i++) {
				interactions[i] = Interactions.getInstance().getIndex(inter.get(i).split(" "));
			}
			return interactions;
		} else {
			return null;
		}
	}
	
	private void learnModels() {
		int progressMax = 0;
		int trainingsCount = 0;
	
		// train Model (Trainingsdata)
		if (interactionsTraining!= null) {
			progressMax = models.length * (interactionsUser.length + interactionsTraining.length);		
		
			
			for (int j=0; j<models.length; j++) {
				models[j].setTrainingsData(interactionsTraining);
				
				// inform results
				if (j==0) {
					listener.newResult(0, results);
				} else {
					listener.newResult((int)((((double)((j)*interactionsTraining.length))/((double)progressMax))*100), results);
				}
			}
			
			trainingsCount = interactionsTraining.length;
		} else {
			progressMax = models.length * interactionsUser.length;
		}
		
		// enable roles;
		for (int i=0; i<roleModels.size(); i++) {
			roleModels.get(i).enableRoleLearning(true);
		}
		
		
		// user Data
		int[] predictionRanks = new int[models.length];
		double[] mpp = new double[models.length];
		
		for (int i=0; i<interactionsUser.length-1; i++) {
			
			for (int j=0; j<models.length; j++) {
				models[j].newObservation(interactionsUser[i]);
				
				
				int predictionRank = models[j].getPredictionRank(interactionsUser[i], interactionsUser[i+1]);
				predictionRanks[j] = predictionRanks[j] + predictionRank;
				
				if (models[j].getNextAction()==interactionsUser[i+1]) {
					results.getPerfectMatches()[j]++;
				}
				
				mpp[j] = mpp[j] + models[j].getPredictionProbability(interactionsUser[i], interactionsUser[i+1]);
				
				
				results.setPredictionRanks(j, ((double)predictionRanks[j]) / ((double)i+1));
				results.setMPA(j, ((double)results.getPerfectMatches()[j]) / ((double)i+1));
				results.setMPP(j, mpp[j] / ((double)i+1));
				
				//results.getAllMPP()[j][i] = models[j].getPredictionProbability(interactionsUser[i], interactionsUser[i+1]);
				results.getAllMPP()[j][i] = mpp[j] / ((double)i+1);
			}
			
			
			// inform results
			if (i==0) {
				listener.newResult(0, results);
			} else {
				listener.newResult((int)((((double)((i+2+trainingsCount)*models.length))/((double)progressMax))*100), results);
			}
		}
	}
	
	
	/**
	 * returns an instance of a model
	 * @param str name of the model
	 * @return proibabilistic model
	 */
	private IPM getModelInstance(String str) {
		if (str.equals("MK")) {
			return new MK();
		}
		if (str.equals("IPAM")) {
			return new IPAM();
		}
		if (str.equals("RMMUniform")) {
			return new RMMUniform();
		}
		if (str.equals("RMMRank")) {
			return new RMMRank();
		}
		if (str.equals("LEV3")) {
			return new LEV();
		}
		if (str.equals("KO3/19")) {
			return new KO();
		}
		if (str.equals("KO*/19")) {
			IRolePM tmp = new KOStar19();
			roleModels.add(tmp);
			return (IPM)tmp;
		}
		
		return null;
	}
	

}
